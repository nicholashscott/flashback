//  Created by Nick Scott

import UIKit
import CoreData

protocol HaveExtraDelegate {
    func haveExtraFunc()
    func populateThreads(_: ThreadsViewController.ThreadsChoice)
}

protocol BuddiesPushDelegate {
    
    func populateGroups()
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    static var delegate:HaveExtraDelegate? //updates buddies with call from local
    static var delegateBuddies:BuddiesPushDelegate? //updates buddies with call from local

    
    var window: UIWindow?
    let defaults = NSUserDefaults.standardUserDefaults()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        Parse.enableLocalDatastore()

        
        // Initialize Parse.
        Parse.setApplicationId("4xg3tzVKRCohY2Ae7xtxKmoYMtzxo0D1ZkUjRupL",
            clientKey: "fAxtP3jBolysKsx6iZKm1E2dbdzbJyje2JZ2ZN4I")
        
        // [Optional] Track statistics around application opens.
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
    
        
        if application.applicationState != UIApplicationState.Background {
            if let notificationPayload = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
                
                let type:NSString = (notificationPayload["type"] as? NSString)!
                if type == "comment" {
                    if PFUser.currentUser() != nil {
                        let picVideoId:NSString = (notificationPayload["picVideoId"] as? NSString)!
                        let activityId:NSString = (notificationPayload["activityId"] as? NSString)!

                        let isVideo:Bool = (notificationPayload["isVideo"] as? Bool)!
                        let createdAt:NSString = (notificationPayload["createdAt"] as? NSString)!
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        
                        let createdAtNSDate = dateFormatter.dateFromString(createdAt as String)
                        
                        let name:NSString = (notificationPayload["name"] as? NSString)!
                        
                        let message:NSString = (notificationPayload["message"] as? NSString)!
                        
                        
                        let isGroup:Bool = (notificationPayload["isGroup"] as? Bool)!

                        if isGroup == true {
                            let picVideoGroupName:NSString = (notificationPayload["picVideoGroupName"] as? NSString)!
                            globalPushComment.picVideoName = picVideoGroupName as String

                
                        } else {
                            let picVideoReceiver:NSString = (notificationPayload["picVideoReceiver"] as? NSString)!
                            let picVideoUser:NSString = (notificationPayload["picVideoUser"] as? NSString)!
                            if PFUser.currentUser()?.username == picVideoReceiver {
                                globalPushComment.picVideoName = picVideoUser as String
                            } else {
                                globalPushComment.picVideoName = picVideoReceiver as String
                            }

                            
                        }

                        globalPushComment.activityId = activityId as String
                        globalPushComment.picVideoId = picVideoId as String
                        globalPushComment.isVideo = isVideo
                        globalPushComment.createdAtNSDate = createdAtNSDate
                        globalPushComment.name = name as String
                        globalPushComment.message = message as String
    
                        GlobalOpenedByPushNotification = "comment"

                    }
                } else if type == "group" {
                    
                    GlobalOpenedByPushNotification = "group"
                    
                } else if type == "flashback" {
                    GlobalOpenedByPushNotification = "flashback" 

                }
            }
        }
        
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        var initialViewController:UIViewController!
        if PFUser.currentUser() == nil {

            initialViewController = storyboard.instantiateViewControllerWithIdentifier("Main") 

        } else {

            initialViewController = storyboard.instantiateViewControllerWithIdentifier("LoggedIn") 
            GlobalSignInUp = false

        }
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()

    
        if let tempGto:AnyObject = defaults.objectForKey("GlobalTimeOpened") {
            let tempDate:AnyObject = (tempGto as? NSDate)!
            
            let userCalendar = NSCalendar.currentCalendar()
            let releaseTime = userCalendar.dateByAddingUnit(
                .Hour,
                value: -2,
                toDate: NSDate(),
                options: [])!
            
            let unlock = tempDate.compare(releaseTime)
            if unlock == .OrderedAscending {
                GlobalShouldReload = true
            } else {
                GlobalShouldReload = false
            }
        } else {
            GlobalShouldReload = true
        }
        
        application.setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        application.statusBarHidden = false
        


        if application.respondsToSelector("registerUserNotificationSettings:") {
            let userNotificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
            let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        } else {
            application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [UIUserNotificationType.Sound, UIUserNotificationType.Alert, UIUserNotificationType.Badge], categories: nil))

        }
        
        return true
    }
    
    
    //for push notificatinos
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
            }
    
    //for push notificatinos
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if error.code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
        }
    }
    
    
    //for push notificatinos
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        if let type:NSString = (userInfo["type"] as? NSString) {
            if type == "comment" || type == "group" {
                ThreadsViewController.haveExtra = true
                AppDelegate.delegate?.haveExtraFunc()
            } else if type == "flashback" {
                AppDelegate.delegate?.populateThreads(ThreadsViewController.ThreadsChoice.getNew)
            }
            
            if type == "group" {
                AppDelegate.delegateBuddies?.populateGroups()
            }
        }

        if application.applicationState == UIApplicationState.Inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        NSNotificationCenter.defaultCenter().postNotificationName("stopAv", object: nil)
        defaults.setObject(NSDate(), forKey: "GlobalTimeOpened")

    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NSNotificationCenter.defaultCenter().postNotificationName("stopAv", object: nil)
        
        
        
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        NSNotificationCenter.defaultCenter().postNotificationName("startAv", object: nil)
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NSNotificationCenter.defaultCenter().postNotificationName("startAv", object: nil)
        if let tempGto:AnyObject = defaults.objectForKey("GlobalTimeOpened") {
            let tempDate:AnyObject = (tempGto as? NSDate)!
            
            let userCalendar = NSCalendar.currentCalendar()
            let releaseTime = userCalendar.dateByAddingUnit(
                .Hour,
                value: -2,
                toDate: NSDate(),
                options: [])!
            
            let unlock = tempDate.compare(releaseTime)
            if unlock == .OrderedAscending {
                GlobalShouldReload = true
            } else {
                GlobalShouldReload = false
            }
        }

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

        NSNotificationCenter.defaultCenter().postNotificationName("stopAv", object: nil)
        defaults.setObject(NSDate(), forKey: "GlobalTimeOpened")
        
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.coredata" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Model", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Flashback.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }

}

