//  Created by Nick Scott


import UIKit

protocol ConfirmDelegate {
    func signedIn()
    
}

class ConfirmPhoneViewController: UIViewController, UITextFieldDelegate {
    
    var delegate:ConfirmDelegate!

    //from login
    var username:String!
    var password:String!
    var phone:String!
    var confirmationCode:Int!
    
    @IBOutlet var boxView: UIView!
    @IBOutlet var firstDigit: UITextField!
    @IBOutlet var thirdDigit: UITextField!
    @IBOutlet var secondDigit: UITextField!

    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var blurView:UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))

    
    @IBAction func back(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func go(sender: AnyObject) {
        
        completeSignup()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.firstDigit.resignFirstResponder()
        self.secondDigit.resignFirstResponder()
        self.thirdDigit.resignFirstResponder()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.boxView.backgroundColor = UIColor(patternImage: UIImage(named: "loginBackground")!)
        blurView.alpha = 0.5
        blurView.frame = self.view.bounds
    }
    
    func completeSignup(){
        
        let tempCode:String = (firstDigit.text! + secondDigit.text! + thirdDigit.text!)
        
        if confirmationCode == Int(tempCode) {
            self.view.addSubview(self.blurView)
            activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            var error:String?
            let user = PFUser()
            user.username = username
            user["userLower"] = (username).lowercaseString
            user.password = password
            user["phone"] = phone
            
            user.signUpInBackgroundWithBlock {
                (succeeded, signupError) -> Void in
                
                self.activityIndicator.stopAnimating()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                
                if signupError == nil {
                    let installation = PFInstallation.currentInstallation()
                    installation["user"] = PFUser.currentUser()
                    installation.saveInBackground()
                    self.delegate.signedIn()
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                } else {
                    if let errorString = signupError!.userInfo["error"] as? NSString {
                        error = errorString as String
                        self.displayAlert("Couldn't Sign Up :(", error: error!)
                    }
                }
            }
        } else {
            self.activityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            self.displayAlert("Code is incorrect", error: "Sorry, the code is incorrect :( Please check the code and try again or press back to change what phone number it is sent to.")
        }
    }
    
    func goNext(textField : UITextField){
        if  textField == firstDigit {
            firstDigit.resignFirstResponder()
            secondDigit.becomeFirstResponder()
        } else if textField == secondDigit {
            secondDigit.resignFirstResponder()
            thirdDigit.becomeFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var shouldProcess = false
        var moveToNextTextField = false
        let insertStringLength = string.characters.count
        if insertStringLength == 0 {
            shouldProcess = true
        }
        else {
            if textField.text!.characters.count == 0 {
                shouldProcess = true
            }
        }
        var myString = textField.text

        if shouldProcess {
            if myString!.characters.count == 0{
                myString = myString! + string
                moveToNextTextField = true
            }
            textField.text = myString
            
            if moveToNextTextField {
                goNext(textField)
            }
        }
        return (myString!.characters.count - range.length < 1)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == thirdDigit {
            self.completeSignup()
        }
        return true
    }

    
    func displayAlert(title:String, error:String){
        
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(false, completion: nil)
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
}
