//  Created by Nick Scott

import UIKit

class CustomHeaderCell: UITableViewCell {

    @IBOutlet var headerLabel: UILabel!
}
