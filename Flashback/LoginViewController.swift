//  Created by Nick Scott


import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate, ConfirmDelegate{
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var boxView: UIView!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var signLabel: UILabel!
    
    var signupActive = false
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var blurView:UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))

    var border = CALayer()
    var width = CGFloat(1.0)
    
    //signup variables
    var trimmedPhone:String?
    var confirmationCode:Int?
    
    //check username
    @IBOutlet var usernameAI: UIActivityIndicatorView!
    @IBOutlet var usernameStatus: UILabel!

    //status
    var fromConfirm:Bool?
    var logIn:Bool?
    
    //Constraints and keyboard
    @IBOutlet var signUpVertical: NSLayoutConstraint!
    @IBOutlet var titleTopConstraint: NSLayoutConstraint!
    var keyboardOpen:Bool = false
    
    //toggle signup/signin
    @IBAction func signUpButton(sender: AnyObject) {
        if signupActive == false {
            
            self.signupActive = true
            
            self.usernameAI.hidden = true
            self.usernameStatus.hidden = true
            
            if self.usernameAI.isAnimating() {
                self.usernameAI.stopAnimating()
            }
            
            self.signInButton.alpha = 0.6
            self.signUpButton.alpha = 0.9
            self.signUpVertical.constant = self.signUpVertical.constant + 45
            
            if self.keyboardOpen == true {
                self.titleTopConstraint.constant = 55
            }
            
            UIView.animateWithDuration(0.5) {
                self.view.layoutIfNeeded()
                self.signLabel.text = "s i g n   u p   t o   f l a s h b a c k . . . . . . . ."
                self.phone.alpha = 1
            }
        }
    }
    
    //toggle signup/signin
    @IBAction func signInButton(sender: AnyObject) {
        if signupActive == true {
            
            signupActive = false
            
            self.usernameAI.hidden = true
            self.usernameStatus.hidden = true
            
            if self.usernameAI.isAnimating() {
                self.usernameAI.stopAnimating()
            }
            
            
            if phone.isFirstResponder() {
                self.phone.resignFirstResponder()
                self.username.isFirstResponder()
            }
            
            self.signInButton.alpha = 0.9
            self.signUpButton.alpha = 0.6
            self.signUpVertical.constant = self.signUpVertical.constant - 45
            
            if self.keyboardOpen == true {
                self.titleTopConstraint.constant = 100
            }
            
            UIView.animateWithDuration(0.5) {
                self.view.layoutIfNeeded()
                self.signLabel.text = "s i g n   i n   t o   f l a s h b a c k . . . . . . . ."
                self.phone.alpha = 0
            }
        }
    }
    
    @IBAction func signUp(sender: AnyObject) {
        signUpFunction()
        
    }

    func displayAlert(title:String, error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // protocol function - sets bool to true if came from confirmation window
    func signedIn (){
        fromConfirm = true
    }

    override func viewWillAppear(animated: Bool) {
        
        self.usernameAI.hidden = true
        self.usernameStatus.hidden = true
        
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillChangeFrameNotification, object: nil)
        
        //go to main container if signed up and confirmed phone
        if fromConfirm == true {

            GlobalShouldReload = true
            GlobalSignInUp = true
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:MainScrollView = storyboard.instantiateViewControllerWithIdentifier("LoggedIn") as! MainScrollView
            MainScrollView.firstTime = true
            initialViewController.fromLoginSignUp = true
            self.presentViewController(initialViewController, animated: true, completion: nil)
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        if fromConfirm == true || logIn == true {
            username.text = ""
            password.text = ""
            phone.text = ""
        }
        
        fromConfirm = false
        logIn = false
        
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.boxView.backgroundColor = UIColor(patternImage: UIImage(named: "loginBackground")!)
        self.phone.returnKeyType = UIReturnKeyType.Go
        
        self.username.text = ""
        self.password.text = ""
        self.phone.text = ""
        self.phone.delegate = self
        self.username.delegate = self
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGrayColor().CGColor
        border.frame = CGRect(x: 0, y: username.frame.size.height - width, width:  username.frame.size.width, height: username.frame.size.height)
        border.borderWidth = width
        username.layer.addSublayer(border)
        username.layer.masksToBounds = true
        
        let border2 = CALayer()
        let width2 = CGFloat(1.0)
        border2.borderColor = UIColor.lightGrayColor().CGColor
        border2.frame = CGRect(x: 0, y: -username.frame.size.height + width2, width:  username.frame.size.width, height: username.frame.size.height)
        border2.borderWidth = width2
        phone.layer.addSublayer(border2)
        phone.layer.masksToBounds = true
        
        
        blurView.alpha = 0.5
        blurView.frame = self.view.bounds

        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
       
    }
    
    func signUpFunction(){
        var error = ""
        if signupActive == true {
            trimmedPhone = self.phone.text!.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            trimmedPhone = trimmedPhone!.stringByReplacingOccurrencesOfString(") ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            trimmedPhone = trimmedPhone!.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            if username.text!.characters.count < 3 {
                error = "Sorry, username has to be at least 3 characters!"
                self.displayAlert("Wait, something is wrong!", error: error)
            }else if password.text!.characters.count < 6 {
                error = "Sorry, password has to be at least 6 characters!"
                self.displayAlert("Wait, something is wrong!", error: error)
            }else if username.text == "" || password.text == "" || phone.text == "" {
                error = "Please enter a username, password and phone number"
                self.displayAlert("Wait, something is wrong!", error: error)
            } else if (trimmedPhone!).characters.count < 10 {
                error = "Are you missing a digit or two? Please enter a valid phone number."
                self.displayAlert("Wait, something is wrong!", error: error)
            } else {
                
                self.view.addSubview(self.blurView)
                self.view.addSubview(self.activityIndicator)
                var constraints = [NSLayoutConstraint]()
                constraints.append(NSLayoutConstraint(
                    item: self.activityIndicator,
                    attribute: .CenterX,
                    relatedBy: .Equal,
                    toItem: view,
                    attribute: .CenterX,
                    multiplier: 1,
                    constant: 0)
                )
                constraints.append(NSLayoutConstraint(
                    item: self.activityIndicator,
                    attribute: .CenterY,
                    relatedBy: .Equal,
                    toItem: view,
                    attribute: .CenterY,
                    multiplier: 1,
                    constant: -50)
                )
                
                
                self.view.addConstraints(constraints)
                
                self.activityIndicator.startAnimating()
                UIApplication.sharedApplication().beginIgnoringInteractionEvents()

                let phoneNumber:String = trimmedPhone!
                let params = ["phoneNumber": phoneNumber]
                PFCloud.callFunctionInBackground("sendVerificationCode", withParameters: params) {
                    (response: AnyObject?, errorParse: NSError?) -> Void in
                    
                    self.blurView.removeFromSuperview()
                    self.activityIndicator.removeFromSuperview()
                    self.activityIndicator.stopAnimating()
                    
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    if errorParse != nil {
                        error = "Oops something went wrong when trying to connect! Please check your internet connection and try again later."
                        self.displayAlert("Wait, something is wrong!", error: error)
                    } else {
                        if let tempConfirmationCode:Int = response as? Int {
                            self.confirmationCode = tempConfirmationCode
                        }
                        self.performSegueWithIdentifier("toConfirm", sender: self)
                    }
                    
                }
            }
        }else if signupActive == false{
            if username.text == "" || password.text == "" {
                error = "Please enter a username and password"
                self.displayAlert("Couldn't Sign In :(", error: error)
            } else {
                self.view.addSubview(self.blurView)
                self.view.addSubview(self.activityIndicator)
                var constraints = [NSLayoutConstraint]()
                constraints.append(NSLayoutConstraint(
                    item: self.activityIndicator,
                    attribute: .CenterX,
                    relatedBy: .Equal,
                    toItem: view,
                    attribute: .CenterX,
                    multiplier: 1,
                    constant: 0)
                )
                constraints.append(NSLayoutConstraint(
                    item: self.activityIndicator,
                    attribute: .CenterY,
                    relatedBy: .Equal,
                    toItem: view,
                    attribute: .CenterY,
                    multiplier: 1,
                    constant: -50)
                )
                self.view.addConstraints(constraints)

                self.activityIndicator.startAnimating()
                UIApplication.sharedApplication().beginIgnoringInteractionEvents()

                PFUser.logInWithUsernameInBackground(username.text!, password:password.text!) {
                    (user, signupError) -> Void in
                    
                    if signupError == nil {
                       
                        GlobalShouldReload = true
                        GlobalSignInUp = true
                        
                        let installation = PFInstallation.currentInstallation()
                        installation["user"] = PFUser.currentUser()
                        installation.saveInBackground()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let initialViewController:MainScrollView = storyboard.instantiateViewControllerWithIdentifier("LoggedIn") as! MainScrollView
                        
                        initialViewController.fromLoginSignUp = true
                        self.logIn = true
                        
                        MainScrollView.firstTime = true
                        
                        self.presentViewController(initialViewController, animated: true, completion: nil)
                    } else {
                        
                        if signupError?.code == 100 {
                            
                            error = "We couldn't connect. Check your internet connection and try again!"

                        } else if signupError?.code == 101 {
                            
                            error = "Wrong username or password. Check 'em and try again!"

                        } else {
                            error = "Whoops! There seems to be a problem on our end. Sorry about that. Please try again later."
                        }
                        
                        self.displayAlert("Could Not Sign In", error: error)
                    }
                    
                    self.blurView.removeFromSuperview()
                    self.activityIndicator.removeFromSuperview()
                    self.activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if textField == phone
        {
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if (length - index) > 3
            {
                let areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("(%@) ", areaCode)
                index += 3
            }
            if length - index > 3
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            
            textField.returnKeyType = UIReturnKeyType.Go
            
            return false
        } else if textField == username {
            
            textField.returnKeyType = UIReturnKeyType.Next
            
            var shouldChange = false
            
            if username.text!.characters.count + string.characters.count < 20 {
                shouldChange = true
            }
            
            let invalidCharacters = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_").invertedSet
            if let _ = string.rangeOfCharacterFromSet(invalidCharacters, options: [], range:Range<String.Index>(start: string.startIndex, end: string.endIndex)) {
                shouldChange = false
            }
            
            return shouldChange
            
        } else {
            if signupActive{
                textField.returnKeyType = UIReturnKeyType.Next
            } else {
                textField.returnKeyType = UIReturnKeyType.Go
            }
            
            return true
        }
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == username && signupActive == true && username.text != ""  {
            self.usernameAI.hidden = false
            self.usernameAI.startAnimating()

            let usernameQuery = PFUser.query()
            usernameQuery?.whereKey("username", equalTo: textField.text!)
            usernameQuery?.limit = 1
            usernameQuery!.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                if error == nil {
                    if objects!.count != 0 {
                        self.usernameStatus.hidden = false
                        self.usernameStatus.text = "taken :("
                        self.usernameStatus.textColor = UIColor.redColor().colorWithAlphaComponent(0.7)
                    } else if objects!.count == 0 {
                        self.usernameStatus.hidden = false
                        self.usernameStatus.text = "available :)"
                        self.usernameStatus.textColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.7)
                    }
                }else {
                    self.usernameStatus.hidden = true
                }
                self.usernameAI.stopAnimating()
                self.usernameAI.hidden = true
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == username {
            
            self.password.becomeFirstResponder()
            
        } else if textField == password {
            if signupActive == false {
                signUpFunction()
            } else {
                self.phone.becomeFirstResponder()
            }
        }

        return true

    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        
        if textField == username && signupActive == true  {
            
            if self.usernameAI.isAnimating() {
                self.usernameAI.stopAnimating()
            }
            self.usernameAI.hidden = true
            self.usernameStatus.hidden = true
            
        }
        
        if textField == phone
        {
            textField.returnKeyType = UIReturnKeyType.Go
            
            return true
        } else if textField == username {
            
            textField.returnKeyType = UIReturnKeyType.Next
            
            return true
            
        } else {
            if signupActive{
                textField.returnKeyType = UIReturnKeyType.Next
            } else {
                textField.returnKeyType = UIReturnKeyType.Go
            }
            
            return true
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toConfirm" {
            let destinationViewController = segue.destinationViewController as! ConfirmPhoneViewController
            destinationViewController.confirmationCode = confirmationCode
            destinationViewController.delegate = self
            destinationViewController.username = username.text
            destinationViewController.password = password.text
            destinationViewController.phone = trimmedPhone
        }
    }
    

    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if self.signupActive == true {
                self.titleTopConstraint.constant = 55
            }
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    self.keyboardOpen = true
                }
            )
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}

