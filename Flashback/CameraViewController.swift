//  Created by Nick Scott


import UIKit
import CoreMedia
import AVFoundation
import CoreTelephony


class CameraViewController: UIViewController, UITextFieldDelegate, AVCaptureFileOutputRecordingDelegate  {
    
    static var delegate:ContainerView!

    @IBAction func goRight(sender: AnyObject) {
        CameraViewController.delegate!.disableScroll(ChildViews.C)
    }
    
    @IBAction func goLeft(sender: AnyObject) {
        CameraViewController.delegate!.disableScroll(ChildViews.A)
    }
    
    @IBOutlet var goLeft: UIButton!
    @IBOutlet var goRight: UIButton!
    
    @IBOutlet var reload: UIButton!
    
    @IBAction func reload(sender: AnyObject) {
        reloadCamera()
    }
    
    let deliveryTimeOptions = ["MONDAY", "1 WEEK", "1 MONTH", "1 YEAR", "CUSTOM"]
    var dayChoice:String = "MONDAY"
    
    let timeOfDay = ["MORNING", "AFTERNOON", "NIGHT", "LATE NIGHT", "CUSTOM"]
    var timeChoice = "MORNING"
    
    let audioOffImage = UIImage(named: "audioOff")
    let audioOnImage = UIImage(named: "audioOn")
    
    var callCenter = CTCallCenter()
    
    @IBOutlet var muteItem: UIButton!
    @IBAction func mute(sender: AnyObject) {
        
        if muted == true {
            muted = false
            
            muteItem.setImage(audioOnImage, forState: UIControlState.Normal)
            if let player = player {
                player.muted = false
            }
        } else {
            muted = true
            muteItem.setImage(audioOffImage, forState: UIControlState.Normal)
            if let player = player {
                player.muted = true
            }
            
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("muteNotification", object: self)
        
    }
    
    //top and bottom blur
    @IBOutlet var topView: UIVisualEffectView!
    @IBOutlet var bottomView: UIVisualEffectView!
    //inner square views
    @IBOutlet var innerView: UIView! // inside square
    @IBOutlet var imageCapturedView: UIView! //when pic is added
    //take view
    @IBOutlet var takeView: UIView!
    //camera buttons
    @IBOutlet var takePhotoButton: UIButton!
    @IBOutlet var deliveryTime: UIButton! //left side set of buttons
    @IBOutlet var dreamOrMemory: UIButton!
    @IBOutlet var timeOfDelivery: UIButton! // right side set of buttons
    @IBOutlet var flipCamera: UIButton!
    //left side options
    @IBOutlet var buttonsLeft: UIView!
    @IBOutlet var option1: UIButton!
    @IBOutlet var option2: UIButton!
    @IBOutlet var option3: UIButton!
    @IBOutlet var option4: UIButton!
    //right side options
    @IBOutlet var buttonsRight: UIView!
    @IBOutlet var option12: UIButton!
    @IBOutlet var option22: UIButton!
    @IBOutlet var option32: UIButton!
    @IBOutlet var option42: UIButton!
    
    var buttons: Array<UIButton> = Array()
    var titles: Array<String> = Array()

    var buttons2: Array<UIButton> = Array()
    var titles2: Array<String> = Array()
    var titlesLocal2: Array<String> = Array()
    var myDatePicker = UIDatePicker()
    var selectDate   = UIButton(type: UIButtonType.System)
    var customDate:NSDate?
    var customTime:NSDate?
    
    var lastWasToday:Bool = false //for setting custom date
    var timeNSDate:NSDate? //for seeing if time should be replaced with morning


    @IBOutlet var picTitle: UITextField!
    @IBOutlet var sendPicTitle: UIButton!
    
    
    var flashOn:Bool?, selfie:Bool?
    var imageCaptured = UIImageView()
    var didTakePhoto = false
    var image = UIImage()
    var memory:Bool?
    @IBOutlet var textToSuper: NSLayoutConstraint! //adjust for keyboard up and down
    @IBOutlet var keyboardSpacing: NSLayoutConstraint!    //keyboard constraint to text field
    @IBOutlet var keyboardSpacing2: NSLayoutConstraint!  // send button in  title text field
    @IBOutlet var flash: UIButton!    //flash button
    var frameOfTakePhoto:CGRect = CGRect()
    var trailingTake:NSLayoutConstraint = NSLayoutConstraint() // to move take button to right after pressed
    @IBOutlet var centerTake: NSLayoutConstraint!
    
    let longPressRec = UILongPressGestureRecognizer()
    
    //settting up AVFoundation
    var error:NSError?
    var captureSession: AVCaptureSession?
    var movieOutput: AVCaptureMovieFileOutput?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var backCameraDevice:AVCaptureDevice!
    var frontCameraDevice:AVCaptureDevice!
    var audioDevice:AVCaptureDevice!
    var activeDevice:AVCaptureDevice?
    var backInput = AVCaptureDeviceInput()
    var frontInput = AVCaptureDeviceInput()
    var audioInput = AVCaptureDeviceInput()
    var delegate : AVCaptureFileOutputRecordingDelegate?
    
    var player : AVPlayer? = nil
    var playerLayer : AVPlayerLayer? = nil
    var asset : AVAsset? = nil
    var playerItem: AVPlayerItem? = nil
    var didTakeVideo:Bool?
    
    //previewing video vs saving
    var highQualityURL:NSURL?
    var lowQualityData:NSData?
    var lowQualityUrl:NSURL?
    
    override func viewWillDisappear(animated: Bool) {
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        self.player?.pause()
    }
    
    
    deinit{
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    @IBOutlet var takePhotoView: UIView!
    
    var circleView:CircleView?
    
    func addCircleView() {
        
        // Create a new CircleView
        circleView = CircleView(frame: CGRect(x: self.takePhotoButton.frame.minX - 2, y: self.takePhotoButton.frame.minY - 2, width: self.takePhotoButton.frame.width + 4, height: self.takePhotoButton.frame.height + 4))
        
        self.takePhotoView.addSubview(circleView!)
        
        // Animates drawing of the circle over 1 second
        circleView!.animateCircle(10)
    }
    
    func muteNotification(notification:NSNotification){
        if notification.object as? UIViewController != self {
            
            dispatch_async(dispatch_get_main_queue()) {
                if muted == false {
                    self.muteItem.setImage(self.audioOnImage, forState: UIControlState.Normal)
                    
                    if let player = self.player {
                        player.muted = false
                    }
                } else {
                    self.muteItem.setImage(self.audioOffImage, forState: UIControlState.Normal)
                    if let player = self.player {
                        player.muted = true
                    }
                }
                
            }
        }
    }
    
    
    override func viewDidLoad() {
        
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        
        
        center.addObserver(self, selector: "stopAv:", name:"stopAv", object: nil)
        center.addObserver(self, selector: "startAv:", name:"startAv", object: nil)
        
        center.addObserver(self, selector: "muteNotification:", name:"muteNotification", object: nil)
        
        self.flashOn = false
        self.selfie = false
        muted = false
        
        picTitle.delegate = self
        self.sendPicTitle.addTarget(self, action: "sendPhoto:", forControlEvents: UIControlEvents.TouchUpInside)
        self.sendPicTitle.layer.borderColor = UIColor.blackColor().CGColor
        self.sendPicTitle.layer.borderWidth = 1
        
        //set up left and right options
        self.titles = deliveryTimeOptions
        self.buttons = [self.option1, self.option2, self.option3, self.option4]
        
        for var i:Int = 0; i < titles.count; ++i {
            
            if titles[i] == dayChoice {
                titles.removeAtIndex(i)
            }
        }
        
        for var i:Int = 0; i  < 4; ++i {
            buttons[i].backgroundColor = UIColor.lightGrayColor()
            buttons[i].setTitle(titles[i], forState: UIControlState.Normal)
            buttons[i].alpha = 0.8
            buttons[i].setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            buttons[i].setTitleColor(UIColor.highlightColor(), forState: UIControlState.Highlighted)
            buttons[i].titleLabel!.font = UIFont(name: "AvenirNext-Bold", size: 14)
            buttons[i].layer.borderColor = UIColor.highlightColor().CGColor
            buttons[i].layer.borderWidth = 2
            buttons[i].addTarget(self, action: "choseOption:", forControlEvents: UIControlEvents.TouchUpInside)
            buttons[i].hidden = false
            buttons[i].titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
        self.titles2 = timeOfDay
        self.buttons2 = [self.option12, self.option22, self.option32, self.option42]
        titlesLocal2 = self.titles2
        
        for var i:Int = 0; i < titlesLocal2.count; ++i {
            if titlesLocal2[i] == timeChoice {
                titlesLocal2.removeAtIndex(i)
            }
        }
        
        for var i:Int = 0; i  < 4; ++i {
            buttons2[i].backgroundColor = UIColor.lightGrayColor()
            buttons2[i].setTitle(titlesLocal2[i], forState: UIControlState.Normal)
            buttons2[i].alpha = 0.8
            buttons2[i].setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            buttons2[i].setTitleColor(UIColor.highlightColor(), forState: UIControlState.Highlighted)
            buttons2[i].titleLabel!.font = UIFont(name: "AvenirNext-Bold", size: 14)
            buttons2[i].layer.borderColor = UIColor.highlightColor().CGColor
            buttons2[i].layer.borderWidth = 2
            buttons2[i].addTarget(self, action: "choseOption2:", forControlEvents: UIControlEvents.TouchUpInside)
            buttons2[i].hidden = false
            buttons2[i].titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
        //assign front and back camera
        let availableCameraDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in availableCameraDevices as! [AVCaptureDevice] {
            if device.position == .Back {
                backCameraDevice = device
                do {
                    backInput = try AVCaptureDeviceInput(device: backCameraDevice)
                } catch let error1 as NSError {
                    error = error1
                }
            }
            else if device.position == .Front {
                frontCameraDevice = device
                do {
                    frontInput = try AVCaptureDeviceInput(device: frontCameraDevice)
                } catch let error1 as NSError {
                    error = error1
                }
                
            }
        }
        
        //set active camera to back and then call  setup function
        assignDevicesInputs()
        setUpCamera(self.backInput)
        addPhotoOutput()
        addVideoOutput()
        
        
        //set up take photo button
        self.trailingTake = NSLayoutConstraint(item: self.takePhotoButton, attribute:
            .TrailingMargin, relatedBy: .Equal, toItem: self.view,
            attribute: .TrailingMargin, multiplier: 1.0, constant: -5)
        self.frameOfTakePhoto = self.takePhotoButton.frame
        self.takeView.addConstraint(self.centerTake)
        self.view.removeConstraint(self.trailingTake)
        self.takePhotoButton.alpha = 1.0
        self.takePhotoButton.backgroundColor = UIColor.lightGrayColor()
        self.takePhotoButton.setTitle("", forState: UIControlState.Normal)
        self.takePhotoButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.takePhotoButton.titleLabel!.font = UIFont(name: "AvenirNext-Regular", size: 18)
        self.takePhotoButton.layer.borderColor = UIColor.highlightColor().CGColor
        self.takePhotoButton.layer.borderWidth = 4
        self.takePhotoButton.addTarget(self, action: "didPressTakePhoto:", forControlEvents: UIControlEvents.TouchUpInside)
        self.longPressRec.addTarget(self, action: "didPressRecord:")
        self.takePhotoButton.addGestureRecognizer(self.longPressRec)
        
        
        self.takePhotoButton.hidden = false
        
        
        //set up flip camera/flash buttons
        self.flipCamera.addTarget(self, action: "flipCamera:", forControlEvents: UIControlEvents.TouchUpInside)
        self.flipCamera.hidden = false
        self.flash.hidden = false
        self.reload.hidden = true
        
        
        //set up delivery date button - note that that I could make a style function to clean this code up
        self.deliveryTime.alpha = 0.8
        self.deliveryTime.setTitle(dayChoice, forState: UIControlState.Normal)
        self.deliveryTime.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.deliveryTime.setTitleColor(UIColor.highlightColor(), forState: UIControlState.Highlighted)
        self.deliveryTime.layer.borderColor = UIColor.highlightColor().CGColor
        self.deliveryTime.addTarget(self, action: "showLeft", forControlEvents: UIControlEvents.TouchUpInside)
        self.deliveryTime.hidden = false
        self.deliveryTime.layer.borderColor = UIColor.highlightColor().CGColor
        self.deliveryTime.layer.borderWidth = 2
        self.deliveryTime.backgroundColor = UIColor.lightGrayColor()
        
        //set up dream or memory button
        self.dreamOrMemory.alpha = 0.8
        self.dreamOrMemory.setTitle("MEMORY", forState: UIControlState.Normal)
        self.dreamOrMemory.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.dreamOrMemory.setTitleColor(UIColor.highlightColor(), forState: UIControlState.Highlighted)
        self.dreamOrMemory.layer.borderColor = UIColor.highlightColor().CGColor
        self.dreamOrMemory.addTarget(self, action:"showMiddle", forControlEvents: UIControlEvents.TouchUpInside)
        self.dreamOrMemory.hidden = false
        self.dreamOrMemory.layer.borderWidth = 2
        self.dreamOrMemory.backgroundColor = UIColor.lightGrayColor()
        
        //set up time of delivery button
        self.timeOfDelivery.alpha = 0.8
        self.timeOfDelivery.titleLabel?.adjustsFontSizeToFitWidth = true
        self.timeOfDelivery.setTitle(timeChoice, forState: UIControlState.Normal)
        self.timeOfDelivery.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.timeOfDelivery.setTitleColor(UIColor.highlightColor(), forState: UIControlState.Highlighted)
        self.timeOfDelivery.layer.borderColor = UIColor.highlightColor().CGColor
        self.timeOfDelivery.addTarget(self, action:"showRight", forControlEvents: UIControlEvents.TouchUpInside)
        self.timeOfDelivery.hidden = false
        self.timeOfDelivery.layer.borderWidth = 2
        self.timeOfDelivery.backgroundColor = UIColor.lightGrayColor()
        
        self.buttonsLeft.hidden = true
        self.buttonsRight.hidden = true
        
        //set up text box that has title of photo
        self.picTitle!.placeholder = "say something interesting..."
        self.picTitle!.userInteractionEnabled = true
        self.picTitle!.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.sendPicTitle.alpha = 0
        self.picTitle!.hidden = true
        self.picTitle!.delegate = self
        
        //date picker
        self.myDatePicker.alpha = 0.8
        self.myDatePicker.layer.borderColor = UIColor.highlightColor().CGColor
        self.myDatePicker.layer.borderWidth = 1
        self.myDatePicker.backgroundColor = UIColor.lightGrayColor()
        self.myDatePicker.hidden = true
        
        
        self.selectDate.alpha = 0.8
        self.selectDate.backgroundColor = UIColor.lightGrayColor()
        self.selectDate.setTitle("SELECT", forState: UIControlState.Normal)
        self.selectDate.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.selectDate.titleLabel!.font = UIFont(name: "AvenirNext-Bold", size: 14)
        self.selectDate.layer.borderColor = UIColor.highlightColor().CGColor
        self.selectDate.layer.borderWidth = 1
        self.selectDate.hidden = true
        
        // if signed off and back on
        do {
            try backCameraDevice?.lockForConfiguration()
        } catch _ {
        }
        backCameraDevice?.flashMode = AVCaptureFlashMode.Off
        backCameraDevice?.unlockForConfiguration()
        self.flash.setImage(UIImage(named: "flashOff"), forState: UIControlState.Normal)
    
    }
    override func viewDidLayoutSubviews() {
        self.takePhotoButton.layer.cornerRadius = 0.5 * self.takePhotoButton.frame.size.height
        
        self.myDatePicker.frame = CGRectMake(self.buttonsLeft.frame.minX, self.view.bounds.height/4, self.dreamOrMemory.frame.maxX - self.buttonsLeft.frame.minX, 0.25 * self.view.bounds.width)
        self.selectDate.frame = CGRectMake(self.buttonsRight.frame.minX, self.myDatePicker.frame.maxY - self.myDatePicker.frame.height/2-20, self.buttonsRight.frame.width, 40)
        self.muteItem.layer.cornerRadius = 0.5 * self.muteItem.frame.size.height
        self.reload.layer.cornerRadius = 0.5 * self.muteItem.frame.size.height
        self.flash.layer.cornerRadius = 0.5 * self.flash.frame.size.height
        self.flipCamera.layer.cornerRadius = 0.5 * self.flipCamera.frame.size.height
        
        previewLayer?.frame = CGRectMake(0, 0, self.innerView!.frame.width, self.innerView!.frame.height)

        
    }
    
    //assign devices
    func assignDevicesInputs(){
        //assign front and back camera devices and inputs
        let availableCameraDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in availableCameraDevices as! [AVCaptureDevice] {
            if device.position == .Back {
                backCameraDevice = device
                do {
                    backInput = try AVCaptureDeviceInput(device: backCameraDevice)
                } catch let error1 as NSError {
                    error = error1
                }
            }
            else if device.position == .Front {
                frontCameraDevice = device
                do {
                    frontInput = try AVCaptureDeviceInput(device: frontCameraDevice)
                } catch let error1 as NSError {
                    error = error1
                }
            }
        }
        //audio device and input
        let audioDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeAudio)
        for device in audioDevices as! [AVCaptureDevice] {
            audioDevice = device
            do {
                audioInput = try AVCaptureDeviceInput(device: audioDevice)
            } catch let error1 as NSError {
                error = error1
            }
        }
    }
    
    //setup AV capture session
    func setUpCamera(input: AVCaptureDeviceInput?){
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetHigh
        if captureSession!.canAddInput(input) {
            captureSession!.addInput(input)
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
            previewLayer?.frame = CGRectMake(0, 0, self.innerView!.frame.width, self.innerView!.frame.height)
            self.innerView.layer.insertSublayer(previewLayer!, atIndex: 1)
            captureSession!.startRunning()
        }
    }
    
    func addPhotoOutput() {
        stillImageOutput = AVCaptureStillImageOutput()
        stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        if captureSession!.canAddOutput(stillImageOutput) {
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession!.addOutput(stillImageOutput)
        }
    }
    
    func addVideoOutput(){

        movieOutput = AVCaptureMovieFileOutput()
        if captureSession!.canAddOutput(movieOutput) {
            captureSession!.addOutput(movieOutput)
            delegate = self
        }
        
    }
    
    //flash toggle
    @IBAction func flash(sender: AnyObject) {
        if flashOn == false && selfie == false {
            flashOn = true
            do {
                try backCameraDevice?.lockForConfiguration()
            } catch _ {
            }
            backCameraDevice!.flashMode = AVCaptureFlashMode.On
            backCameraDevice?.unlockForConfiguration()
            
            self.flash.setImage(UIImage(named: "flashOn"), forState: UIControlState.Normal)
            
        } else if flashOn == true && selfie == false {
            flashOn = false
            do {
                try backCameraDevice?.lockForConfiguration()
            } catch _ {
            }
            backCameraDevice!.flashMode = AVCaptureFlashMode.Off
            backCameraDevice?.unlockForConfiguration()
            self.flash.setImage(UIImage(named: "flashOff"), forState: UIControlState.Normal)
            
        }
    }
    
    func flipCamera(sender: AnyObject) {
        if (self.captureSession?.canAddInput(frontInput) != nil) && self.selfie == false  {
            self.selfie = true
            
            if flashOn == true {
                flashOn == false
                self.flash.setImage(UIImage(named: "flashOff"), forState: UIControlState.Normal)
                
                
            }
            self.captureSession?.beginConfiguration()
            self.captureSession?.removeInput(backInput)
            self.captureSession?.addInput(frontInput)
            self.captureSession?.commitConfiguration()
            
        } else if self.selfie == true && (self.captureSession?.canAddInput(backInput) != nil){
            self.selfie = false
            
            self.captureSession?.beginConfiguration()
            self.captureSession?.removeInput(frontInput)
            self.captureSession?.addInput(backInput)
            self.captureSession?.commitConfiguration()
        }
    }
    
    
    func didPressRecord(gestureRecognizer:UILongPressGestureRecognizer) {

        if callCenter.currentCalls == nil {
            if captureSession!.canAddInput(audioInput) {
                captureSession!.addInput(audioInput)
            }
            if gestureRecognizer.state == UIGestureRecognizerState.Began {
                
                if let _ = movieOutput!.connectionWithMediaType(AVMediaTypeVideo) {
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
                    let outputPath = "\(documentsPath)/output.mov"
                    let outputFileUrl = NSURL(fileURLWithPath: outputPath)
                    let error2 = NSErrorPointer()
                    let fileManager = NSFileManager.defaultManager()
                    if fileManager.fileExistsAtPath(outputPath) {
                        do {
                            try fileManager.removeItemAtPath(outputPath)
                        } catch let error as NSError {
                            error2.memory = error
                        }
                    }
                    self.movieOutput!.maxRecordedDuration = CMTimeMake(10, 1)
                    self.movieOutput!.startRecordingToOutputFileURL(outputFileUrl, recordingDelegate: delegate)
                }
            }
            if gestureRecognizer.state == UIGestureRecognizerState.Ended || gestureRecognizer.state == UIGestureRecognizerState.Cancelled {
                circleView?.removeFromSuperview()
                if let _ = movieOutput {
                    self.movieOutput!.stopRecording()
                }
            }
        }
    }
    
    
    
    func stopAv(notification: NSNotification){
        captureSession?.stopRunning()
        captureSession?.removeInput(audioInput)
        captureSession?.removeOutput(movieOutput)
    }
    
    func startAv(notification: NSNotification){
        if captureSession?.running != true {
            addVideoOutput()
            captureSession?.startRunning()
        }
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        
        takePhotoButton.layer.borderWidth = 0
        addCircleView()
        
        if flashOn == true{
            do {
                try backCameraDevice.lockForConfiguration()
            } catch _ {
            }
            backCameraDevice.torchMode = AVCaptureTorchMode.On
            backCameraDevice.unlockForConfiguration()
        } else {
            do {
                try backCameraDevice.lockForConfiguration()
            } catch _ {
            }
            backCameraDevice.torchMode = AVCaptureTorchMode.Off
            backCameraDevice.unlockForConfiguration()
        }
        
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!){
        
        takePhotoButton.layer.borderColor = UIColor.highlightColor().CGColor
        takePhotoButton.layer.borderWidth = 4
        takePhotoButton.hidden = true
        flash.hidden = true
        flipCamera.hidden = true
        
        if backCameraDevice.torchMode == .On {
            do {
                try backCameraDevice.lockForConfiguration()
            } catch _ {
            }
            backCameraDevice.torchMode = AVCaptureTorchMode.Off
            backCameraDevice.unlockForConfiguration()
        }
        writeVideoToAssetsLibrary(outputFileURL)
    }
    
    func writeVideoToAssetsLibrary(videoUrl: NSURL) {
        
        let videoAsset: AVAsset = AVAsset(URL: videoUrl)
        let clipVideoTrack:AVAssetTrack = videoAsset.tracksWithMediaType(AVMediaTypeVideo).first!
        let composition = AVMutableComposition()
        composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height*1.3)
        videoComposition.frameDuration = CMTimeMake(1, 30)
        
        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30))
        
        let transform1: CGAffineTransform = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, -(0.15 * clipVideoTrack.naturalSize.width))
        let transform2 = CGAffineTransformRotate(transform1, CGFloat(M_PI_2))
        let finalTransform = transform2

        transformer.setTransform(finalTransform, atTime: kCMTimeZero)
        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]
        
        let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        exporter!.videoComposition = videoComposition
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let outputPath = "\(documentsPath)/output2.mov"
        let outputFileUrl = NSURL(fileURLWithPath: outputPath)
        
        let error2 = NSErrorPointer()
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(outputPath) {
            do {
                try fileManager.removeItemAtPath(outputPath)
            } catch let error as NSError {
                error2.memory = error
            }
            
        }
        
        exporter!.outputURL = outputFileUrl
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({ () -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                self.handleExportCompletion(exporter!)
            })
        })
        
    }
    
    func compressToLowQuality(videoUrl: NSURL, completion:NSURL -> ()){
        
        let videoAsset: AVAsset = AVAsset(URL: videoUrl)
        var exporter:AVAssetExportSession?
        if self.dreamOrMemory.titleLabel == "DREAM" {
            exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        }else {
            exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPreset960x540)
        }
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let outputPath = "\(documentsPath)/output3.mov"
        let outputFileUrl = NSURL(fileURLWithPath: outputPath)
        
        let error2 = NSErrorPointer()
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(outputPath) {
            do {
                try fileManager.removeItemAtPath(outputPath)
            } catch let error as NSError {
                error2.memory = error
            }
        }
        
        exporter!.outputURL = outputFileUrl
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({ () -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                
                completion(exporter!.outputURL!)
                
            })
        })
    }
    
    
    func handleExportCompletion(session: AVAssetExportSession) {
        
        playVideo(session.outputURL!)
        highQualityURL = session.outputURL!
        
        //hiding camera preview and buttons
        self.previewLayer?.removeFromSuperlayer()
        self.flipCamera.hidden = true
        self.flash.hidden = true
        self.buttonsRight.hidden = true
        self.buttonsLeft.hidden = true
        
        //move button to right
        self.takeView.removeConstraint(self.centerTake)
        self.view.addConstraint(self.trailingTake)
        self.takePhotoButton.setTitle("SEND", forState: UIControlState.Normal)
        self.takePhotoButton.removeTarget(self, action: "didTakePhoto", forControlEvents: UIControlEvents.TouchUpInside)
        self.takePhotoButton.removeGestureRecognizer(self.longPressRec)
        self.takePhotoButton.addTarget(self, action: "sendPhoto:", forControlEvents: UIControlEvents.TouchUpInside)
        self.picTitle!.hidden = false
        self.takePhotoButton.hidden = false
        
        self.didTakeVideo = true

        if self.timeChoice == "CUSTOM" && self.dayChoice == "TODAY" {
    
            let changeTime = self.customTime!.compare(NSDate())
            if changeTime == .OrderedAscending {
                
                self.customTime = NSDate()
                
                let formatter = NSDateFormatter()
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                
                let dateString = formatter.stringFromDate(self.customTime!)
                
                self.timeOfDelivery.setTitle(dateString, forState: UIControlState.Normal)
            }
        }
        
    }
    
    //play video
    func playVideo(croppedUrl: NSURL) {
        
        let videoURL = croppedUrl
        asset = AVAsset(URL: videoURL)
        playerItem = AVPlayerItem(asset: asset!)
        player = AVPlayer(playerItem: self.playerItem!)
        
        if let player = player {
            playerLayer = AVPlayerLayer(player: self.player)
            playerLayer!.frame = CGRectMake(0, 0, imageCapturedView.frame.width, imageCapturedView.frame.height)
            
            self.goLeft.hidden = true
            self.goRight.hidden = true
            self.reload.hidden = false
            self.picTitle!.hidden = false
            self.takePhotoButton.hidden = false
            
            self.imageCapturedView.addSubview(self.picTitle!)
            self.imageCapturedView.addSubview(self.sendPicTitle!)
            self.imageCapturedView.alpha = 1.0
            
            self.imageCapturedView.layer.insertSublayer(self.playerLayer!, atIndex: 0)
            
            player.actionAtItemEnd = .None
            NSNotificationCenter.defaultCenter().addObserver(self,
                selector: "restartVideoFromBeginning",
                name: AVPlayerItemDidPlayToEndTimeNotification,
                object: player.currentItem)
            if muted == true {
                player.muted = true
            }
            self.view.addSubview(buttonsLeft)
            self.view.addSubview(buttonsRight)
            self.view.addSubview(muteItem)
            self.view.addSubview(reload)
            player.play()
        }
    }
    
    //loop video
    func restartVideoFromBeginning()  {
        let seconds : Int64 = 0
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(seconds, preferredTimeScale)
        player!.seekToTime(seekTime)
        player!.play()
    }
    
    var viewImage:UIImageView?
    
    
    //taking and setting photo and moving buttons/look
    func didPressTakePhoto(sender: AnyObject) {
        if let videoConnection = stillImageOutput!.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    if self.selfie == true {
                        self.image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.LeftMirrored)
                        
                    }else {
                        self.image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                    }
                    
                    self.viewImage = UIImageView()
                    self.viewImage!.frame = CGRectMake(0, 0, self.innerView.frame.width, self.innerView.frame.height)
                    self.innerView.insertSubview(self.viewImage!, atIndex: 0)
                    self.viewImage!.image = self.image
                    
                    self.innerView.backgroundColor = UIColor(patternImage: self.image)
                    
                    self.image = ImageUtil.cropToSquare(image: self.image)
                    self.imageCaptured.hidden = false
                    self.imageCaptured.frame = CGRectMake(0, 0, self.imageCapturedView.frame.width, self.imageCapturedView.frame.height)
                    self.imageCaptured.image = self.image
                    self.imageCapturedView.addSubview(self.imageCaptured)
                    self.imageCapturedView.alpha = 1.0
                    self.flipCamera.hidden = true
                    self.flash.hidden = true
                    self.reload.hidden = false
                    self.goLeft.hidden = true
                    self.goRight.hidden = true
                    
                    self.buttonsRight.hidden = true
                    self.buttonsLeft.hidden = true
                    //move over button
                    self.takeView.removeConstraint(self.centerTake)
                    self.view.addConstraint(self.trailingTake)
                    self.takePhotoButton.setTitle("SEND", forState: UIControlState.Normal)
                    self.takePhotoButton.removeTarget(self, action: "didTakePhoto", forControlEvents: UIControlEvents.TouchUpInside)
                    self.takePhotoButton.removeGestureRecognizer(self.longPressRec)
                    self.takePhotoButton.addTarget(self, action: "sendPhoto:", forControlEvents: UIControlEvents.TouchUpInside)
                    self.picTitle!.hidden = false
                    self.imageCapturedView.addSubview(self.picTitle!)
                    self.imageCapturedView.addSubview(self.sendPicTitle!)
                    self.previewLayer?.removeFromSuperlayer()
                    self.didTakePhoto = true
                    
                    if self.timeChoice == "CUSTOM" && self.dayChoice == "TODAY" {
                        
                        let changeTime = self.customTime!.compare(NSDate())
                        if changeTime == .OrderedAscending {
                            
                            self.customTime = NSDate()
                            
                            let formatter = NSDateFormatter()
                            formatter.dateStyle = NSDateFormatterStyle.NoStyle
                            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                            
                            let dateString = formatter.stringFromDate(self.customTime!)
                            
                            self.timeOfDelivery.setTitle(dateString, forState: UIControlState.Normal)
                        }
                    }
                    
                }
            })
        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var shouldChange = false
        
        if textField.text!.characters.count + string.characters.count < 140 {
            shouldChange = true
        }
        
        return shouldChange
    }
    
    func showLeft(){

        self.buttonsLeft.hidden = false
        self.buttonsRight.hidden = true
        self.myDatePicker.hidden = true
        self.selectDate.hidden = true
        
    }
    
    func showRight(){
        
        self.buttonsRight.hidden = false
        self.buttonsLeft.hidden = true
        self.myDatePicker.hidden = true
        self.selectDate.hidden = true
    }
    
    func showMiddle(){
        if self.dreamOrMemory.titleLabel!.text ==  "MEMORY" {
            self.dreamOrMemory.setTitle("DREAM", forState: UIControlState.Normal)
            self.dreamOrMemory.setTitleColor(UIColor.highlightColor(), forState: UIControlState.Normal)
            self.dreamOrMemory.titleLabel!.font = UIFont(name: "AvenirNext-Bold", size: 20)
        }else {
            self.dreamOrMemory.setTitle("MEMORY", forState: UIControlState.Normal)
            self.dreamOrMemory.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.dreamOrMemory.titleLabel!.font = UIFont(name: "AvenirNext-Bold", size: 18)
        }
        buttonsLeft.hidden = true
        buttonsRight.hidden = true
        self.myDatePicker.hidden = true
        self.selectDate.hidden = true
    }
    
    //choose delivery date (left side)
    func choseOption(sender:UIButton){
        dayChoice = sender.titleLabel!.text!
        if dayChoice == "CUSTOM" {
            self.view.addSubview(self.myDatePicker)
            self.view.addSubview(self.selectDate)
            self.myDatePicker.datePickerMode = UIDatePickerMode.Date
            let currentDate = NSDate()
            self.myDatePicker.minimumDate = currentDate
            self.myDatePicker.date = currentDate
            self.selectDate.removeTarget(self, action: "setCustomTime", forControlEvents: UIControlEvents.TouchUpInside)
            self.selectDate.addTarget(self, action: "setCustomDate", forControlEvents: UIControlEvents.TouchUpInside)
            myDatePicker.hidden = false
            selectDate.hidden = false
        } else {
            self.timeOfDelivery.addTarget(self, action:"showRight", forControlEvents: UIControlEvents.TouchUpInside)
            self.deliveryTime.setTitle(dayChoice, forState: UIControlState.Normal)
            self.titles = deliveryTimeOptions
            for var i:Int = 0; i < self.titles.count; ++i {
                if self.titles[i] == dayChoice {
                    self.titles.removeAtIndex(i)
                }
            }
            for var i:Int = 0; i  < 4; ++i {
                buttons[i].setTitle(titles[i], forState: UIControlState.Normal)
            }
        }
        
        buttonsLeft.hidden = true
    }
    
    //choose delivery time (right side)
    func choseOption2 (sender:UIButton){
        
        timeChoice = sender.titleLabel!.text!
        if timeChoice == "CUSTOM" || dayChoice == "TODAY" {
            self.view.addSubview(self.myDatePicker)
            self.view.addSubview(self.selectDate)
            if dayChoice == "TODAY" {
                
                let userCalendar = NSCalendar.currentCalendar()
                
                let dateComponentsTime:NSDateComponents = userCalendar.components([.Minute, .Hour, .Day, .Month, .Year], fromDate: NSDate())
                
                let timeNow = userCalendar.dateFromComponents(dateComponentsTime)!
                
                self.myDatePicker.minimumDate = timeNow
                self.myDatePicker.setDate(timeNow, animated: false)
            } else {
                self.myDatePicker.minimumDate = nil
            }
            self.myDatePicker.datePickerMode = UIDatePickerMode.Time
            self.selectDate.removeTarget(self, action: "setCustomDate", forControlEvents: UIControlEvents.TouchUpInside)
            self.selectDate.addTarget(self, action: "setCustomTime", forControlEvents: UIControlEvents.TouchUpInside)
            myDatePicker.hidden = false
            selectDate.hidden = false
        } else {
            self.timeOfDelivery.setTitle(timeChoice, forState: UIControlState.Normal)
            self.titlesLocal2 = timeOfDay
            for var i:Int = 0; i < self.titlesLocal2.count; ++i {
                if self.titlesLocal2[i] == timeChoice {
                    self.titlesLocal2.removeAtIndex(i)
                }
            }
            for var i:Int = 0; i  < 4; ++i {
                buttons2[i].setTitle(titlesLocal2[i], forState: UIControlState.Normal)
            }
            
        }
        
        buttonsRight.hidden = true
    }
    
    //set custome date and time
    func setCustomDate(){
        
        lastWasToday = false
        
        self.deliveryTime.setTitle(dayChoice, forState: UIControlState.Normal)
        self.titles = deliveryTimeOptions
        for var i:Int = 0; i < self.titles.count; ++i {
            if self.titles[i] == dayChoice {
                self.titles.removeAtIndex(i)
            }
        }
        for var i:Int = 0; i  < 4; ++i {
            buttons[i].setTitle(titles[i], forState: UIControlState.Normal)
        }
        
        self.customDate = self.myDatePicker.date
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        let userCalendar = NSCalendar.currentCalendar()
        
        
        let dateComponentsToday:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: NSDate())
        
        let dayToday = userCalendar.dateFromComponents(dateComponentsToday)!
        
        let dayTomorrow = userCalendar.dateByAddingUnit(
            .Day,
            value: 1,
            toDate: dayToday,
            options: [])!
        
        let dateComponentsPicker:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: myDatePicker.date)
        
        let dayPicker = userCalendar.dateFromComponents(dateComponentsPicker)!
        
        self.selectDate.hidden = true
        self.myDatePicker.hidden = true
        
        if dayToday == dayPicker {
            
            
            let dateComponentsTime:NSDateComponents = userCalendar.components([.Minute, .Hour, .Day, .Month, .Year], fromDate: NSDate())
            
            let timeNow = userCalendar.dateFromComponents(dateComponentsTime)!
            
            self.customTime = timeNow
            lastWasToday = true
            self.timeChoice = "CUSTOM"
            self.deliveryTime.setTitle("TODAY", forState: UIControlState.Normal)
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.NoStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            let dateString = formatter.stringFromDate(NSDate())
            self.timeOfDelivery.setTitle(dateString, forState: UIControlState.Normal)
            
            self.titlesLocal2 = timeOfDay
            for var i:Int = 0; i < self.titlesLocal2.count; ++i {
                if self.titlesLocal2[i] == timeChoice {
                    self.titlesLocal2.removeAtIndex(i)
                }
            }
            self.timeOfDelivery.removeTarget(self, action:"showRight", forControlEvents: UIControlEvents.TouchUpInside)
            self.timeOfDelivery.addTarget(self, action: "choseOption2:", forControlEvents: UIControlEvents.TouchUpInside)
            self.timeNSDate = self.myDatePicker.date
            
            let formatter2 = NSDateFormatter()
            formatter2.dateStyle = NSDateFormatterStyle.NoStyle
            formatter2.timeStyle = NSDateFormatterStyle.ShortStyle
            
            let dateString2 = formatter2.stringFromDate(self.customTime!)
            
            self.timeOfDelivery.setTitle(dateString2, forState: UIControlState.Normal)
            
            dayChoice = "TODAY"
            
            
        } else if dayTomorrow == dayPicker {
            
            self.deliveryTime.setTitle("TOMORROW", forState: UIControlState.Normal)
            
            
            dayChoice = "TOMORROW"
            
        } else {
            let dateString:String = formatter.stringFromDate(self.customDate!)
            let dateStyler = NSDateFormatter()
            dateStyler.dateFormat = "yyyy-MM-dd"
            
            let myDate = self.customDate
            
            let myWeekday = NSCalendar.currentCalendar().components(NSCalendarUnit.Weekday, fromDate: myDate!).weekday
            
            var myWeekdayConverted:String?
            
            switch(myWeekday) {
            case 1:
                myWeekdayConverted = "Sun"
            case 2:
                myWeekdayConverted = "Mon"
            case 3:
                myWeekdayConverted = "Tue"
            case 4:
                myWeekdayConverted = "Wed"
            case 5:
                myWeekdayConverted = "Thu"
            case 6:
                myWeekdayConverted = "Fri"
            case 7:
                myWeekdayConverted = "Sat"
            default:
                myWeekdayConverted = "error"
            }
            
            let combo = myWeekdayConverted! + " " + dateString
            self.deliveryTime.setTitle(combo, forState: UIControlState.Normal)
            
            dayChoice = dateString
        }
    }
    
    func setCustomTime(){
        
        self.titlesLocal2 = timeOfDay
        for var i:Int = 0; i < self.titlesLocal2.count; ++i {
            if self.titlesLocal2[i] == timeChoice {
                self.titlesLocal2.removeAtIndex(i)
            }
        }
        for var i:Int = 0; i  < 4; ++i {
            buttons2[i].setTitle(titlesLocal2[i], forState: UIControlState.Normal)
        }
        
        
        self.timeNSDate = self.myDatePicker.date
        self.customTime = self.myDatePicker.date
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.NoStyle
        formatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        let dateString = formatter.stringFromDate(self.customTime!)
        
        
        
        
        
        self.timeOfDelivery.setTitle(dateString, forState: UIControlState.Normal)
        self.selectDate.hidden = true
        self.myDatePicker.hidden = true
        timeChoice = "CUSTOM"
    }
    
    //close drop down menus if touch outside of them
    override func  touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if self.buttonsLeft.hidden == false {
            let touch:UITouch = touches.first!
            let location = touch.locationInView(self.parentViewController?.view)
            if self.buttonsLeft.frame.contains(location){
                
            }else{
                self.buttonsLeft.hidden = true
            }
        }
        if self.buttonsRight.hidden == false {
            let touch = touches.first!
            let location = touch.locationInView(self.parentViewController?.view)
            if self.buttonsRight.frame.contains(location){
                
            }else{
                self.buttonsRight.hidden = true
            }
        }
        
        if self.myDatePicker.hidden == false {
            let touch = touches.first!
            let location = touch.locationInView(self.parentViewController?.view)
            if self.myDatePicker.frame.contains(location) || self.selectDate.frame.contains(location){
                
            }else{
                self.myDatePicker.hidden = true
                self.selectDate.hidden = true
                
            }
        }
    }
    
    func sendPhoto(send: AnyObject) {
        if didTakeVideo == true {
            compressToLowQuality(highQualityURL!){ (completion) in
                self.lowQualityUrl = completion
                self.lowQualityData = NSData(contentsOfURL: self.lowQualityUrl!)
                self.performSegueWithIdentifier("whoReceives", sender: self)
                
            }
            
        } else if didTakePhoto == true {
            self.performSegueWithIdentifier("whoReceives", sender: self)
        }
        self.view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "whoReceives") {
            var imageToSend = segue.destinationViewController as! ReceiversViewController;
            
            if self.didTakePhoto {
                imageToSend.imageFromCamera = image
                imageToSend.video = false
            } else {
                imageToSend.lowQualityUrl = lowQualityUrl!
                imageToSend.videoData = lowQualityData!
                imageToSend.video = true
            }
            
            imageToSend.picTitle = picTitle
            
            
            let selectedDateInput = self.deliveryTime.titleLabel?.text
            let selectedTimeInput = self.timeOfDelivery.titleLabel?.text
            let storeMemory = self.dreamOrMemory.titleLabel?.text
            
            
            if storeMemory == "DREAM" {
                memory = false
            } else {
                memory = true
            }
            
            imageToSend.isMemory = memory
            
            func randRange (lower: Int , upper: Int) -> Int {
                return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
            }
            
            var hour = Int()
            
            var minute = randRange(0, upper: 59)
            var second = randRange(0, upper: 59)
            let userCalendar = NSCalendar.currentCalendar()
            
            
            if selectedTimeInput == "MORNING" {
                hour = randRange(6,upper: 12)
            }else if selectedTimeInput == "AFTERNOON" {
                hour = randRange(12,upper: 18)
            }else if selectedTimeInput == "NIGHT" {
                hour = randRange(18,upper: 24)
            }else if selectedTimeInput == "LATE NIGHT" {
                hour = randRange(0,upper: 6)
            }else{
                
                hour = Int(userCalendar.component(.Hour, fromDate: self.customTime!))
                minute = Int(userCalendar.component(.Minute, fromDate: self.customTime!))
                second = 0
            }
            
            
            var date:NSDate?
            
            
            if selectedDateInput == "MONDAY" {
                let dateComponentsWeekDay:NSDateComponents = userCalendar.components(.Weekday, fromDate: NSDate())
                if dateComponentsWeekDay.weekday != 2 {
                    date = userCalendar.dateBySettingUnit(NSCalendarUnit.Weekday, value: 2, ofDate: NSDate(), options: [])!
                } else {
                    date = userCalendar.dateByAddingUnit(
                        .Day,
                        value: 7,
                        toDate: NSDate(),
                        options: [])!
                }
                
            }else if selectedDateInput == "1 WEEK" {
                date = userCalendar.dateByAddingUnit(
                    .Day,
                    value: 7,
                    toDate: NSDate(),
                    options: [])!
            }else if selectedDateInput == "1 MONTH" {
                date = userCalendar.dateByAddingUnit(
                    .Month,
                    value: 1,
                    toDate: NSDate(),
                    options: [])!
            }else if selectedDateInput == "1 YEAR" {
                date = userCalendar.dateByAddingUnit(
                    .Year,
                    value: 1,
                    toDate: NSDate(),
                    options: [])!
            }else{
                
                date = self.customDate as NSDate?
                
            }
            
            let dateComponents = NSDateComponents()
            dateComponents.second = second
            dateComponents.minute = minute
            dateComponents.hour = hour
            dateComponents.day = Int(userCalendar.component(.Day, fromDate: date!))
            dateComponents.month = Int(userCalendar.component(.Month, fromDate: date!))
            dateComponents.year = userCalendar.component(.Year, fromDate: date!)
            
            let dateToSend = userCalendar.dateFromComponents(dateComponents)!
            

            
            imageToSend.unlockDate = dateToSend
            
        }
    }
    
    
    
    func reloadCamera(){
        self.navigationController!.navigationBar.hidden = true
        func setAppearance(){
            self.goLeft.hidden = false
            self.goRight.hidden = false
            self.flipCamera.hidden = false
            self.flash.hidden = false
            self.view.removeConstraint(self.trailingTake)
            self.takeView.addConstraint(self.centerTake)
            self.takePhotoButton.setTitle("", forState: UIControlState.Normal)
            self.takePhotoButton.addGestureRecognizer(self.longPressRec)
            self.takePhotoButton.removeTarget(self, action: "sendPhoto:", forControlEvents: UIControlEvents.TouchUpInside)
            self.takePhotoButton.addTarget(self, action: "didPressTakePhoto:", forControlEvents: UIControlEvents.TouchUpInside)
            self.picTitle!.placeholder = "say something interesting..."
            self.picTitle!.hidden = true
            self.sendPicTitle.alpha = 0
            self.picTitle!.text = ""
        }
        
        if self.didTakeVideo == true {
            self.didTakeVideo = false
            self.player?.pause()
            self.playerLayer?.removeFromSuperlayer()
            self.innerView.layer.insertSublayer(previewLayer!, atIndex: 1)
            setAppearance()
        } else if self.didTakePhoto == true {
            self.viewImage?.removeFromSuperview()
            self.didTakePhoto = false
            self.imageCaptured.hidden = true
            self.innerView.layer.insertSublayer(previewLayer!, atIndex: 1)
            setAppearance()
        }
        
        self.picTitle.resignFirstResponder()
        self.navigationController!.navigationBar.hidden = true
        
        self.takeView.updateConstraints()
        self.takePhotoButton.layer.cornerRadius = 0.5 * self.takePhotoButton.frame.size.height
        self.takePhotoButton.clipsToBounds = true
        self.muteItem.layer.cornerRadius = 0.5 * self.muteItem.frame.size.height
        self.flash.layer.cornerRadius = 0.5 * self.flash.frame.size.height
        self.flipCamera.layer.cornerRadius = 0.5 * self.flipCamera.frame.size.height
        self.reload.hidden = true
        self.takePhotoButton.layer.cornerRadius = 0.5 * self.takePhotoButton.frame.size.height
        self.takePhotoButton.clipsToBounds = true
        self.takePhotoButton.hidden = false
        flash.hidden = false
        flipCamera.hidden = false
        
        
        if deliveryTime.titleLabel!.text == "TODAY" {
            if timeChoice == "CUSTOM" {
                if let earlyTime = timeNSDate {
                    let unlock = earlyTime.compare(NSDate())
                    if unlock == .OrderedAscending {
                        let formatter = NSDateFormatter()
                        formatter.dateStyle = NSDateFormatterStyle.NoStyle
                        formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                        
                        let dateString = formatter.stringFromDate(NSDate())
                        self.timeOfDelivery.setTitle(dateString, forState: UIControlState.Normal)
                        
                        self.titlesLocal2 = timeOfDay
                        for var i:Int = 0; i < self.titlesLocal2.count; ++i {
                            if self.titlesLocal2[i] == timeChoice {
                                self.titlesLocal2.removeAtIndex(i)
                            }
                        }
                        for var i:Int = 0; i  < 4; ++i {
                            buttons2[i].setTitle(titlesLocal2[i], forState: UIControlState.Normal)
                        }
                        
                        
                    }
                    
                }
            }
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        reloadCamera()
        
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillChangeFrameNotification, object: nil)
        center.addObserver(self, selector: "keyboardNotificationHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        if muted == true {
            muteItem.setImage(audioOffImage, forState: UIControlState.Normal)
        } else {
            muteItem.setImage(audioOnImage, forState: UIControlState.Normal)
        }
        
    }
    
    //keyboard comes up
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue()
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardSpacing?.constant = endFrame!.size.height - self.takeView.frame.maxY
            keyboardSpacing2?.constant = endFrame!.size.height - self.takeView.frame.maxY
            self.sendPicTitle.alpha = 0.75
    
            textToSuper.constant = self.sendPicTitle.frame.width - 1

            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in

                }
            )
        }
    }
    //keyboard goes down
    func keyboardNotificationHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardSpacing?.constant = 0
            self.keyboardSpacing2?.constant = 0
            self.sendPicTitle.alpha = 0
            textToSuper.constant = 0
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in}
            )
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        
    }
    
    
}
