//  Created by Nick Scott

import UIKit
import CoreData

class BuddiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GroupDelegate, BuddiesDelegate, ContactsDelegate, BuddiesPushDelegate {

    static var delegate:ContainerView!

    @IBOutlet var addGroupButton: UIButton!
    @IBOutlet var addBuddyButton: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var buddiesOrGroups: UISegmentedControl!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var ofWLabel: UILabel!
    
    
    @IBOutlet var errorViewHeight: NSLayoutConstraint!
    @IBOutlet var errorLabel: UILabel!


    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var blurView:UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
    
    var user:PFUser?
    var buddies = [PFUser]()
    var groups = [PFObject]()
    var refresher = UIRefreshControl()
    var currentRow:Int? //set to -1 at first because no row shown on load
    var groupTable:Bool!

    
    //scroll to camerea
    @IBAction func right(sender: AnyObject) {
        BuddiesViewController.delegate!.disableScroll(ChildViews.B)
    }
    
    @IBAction func addBuddy(sender: AnyObject) {
        self.performSegueWithIdentifier("popup", sender: self)
    }
    
    @IBAction func addGroupAction(sender: AnyObject) {
        self.performSegueWithIdentifier("groupFromContacts", sender: self)
    }
    
    // table and toggle
    @IBAction func buddiesOrGroups(sender: AnyObject) {
        switch buddiesOrGroups.selectedSegmentIndex
        {
        case 0:
            if groupTable == true {
                tableView.endUpdates()
                self.ofWLabel.text = "of"
                self.currentRow = -1
                self.groupTable = false
                self.addGroupButton.hidden = true
                self.addBuddyButton.hidden = false
                self.tableView.reloadData()
            }
        case 1:
            if groupTable == false {
                tableView.endUpdates()
                self.ofWLabel.text = "w/"
                self.currentRow = -1
                self.groupTable = true
                self.addGroupButton.hidden = false
                self.addBuddyButton.hidden = true
                self.tableView.reloadData()
            }
        default:
            break;
        }
    }
    
    @IBAction func logout(sender: AnyObject) {
        
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        
        do {
            try PFObject.unpinAllObjects()
        } catch let error1 as NSError {
            print("Could not unpin \(error1), \(error1.userInfo)")
        }
        PFQuery.clearAllCachedResults()
        let installation = PFInstallation.currentInstallation()
        installation.removeObjectForKey("user")
        do {
            try installation.save()
        } catch let error1 as NSError {
            print("Could not save \(error1), \(error1.userInfo)")
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("LastQueryThread")
        defaults.removeObjectForKey("countAtLastQuery")
        defaults.removeObjectForKey("moreFlashbacks")
        defaults.removeObjectForKey("firstFlashback")
        defaults.removeObjectForKey("arrayEqualLastQuery")
        defaults.removeObjectForKey("arrayEqualFirstFlashback")
        ThreadsViewController.haveExtra = false
        
        
        deleteAllCore()
        
        PFUser.logOutInBackgroundWithBlock{
            (results) -> Void in
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            self.dismissViewControllerAnimated(false, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewControllerWithIdentifier("Main") 
            self.presentViewController(initialViewController, animated: true, completion: nil)
        }
    }
    
    func deleteAllCore(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext:NSManagedObjectContext = appDelegate.managedObjectContext!
        let fetchRequest1 = NSFetchRequest(entityName:"Threads")
        var error1:NSError?
        let fetchedResults1 = (try! managedContext.executeFetchRequest(fetchRequest1)) as? [Threads]
        if let results1 = fetchedResults1 {
            for result in results1 {
                managedContext.deleteObject(result)
            }
            do {
                try managedContext.save()
            } catch let error as NSError {
                error1 = error
                print("Could not save \(error1), \(error1?.userInfo)")
            }
            
        } else {
            print("Could not fetch \(error1), \(error1!.userInfo)")
        }
    }
    
    func addFromContacts(){
        self.performSegueWithIdentifier("fromContacts", sender: self)
    }
    
    func memories() {
        let memories = self.storyboard!.instantiateViewControllerWithIdentifier("memories") as! ViewAllViewController
        if groupTable == false {
            memories.buddy = buddies[currentRow!]
            memories.name = buddies[currentRow!].username
            memories.groupBool = false
            memories.group = nil
        } else {
            memories.group = groups[currentRow!]
            memories.buddy = nil
            let tempGroup:PFObject = groups[currentRow!]
            memories.name = (tempGroup["groupName"] as? String)!
            memories.groupBool = true
        }
        self.navigationController!.pushViewController(memories, animated: true)
    }
    
    override func viewDidLoad() {
        AppDelegate.delegateBuddies = self
        AddContactsViewController.delegate = self
        
        if let globalPush = GlobalOpenedByPushNotification {
            if globalPush == "group" {
                self.ofWLabel.text = "w/"
                self.currentRow = -1
                self.groupTable = true
                self.addGroupButton.hidden = false
                self.addBuddyButton.hidden = true
                self.tableView.reloadData()
                buddiesOrGroups.selectedSegmentIndex = 1
            } else {
                groupTable = false
                self.addGroupButton.hidden = true
                self.addBuddyButton.hidden = false
            }
                
        } else {
            groupTable = false
            self.addGroupButton.hidden = true
            self.addBuddyButton.hidden = false

        }

        user = PFUser.currentUser()! as PFUser
        self.usernameLabel.text = user!.username!

        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        refresher.tintColor = UIColor.highlightColor()
        tableView.addSubview(refresher)
        
        if GlobalSignInUp == true {
            populateBuddies()
            populateGroups()
        } else {
            populateBuddiesLocal()
            if let globalPush = GlobalOpenedByPushNotification {
                if globalPush == "group" {
                    populateGroups()
                    
                }
            } else {
                populateGroupsLocal()
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        addBuddyButton.layer.cornerRadius = 0.5 * addBuddyButton.frame.width
        addBuddyButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        addBuddyButton.layer.borderWidth = 1
        
        addGroupButton.layer.cornerRadius = 0.5 * addBuddyButton.frame.width
        addGroupButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        addGroupButton.layer.borderWidth = 1
    }
    
    func refresh(){
        if groupTable == false {
            populateBuddies()
        } else {
            populateGroups()
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }

    func populateGroups(){
        self.populateGroupsLocal()
        if Reachability.isConnectedToNetwork() {
            
            if self.errorViewHeight.constant == 20 {
                UIView.animateWithDuration(0.65, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations:
                    { () -> Void in
                        
                        self.errorViewHeight.constant = 0
                        self.view.layoutIfNeeded()
                        
                    }, completion: {finished in
                        
                })
            }
            
            ParseUtilFuncs.populateGroups{ (tempGroups, error) in
                if error == false {
                    self.groups = tempGroups!
                    self.currentRow = -1
                    if self.refresher.refreshing {
                        self.refresher.endRefreshing()
                    }
                    self.currentRow = -1
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                    }
                }else {
                    self.errorLabel.text = "oops! something went wrong!"
                    self.errorViewHeight.constant = 20
                    self.refresher.endRefreshing()
                    self.delay(3){
                        if !self.refresher.refreshing {
                            UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                { () -> Void in
                                    
                                    self.errorViewHeight.constant = 0
                                    self.view.layoutIfNeeded()
                                    
                                }, completion: {finished in
                            })
                        }
                    }
                    
                }

            }
        }else {
            self.errorLabel.text = "no internet connection"
            self.errorViewHeight.constant = 20
            self.refresher.endRefreshing()
            self.delay(3){
                if !self.refresher.refreshing {
                    UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.errorViewHeight.constant = 0
                            self.view.layoutIfNeeded()
                            
                        }, completion: {finished in
                    })
                }
            }
        }

    }
    
    
    
    func populateBuddies(){
        self.populateBuddiesLocal()
        if Reachability.isConnectedToNetwork() {
            
            if self.errorViewHeight.constant == 20 {
                UIView.animateWithDuration(0.65, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations:
                    { () -> Void in
                        
                        self.errorViewHeight.constant = 0
                        self.view.layoutIfNeeded()
                        
                    }, completion: {finished in
                        
                })
            }
            
            ParseUtilFuncs.populateBuddies{ (tempBuddiesGlobal, error) in
                if error == false {
                    self.buddies = tempBuddiesGlobal!
                    if self.refresher.refreshing {
                        self.refresher.endRefreshing()
                    }
                    self.currentRow = -1
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                    }
                
                } else {
                    self.errorLabel.text = "oops! something went wrong!"
                    self.errorViewHeight.constant = 20
                    self.refresher.endRefreshing()
                    self.delay(3){
                        if !self.refresher.refreshing {
                            UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                { () -> Void in
                                    
                                    self.errorViewHeight.constant = 0
                                    self.view.layoutIfNeeded()
                                    
                                }, completion: {finished in
                            })
                        }
                    }

                }
            }
        
        }else {
            self.errorLabel.text = "no internet connection"
            self.errorViewHeight.constant = 20
            self.refresher.endRefreshing()
            self.delay(3){
                if !self.refresher.refreshing {
                    UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.errorViewHeight.constant = 0
                            self.view.layoutIfNeeded()
                            
                        }, completion: {finished in
                    })
                }
            }
        }
    

    }
    
    func populateGroupsLocal(){
        ParseUtilFuncs.populateGroupsLocal{ (tempGroups) in
            self.groups = tempGroups
            
            self.currentRow = -1
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()

            }
        }
    }
    
    func populateBuddiesLocal(){
        
        ParseUtilFuncs.populateBuddiesLocal{ tempBuddiesGlobal in
            self.buddies = tempBuddiesGlobal
            
            self.currentRow = -1
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    //functions for showing outgoing and deleting
    func waiting(){
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        self.view.alpha = 0.75
    }
    
    func stopWatiting(){
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        self.view.alpha = 1.0
    }
    
    func delete(){
        let title = "Delete all outgoing flashbacks"
        let error = "This will delete all flashbacks that have not been delivered. Are you sure?"
        displayAlert(title, error: error)
    }
    
    func displayAlert(title:String, error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "no", style: UIAlertActionStyle.Cancel, handler: { action in
        }))
        alert.addAction(UIAlertAction(title: "yes", style: .Default, handler: { action in
            self.waiting()
            //Cloud Code deletes PicVideo if no activity points to it
            let deleteQuery = PFQuery(className: "Activity")
            if self.groupTable == false {
                deleteQuery.whereKey("user", equalTo: self.user!)
                deleteQuery.whereKey("receiver", equalTo: self.buddies[self.currentRow!])
                deleteQuery.whereKey("dateUnlocked", greaterThan: NSDate())
            } else {
                deleteQuery.whereKey("user", equalTo: self.user!)
                deleteQuery.whereKey("group", equalTo: self.self.groups[self.currentRow!])
                deleteQuery.whereKey("dateUnlocked", greaterThan: NSDate())
            }
            
            deleteQuery.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                if error == nil {
                    for object in objects! {
                        object.deleteEventually()
                    }
                    self.currentRow = -1
                    self.tableView.reloadData()
                    self.stopWatiting()
                } else {
                    self.stopWatiting()
                    let title = "Something went wrong :("
                    let error = "Should we try again?"
                    self.displayAlertFailedDelete(title, error: error)
                }
            }
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func displayAlertFailedDelete(title:String, error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "no", style: UIAlertActionStyle.Cancel, handler: { action in
        }))
        alert.addAction(UIAlertAction(title: "yes", style: .Default, handler: { action in
            self.delete()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var plus1:Int
        if groupTable == false {
            plus1 = buddies.count + 1
        } else {
            plus1 = groups.count + 1
        }
        return plus1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if groupTable == false {
            let cell:BuddiesCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! BuddiesCell
            if indexPath.row < self.buddies.count && indexPath.row < buddies.count {
                cell.accessoryType = UITableViewCellAccessoryType.DetailButton
                
                let name:String = self.buddies[indexPath.row].username!
                cell.name.text = name
                
                cell.delete.hidden = true
                cell.numberOfOutgoing.hidden = true
                
                cell.tintColor = UIColor.darkGrayColor()
                
                if indexPath.row == currentRow {
                    
                    cell.activityIndicator.hidden = false
                    cell.activityIndicator.color = UIColor.highlightColor()
                    cell.activityIndicator.startAnimating()

                    cell.memories.hidden = false
                    cell.memories.addTarget(self, action: "memories", forControlEvents: UIControlEvents.TouchUpInside)
                    
                    var number:Int = 0
                    let buddy:PFUser = self.buddies[indexPath.row]
                    let activityQuery = PFQuery(className: "Activity")
                    activityQuery.whereKey("user", equalTo: user!)
                    activityQuery.whereKey("receiver", equalTo: buddy)
                    activityQuery.whereKey("dateUnlocked", greaterThan: NSDate())
                    activityQuery.findObjectsInBackgroundWithBlock {
                        (objects, error) -> Void in
                        cell.activityIndicator.stopAnimating()
                        if error == nil {
                            cell.activityIndicator.hidden = true
                            for _ in objects! {
                                number++
                            }
                            cell.numberOfOutgoing.text = String(number)
                            cell.delete.hidden = true
                            cell.numberOfOutgoing.hidden = false
                            
                            if number > 0  {
                                cell.numberOfOutgoing.text = String(number)
                                cell.delete.hidden = false
                                cell.delete.addTarget(self, action: "delete", forControlEvents: UIControlEvents.TouchUpInside)
                            }
                        }else {
                            cell.activityIndicator.color = UIColor.redColor()
                            cell.activityIndicator.hidden = false
                            cell.numberOfOutgoing.text = ""

                            cell.delete.hidden = true
                            cell.numberOfOutgoing.hidden = false
                        }


                    }
                } else {
                    cell.memories.hidden = true
                    cell.activityIndicator.hidden = true

                }
            } else {

                cell.name.text = ""
                cell.accessoryType = UITableViewCellAccessoryType.None
                
            }
            
            return cell
        } else {
            let cell:BuddiesGroupCell = self.tableView.dequeueReusableCellWithIdentifier("groupCell") as! BuddiesGroupCell

            if indexPath.row < self.groups.count && indexPath.row < groups.count {
                
                let groupMembersPF:[PFUser] = self.groups[indexPath.row]["members"] as AnyObject? as! [PFUser]
                var members = [String]()
                for member in groupMembersPF {
                    if member.username! != user?.username! {
                        members.append(member.username!)
                    }
                }
                let memberNames:[String] = members
                let multipleLineString = memberNames.joinWithSeparator(", ")
                cell.groupNames.text = multipleLineString
                
                cell.groupNames.preferredMaxLayoutWidth = CGRectGetWidth(cell.groupNames.frame)
                cell.groupNames.numberOfLines = 1
                cell.groupNames.lineBreakMode = NSLineBreakMode.ByTruncatingTail
                
                cell.name.text = multipleLineString
                
                cell.name.preferredMaxLayoutWidth = CGRectGetWidth(cell.name.frame)
                cell.name.numberOfLines = 1
                cell.name.lineBreakMode = NSLineBreakMode.ByTruncatingTail

                
                cell.accessoryType = UITableViewCellAccessoryType.DetailButton
                cell.name.text = self.groups[indexPath.row]["groupName"] as? String
                

                
                cell.delete.hidden = true
                cell.numberOfOutgoing.hidden = true
                
                cell.tintColor = UIColor.darkGrayColor()
                
                
                if indexPath.row == currentRow {
                    
                    cell.groupNames.preferredMaxLayoutWidth = CGRectGetWidth(cell.groupNames.frame)
                    cell.groupNames.numberOfLines = 0
                    cell.groupNames.lineBreakMode = NSLineBreakMode.ByWordWrapping
                    
                    cell.name.preferredMaxLayoutWidth = CGRectGetWidth(cell.name.frame)
                    cell.name.numberOfLines = 0
                    cell.name.lineBreakMode = NSLineBreakMode.ByWordWrapping
                    
                    cell.memories.hidden = false
                    cell.outgoing.hidden = false
                    cell.activityIndicator.hidden = false
                    cell.activityIndicator.startAnimating()
                    cell.activityIndicator.color = UIColor.highlightColor()
                    
                    
                    cell.memories.hidden = false
                    cell.memories.addTarget(self, action: "memories", forControlEvents: UIControlEvents.TouchUpInside)

                    
                    var number:Int = 0
                    
                    let group:PFObject = self.groups[indexPath.row]
                    let activityQuery = PFQuery(className: "Activity")
                    activityQuery.whereKey("user", equalTo: user!)
                    activityQuery.whereKey("group", equalTo: group)
                    activityQuery.whereKey("dateUnlocked", greaterThan: NSDate())
                    activityQuery.findObjectsInBackgroundWithBlock {
                        (objects, error) -> Void in
                        cell.activityIndicator.stopAnimating()
                        if error == nil {

                            cell.activityIndicator.hidden = true

                            for _ in objects! {
                                number++
                            }
                            cell.numberOfOutgoing.text = String(number)
                            cell.delete.hidden = true
                            cell.numberOfOutgoing.hidden = false
                            
                            if number > 0  {
                                cell.numberOfOutgoing.text = String(number)
                                cell.delete.hidden = false
                                cell.delete.addTarget(self, action: "delete", forControlEvents: UIControlEvents.TouchUpInside)
                            }
                        }else {
                            cell.activityIndicator.color = UIColor.redColor()
                            cell.activityIndicator.hidden = false
                            cell.numberOfOutgoing.text = ""
                            cell.delete.hidden = true
                            cell.numberOfOutgoing.hidden = false
                        }

                    }
                } else {
                    cell.memories.hidden = true
                    cell.activityIndicator.hidden = true

                }
            } else {
                cell.name.text = ""
                cell.accessoryType = UITableViewCellAccessoryType.None
                cell.groupNames.text = ""
                cell.memories.hidden = true
                cell.delete.hidden = true
            }
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row != tableView.numberOfRowsInSection(0) - 1 {
            let indexPathLast:NSIndexPath = NSIndexPath(forRow: self.currentRow!, inSection: 0)
            if currentRow != indexPath.row {
                let selectedRowIndex = indexPath
                self.currentRow = selectedRowIndex.row
                self.tableView.reloadRowsAtIndexPaths([indexPath, indexPathLast], withRowAnimation: UITableViewRowAnimation.Automatic)
            } else {
                self.currentRow = -1
                self.tableView.reloadRowsAtIndexPaths([indexPath, indexPathLast], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
            self.tableView.layoutSubviews()
        }

    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        if currentRow != -1 {
            let oldIndexPath:NSIndexPath = NSIndexPath(forRow: self.currentRow!, inSection: 0)
            self.tableView.reloadRowsAtIndexPaths([oldIndexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }

        if currentRow != indexPath.row {
            let selectedRowIndex = indexPath
            self.currentRow = selectedRowIndex.row
            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        } else {
            self.currentRow = -1
            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if groupTable == false {
            if indexPath.row == currentRow && indexPath.row < buddies.count {
                
                return UITableViewAutomaticDimension
            }else {
                return 44
                
            }
            
        } else  {
            if indexPath.row == currentRow && indexPath.row < groups.count {

                
                return UITableViewAutomaticDimension
            } else {
                return 60

            }
        }
            
        
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "popup") {
            let infoToSend = segue.destinationViewController as! AddBuddyViewController
            infoToSend.fromBuddies = true
            infoToSend.delegate2 = self
            infoToSend.delegate3 = self
        } else if (segue.identifier == "groupFromContacts") {
            let infoToSend = segue.destinationViewController as! AddGroupViewController
            infoToSend.fromBuddies = true
            infoToSend.delegate = self
        }
        //note: fromContacts delegate is now set on viewDidLoad as it is static variable because it was being called from Receivers to Buddies
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}
