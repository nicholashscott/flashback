//  Created by Nick Scott

import UIKit

class BuddiesCell: UITableViewCell {
    
    @IBOutlet var name: UILabel!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var delete: UIButton!

    @IBOutlet var numberOfOutgoing: UILabel!
    @IBOutlet var outgoing: UILabel!
        
    @IBOutlet var memories: UIButton!
    
}
