//  Created by Nick Scott


import UIKit

class NavController: UINavigationController {
    
    override func viewDidLoad() {

        navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "AvenirNext-DemiBold", size: 30)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationBar.barTintColor = UIColor.highlightColor()
        navigationBar.tintColor = UIColor.lightGrayColor()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
