//  Created by Nick Scott


import UIKit

class PicCell: UITableViewCell {

    @IBOutlet var again: UIButton!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var commentCell: UILabel!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
}
