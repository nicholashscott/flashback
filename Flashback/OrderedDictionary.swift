

import UIKit

struct OrderedDictionary<KeyType: Hashable, ValueType>: CustomStringConvertible{
    
    typealias ArrayType = [KeyType]
    typealias DictionaryType = [KeyType: ValueType]
    
    var array = ArrayType()
    var dictionary = DictionaryType()
    
    init(){}
    
    
    init(orderedDictionary: OrderedDictionary<KeyType, ValueType>) {
        array = orderedDictionary.array
        dictionary = orderedDictionary.dictionary
    }
    
  
    mutating func insert(value: ValueType, forKey key: KeyType, atIndex index: Int) -> ValueType?
    {
        var adjustedIndex = index
        
   
        let existingValue = self.dictionary[key]
        if existingValue != nil {
           
            let existingIndex = self.array.indexOf(key)!
            
         
            if existingIndex < index {
                adjustedIndex--
            }
            self.array.removeAtIndex(existingIndex)
        }
   
        self.array.insert(key, atIndex:adjustedIndex)
        self.dictionary[key] = value
        
       
        return existingValue
    }
    
    var count: Int {
        return self.array.count
    }
    

    subscript(key: KeyType) -> ValueType? {
 
        get {
          
            return self.dictionary[key]
        }
       
        set {
       
            if let _ = self.array.indexOf(key) {
            } else {
                self.array.append(key)
            }
            
       
            self.dictionary[key] = newValue
        }
    }
    
    subscript(index: Int) -> (KeyType, ValueType) {
        
        get {
          
            precondition(index < self.array.count,
                "Index out-of-bounds")

            let key = self.array[index]

            let value = self.dictionary[key]!

            return (key, value)
        }
    }
    
    var entries: Array<(KeyType, ValueType)> {
        get {
            var tempArray: Array<(KeyType, ValueType)> = []
            for key: KeyType in array {
                let temp = (key, dictionary[key]!)
                tempArray.append(temp)
            }
            return tempArray
        }
    }
    
    var description: String {
        get {
            var temp: String = "OrderedDictionary {\n"
            let entries = self.entries
            let int = 0
            for entry in entries {
                let (key, value) = entry
                temp += "    [\(int)] {\(key): \(value)}\n"
            }
            temp += "}"
            return temp
        }
    }
    
    mutating func removeEntryAtIndex(index: Int) -> (KeyType, ValueType) {
        let key: KeyType = array[index]
        let value: ValueType = dictionary.removeValueForKey(key)!
        array.removeAtIndex(index)
        return (key, value)
    }
    
    mutating func removeEntryForKey(key: KeyType) -> ValueType? {
        if let index = array.indexOf(key) {
            array.removeAtIndex(index)
        }
        return dictionary.removeValueForKey(key)
    }
    
    mutating func append(newElement: (KeyType, ValueType)) {
        let (key, value) = newElement
        dictionary[key] = value
        array.append(key)
    }
    func map<NewKeyType, NewValueType>(transform aTransform: (KeyType, ValueType) -> (NewKeyType, NewValueType)) -> OrderedDictionary<NewKeyType, NewValueType> {
        let tempArray = Array(dictionary)
        let newArray = tempArray.map(aTransform)
        var temp: OrderedDictionary<NewKeyType, NewValueType> = OrderedDictionary<NewKeyType, NewValueType>()
        for entry in newArray {
            temp.append(entry)
        }
        return temp
    }
    
    mutating func sortByValues(isOrderedBefore sortFunction: (ValueType, ValueType) -> Bool?) {
        var tempArray = Array(dictionary)
        tempArray.sortInPlace({
            let (_, aValue) = $0
            let (_, bValue) = $1
            return sortFunction(aValue, bValue)!
        })
        array = tempArray.map({
            let (key, _) = $0
            return key
        })
    }
    
    func sortedByValues(isOrderedBefore: (ValueType, ValueType) -> Bool?) -> OrderedDictionary<KeyType, ValueType> {
        var temp: OrderedDictionary = OrderedDictionary(orderedDictionary: self)
        temp.sortByValues(isOrderedBefore: { isOrderedBefore($0) })
        return temp
    }
    
    mutating func returnIndex(key: KeyType) -> Int?
    {
        let existingIndex = self.array.indexOf(key)!
        
        return existingIndex
    }

    
}