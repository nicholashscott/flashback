//  Created by Nick Scott


import UIKit
import CoreData
import AVFoundation



class ReceiversViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, dismissWindowDelegate, ReceiversDelegate {
    
    //from CameraView
    var imageFromCamera:UIImage!
    var picTitle:UITextField!
    var unlockDate:NSDate!
    var isMemory:Bool!
    var videoData:NSData!
    var video:Bool!
    
    @IBOutlet var send: UIButton!
    @IBOutlet var receiversLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var newGroup: UIButton!
    @IBOutlet var toggleGroup: UISegmentedControl!
    @IBOutlet var clearCancel: UIVisualEffectView!
    @IBOutlet var cancel: UIButton!
    @IBOutlet var clear: UIButton!


    var user:PFUser = PFUser.currentUser()!
    var tableIsGroup:Bool = false
    var refresher = UIRefreshControl()
    let tap = UITapGestureRecognizer() // to clear names

    var buddiesRecent = [PFUser]()
    var buddies = [PFUser]()
    var groupRecent = [PFObject]()
    var groups = [PFObject]()
    var localBuddyPosts = [buddyLocal]()
    struct buddyLocal{
        var receiver:PFUser?
        var createdAt:NSDate?
    }
    var localGroupPosts = [groupLocal]()
    struct groupLocal{
        var group:PFObject?
        var createdAt:NSDate?
    }
    
    //to be saved
    var posts = [PFObject]()
    var namesToSend = [String]()
    var uniqueIds = [String]()
    
    var blurView:UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var waitingLabel = UILabel()
    
    func waiting(){
        blurView.alpha = 0.8
        blurView.frame = self.view.bounds
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.activityIndicator.color = UIColor.highlightColor()
        self.waitingLabel.text = "S E N D I N G . . ."
        self.waitingLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
        self.waitingLabel.textColor = UIColor.highlightColor()
        self.waitingLabel.translatesAutoresizingMaskIntoConstraints = false

        self.view.addSubview(self.blurView)
        self.view.addSubview(self.activityIndicator)
        self.blurView.addSubview(self.waitingLabel)
        self.activityIndicator.hidden = false
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(
            item: self.activityIndicator,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: view,
            attribute: .CenterX,
            multiplier: 1,
            constant: 0)
        )
        constraints.append(NSLayoutConstraint(
            item: self.activityIndicator,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: view,
            attribute: .CenterY,
            multiplier: 1,
            constant: -50)
        )
        self.view.addConstraints(constraints)
        self.activityIndicator.startAnimating()
        
        var constraintsWaiting = [NSLayoutConstraint]()
        constraintsWaiting.append(NSLayoutConstraint(
            item: self.waitingLabel,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: view,
            attribute: .CenterX,
            multiplier: 1,
            constant: 0)
        )
        constraintsWaiting.append(NSLayoutConstraint(
            item: self.waitingLabel,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: view,
            attribute: .CenterY,
            multiplier: 1,
            constant: 0)
        )
        self.view.addConstraints(constraintsWaiting)
    }
    
    @IBAction func send(sender: AnyObject) {
        self.waiting()
        if send.alpha == 1 {
            let acl = PFACL()
            for post in posts {
                if let group:PFObject = post["group"] as? PFObject {
                    let members:[PFUser] = group["members"] as AnyObject? as! [PFUser]
                    for member in members {
                        acl.setReadAccess(true, forUser: member)
                    }
                } else {
                    if let _ = post["receiver"] as? PFUser {
                        acl.setReadAccess(true, forUser: post["receiver"] as! PFUser)
                    }
                    acl.setReadAccess(true, forUser: self.user)
                }
            }
            
            acl.setWriteAccess(true, forUser: self.user)
            
            self.picVideo.ACL = acl
            
            self.picVideo.saveInBackgroundWithBlock{
                (success, error) -> Void in
                if success {
                    PFObject.saveAllInBackground(self.posts)
                    
                    var recentLocals = [PFObject]()
                    
                    for recentLocal in self.localBuddyPosts {
                        let post = PFObject(className: "buddiesRecent")
                        post["receiver"] = recentLocal.receiver
                        post["createdAt"] = recentLocal.createdAt
                        recentLocals.append(post)
                    }
                    
                    PFObject.pinAllInBackground(recentLocals, withName: "recentBuddies", block: {
                        (success, error) -> Void in
                        
                        ParseUtilFuncs.deleteExtraRecentLocal()
                    })
                    
                    var recentGroups = [PFObject]()

                    for recentGroup in self.localGroupPosts {
                        let post = PFObject(className: "groupsRecent")
                        post["group"] = recentGroup.group
                        post["createdAt"] = recentGroup.createdAt
                        recentGroups.append(post)
                    }
                    
                    PFObject.pinAllInBackground(recentLocals, withName: "recentGroups", block: {
                        (success, error) -> Void in
                        
                        ParseUtilFuncs.deleteExtraRecentGroups()
                    })
                    
                    self.navigationController?.popViewControllerAnimated(true)
                    
                    self.blurView.removeFromSuperview()
                    self.activityIndicator.removeFromSuperview()
                    self.activityIndicator.stopAnimating()
                    
                } else {
                    self.displayAlertUploadError("Oh no! Something went wrong!", error: "It looks like we couldn't send your flashback. If you think you have good enough service, please tell us to try again!")
                }
            }
        }
    }
    
    func displayAlertUploadError(title:String, error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "cancel", style: UIAlertActionStyle.Cancel, handler: { action in
            self.blurView.removeFromSuperview()
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator.stopAnimating()
        }))
        alert.addAction(UIAlertAction(title: "try again!", style: .Default, handler: { action in
            self.send(self)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func newGroup(sender: AnyObject) {
        self.performSegueWithIdentifier("addGroup", sender: self)
    }
    @IBAction func toggleGroup(sender: AnyObject) {
        switch toggleGroup.selectedSegmentIndex
        {
        case 0:
            tableIsGroup = false
            self.tableView.reloadData()
        case 1:
            tableIsGroup = true
            self.tableView.reloadData()
        default:
            break; 
        }
    }
    @IBAction func cancel(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func Cancel(sender: AnyObject) {
        
        self.clearCancel.hidden = true
        self.clear.hidden = true
        self.cancel.hidden = true
        
    }
    @IBAction func Clear(sender: AnyObject) {
        
        self.posts.removeAll()
        self.localGroupPosts.removeAll()
        self.localBuddyPosts.removeAll()
        self.namesToSend.removeAll()
        self.uniqueIds.removeAll()
        self.send.alpha = 0.3
        self.clearCancel.hidden = true
        self.clear.hidden = true
        self.cancel.hidden = true
        self.receiversLabel.text = "... waiting for you to pick ..."
        self.receiversLabel.font = UIFont(name: "AvenirNext-Italic", size: 19)
        self.receiversLabel.textColor = UIColor(hex: 0xC0C0C0)
    }
    
    func showClear(label:UITapGestureRecognizer) {
        self.clear.hidden = false
        self.cancel.hidden = false
        self.clearCancel.hidden = false
        
    }
    func refresh(){
        if tableIsGroup == false {
            populateBuddies(true)
            populateRecentBuddies(true)
        } else {
            populateGroups(true)
            populateRecentGroups(true)
        }
    }
    
    func displayAlert(title:String, error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.Cancel, handler: { action in
        }))
        alert.addAction(UIAlertAction(title: "create group", style: .Default, handler: { action in
            self.performSegueWithIdentifier("addGroup", sender: self)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func populateBuddies(local:Bool){
        if local == false {
            ParseUtilFuncs.populateBuddies{ (tempBuddies, error) in
                self.buddies = tempBuddies!
                self.buddies = self.buddies.sort { ($0["username"]! as! String).localizedCaseInsensitiveCompare($1["username"] as! String) == NSComparisonResult.OrderedAscending }
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
                if self.refresher.refreshing {
                    self.refresher.endRefreshing()
                }
                if self.buddies.isEmpty {
                    self.addContacts()
                }
                
            }
        } else {
            ParseUtilFuncs.populateBuddiesLocal{ tempBuddies in
                self.buddies = tempBuddies
                self.buddies = self.buddies.sort { ($0["username"]! as! String).localizedCaseInsensitiveCompare($1["username"] as! String) == NSComparisonResult.OrderedAscending }
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
                if self.refresher.refreshing {
                    self.refresher.endRefreshing()
                }
                if self.buddies.isEmpty {
                    self.addContacts()
                }
            }
        }
    }
    
    func populateGroups(local:Bool) {
        if local == false {
            ParseUtilFuncs.populateGroups{ (tempGroups, error) in
                
                self.groups = tempGroups!
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
            if self.refresher.refreshing {
                self.refresher.endRefreshing()
            }
        } else {
            
            ParseUtilFuncs.populateGroupsLocal{ (tempGroups) in
                self.groups = tempGroups
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
            
            if self.refresher.refreshing {
                self.refresher.endRefreshing()
            }
            
        }
    }
    
    func populateRecentBuddies(local:Bool){
        if local == false {
            ParseUtilFuncs.recentBuddies{ tempRecentGlobal in
                self.buddiesRecent = tempRecentGlobal
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
            if self.refresher.refreshing {
                self.refresher.endRefreshing()
            }
        } else{
            ParseUtilFuncs.populateRecentLocal{ tempRecentGlobal in
                self.buddiesRecent = tempRecentGlobal
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func populateRecentGroups(local:Bool){
        if local == false {
            ParseUtilFuncs.recentGroups{ (groupsLocal) in
                self.groupRecent = groupsLocal

                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
            if self.refresher.refreshing {
                self.refresher.endRefreshing()
            }
        } else {
            ParseUtilFuncs.populateRecentGroupsLocal{ (groupsLocal) in
                self.groupRecent = groupsLocal
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        }
    }

    func postBuddy(postBuddy:PFUser, buddyList:[PFUser]){

        let post = PFObject(className: "Activity")
        post["picVideo"] = picVideo
        post["dateUnlocked"] = unlockDate
        post["viewed"] = buddyList
        if postBuddy != self.user {
            post["receiver"] = postBuddy
        }
        post["user"] = self.user
        post["title"] = picTitle.text
        post["user"] = PFUser.currentUser()!
        post["memory"] = isMemory
        if self.video == false {
            post["video"] = false
        } else {
            post["video"] = true
        }
    
        let acl = PFACL()
        acl.setReadAccess(true, forUser: self.user)
        acl.setReadAccess(true, forUser: postBuddy)
        acl.setWriteAccess(true, forUser: self.user)
        post.ACL = acl

        posts.append(post)
        
        localBuddyPosts.append(buddyLocal(receiver: postBuddy, createdAt: NSDate()))
    }
    
    
    func postGroup(tempGroup:PFObject, buddyList:[PFUser]){
        
        let post = PFObject(className: "Activity")
        post["picVideo"] = picVideo
        post["dateUnlocked"] = unlockDate
        post["group"] = tempGroup
        post["viewed"] = buddyList
        post["user"] = PFUser.currentUser()
        post["title"] = picTitle.text
        post["user"] = PFUser.currentUser()!
        post["memory"] = isMemory
        if self.video == false {
            post["video"] = false
        } else {
            post["video"] = true
        }
        
        let acl = PFACL()
        acl.setReadAccess(true, forUser: self.user)
        let members:[PFUser] = tempGroup["members"] as AnyObject? as! [PFUser]
        for member in members {
            acl.setReadAccess(true, forUser: member)
        }
        
        acl.setWriteAccess(true, forUser: self.user)
        post.ACL = acl
        
        
        posts.append(post)
        
        
        localGroupPosts.append(groupLocal(group: tempGroup, createdAt: NSDate()))
        
    }
    

    override func viewWillAppear(animated: Bool) {
        
        self.navigationController!.navigationBar.hidden = false
        
        if GlobalSignInUp == true {
            populateRecentBuddies(false)
            populateRecentGroups(false)
        }else{
            populateRecentBuddies(true)
            populateRecentGroups(true)
        }
        populateBuddies(true)
        populateGroups(true)
    }
    
    
    var picVideo:PFObject = PFObject(className: "PicVideo")
    

   
    var lowQualityUrl:NSURL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.video == false {
            
            let cropped:UIImage = ImageUtilCrop.cropToSquare(image: imageFromCamera)

            let size = CGSizeApplyAffineTransform(cropped.size, CGAffineTransformMakeScale(0.2, 0.2))
            let hasAlpha = false
            let scale: CGFloat = 0.0
            
            UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
            cropped.drawInRect(CGRect(origin: CGPointZero, size: size))
            
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let imageDataThumb = UIImageJPEGRepresentation(scaledImage, 1)
            let imageFileThumb = PFFile(name: "image.jpeg", data: imageDataThumb!)
            
            picVideo["thumbnail"] = imageFileThumb
            
            let imageData = UIImageJPEGRepresentation(self.imageFromCamera, 1)
            let imageFile = PFFile(name: "image.jpeg", data: imageData!)
            self.picVideo["mediaFile"] =  imageFile
            
        }else if self.video == true {
            
            let outputUrl:NSURL = lowQualityUrl
            let asset:AVAsset = AVAsset(URL: outputUrl)

            let imageGenerator = AVAssetImageGenerator(asset: asset);
            
            var thumbnailTime:CMTime = asset.duration
            thumbnailTime.value = 0
            var error : NSError?
            let imageCG: CGImage!
            do {
                imageCG = try imageGenerator.copyCGImageAtTime(thumbnailTime, actualTime: nil)
            } catch let error1 as NSError {
                error = error1
                imageCG = nil
                print(error)
            }
            let cropped:UIImage = ImageUtilCrop.cropToSquare(image: UIImage(CGImage: imageCG))

            let size = CGSizeApplyAffineTransform(cropped.size, CGAffineTransformMakeScale(0.2, 0.2))
            let hasAlpha = false
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
            cropped.drawInRect(CGRect(origin: CGPointZero, size: size))
            
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let imageDataThumb = UIImageJPEGRepresentation(scaledImage, 1)
            let imageFileThumb = PFFile(name: "image.jpeg", data: imageDataThumb!)
            
            picVideo["thumbnail"] = imageFileThumb
        
            let imageData = self.videoData
            let imageFile = PFFile(name: "video.mov", data: imageData)
            self.picVideo["mediaFile"] =  imageFile
            
        }
        
        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        refresher.tintColor = UIColor.highlightColor()
        tableView.addSubview(refresher)
        
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.newGroup.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Highlighted)
        
        
        self.receiversLabel.userInteractionEnabled = true
        self.tap.addTarget(self, action: "showClear:")
        self.receiversLabel.addGestureRecognizer(self.tap)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       
        return 2
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (section){
        case 0:
            if tableIsGroup == false {
                return self.buddiesRecent.count
            } else {
                return self.groupRecent.count
            }
            
        case 1:
            if tableIsGroup == false {
                return self.buddies.count
            } else {
                return self.groups.count
            }
        default:
            return 1
        }
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell:ReceiversCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! ReceiversCell
        
        cell.buddyGroupName?.font = UIFont(name: "AvenirNext-Regular", size: 17)

        switch (indexPath.section) {
        
        case 0:
            var listItem:String?

            if tableIsGroup == false {
                listItem = buddiesRecent[indexPath.row].username
                cell.names.hidden = true
            } else {
                cell.names.hidden = false
                listItem = groupRecent[indexPath.row]["groupName"] as? String
                let groupMembersPF:[PFUser] = self.groupRecent[indexPath.row]["members"] as AnyObject? as! [PFUser]
                var members = [String]()
                for member in groupMembersPF {
                    if member.username! != user.username! {
                        members.append(member.username!)
                    }
                }
                let memberNames:[String] = members
                let multipleLineString = memberNames.joinWithSeparator(", ")
                cell.names.text = multipleLineString
                
                cell.names.preferredMaxLayoutWidth = CGRectGetWidth(cell.names.frame)
                cell.names.numberOfLines = 0
                cell.names.lineBreakMode = NSLineBreakMode.ByWordWrapping
                

            }
            cell.buddyGroupName?.text = listItem
        case 1:
            if tableIsGroup == false {
                cell.buddyGroupName?.text = buddies[indexPath.row].username!
                cell.names.hidden = true

            } else {
                cell.names.hidden = false
                cell.buddyGroupName?.text = groups[indexPath.row]["groupName"] as? String
                let groupMembersPF:[PFUser] = self.groups[indexPath.row]["members"] as AnyObject? as! [PFUser]
                var members = [String]()
                for member in groupMembersPF {
                    if member.username! != user.username! {
                        members.append(member.username!)
                    }
                }
                let memberNames:[String] = members
                let multipleLineString = memberNames.joinWithSeparator(", ")
                cell.names.text = multipleLineString
                
                cell.names.preferredMaxLayoutWidth = CGRectGetWidth(cell.names.frame)
                cell.names.numberOfLines = 0
                cell.names.lineBreakMode = NSLineBreakMode.ByWordWrapping
            }

        default:
            cell.buddyGroupName?.text = "Other"
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as! CustomHeaderCell

        switch (section) {
        case 0:
            
            if tableIsGroup == false {
                headerCell.headerLabel.text = "Recent buddies";
            } else {
                headerCell.headerLabel.text = "Recent groups";
            }
        case 1:
            if tableIsGroup == false {
                headerCell.headerLabel.text = "All buddies";
            } else {
                headerCell.headerLabel.text = "All groups";
            }

        default:
            headerCell.headerLabel.text = "Other";
        }
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if tableIsGroup == false {
            return 44
        } else {
            return UITableViewAutomaticDimension
        }
        
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25.0
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell:ReceiversCell = tableView.cellForRowAtIndexPath(indexPath) as! ReceiversCell

        selectedCell.contentView.backgroundColor = UIColor.whiteColor()
        
        if (namesToSend.count < 5) {
            switch (indexPath.section) {
            case 0:
                if tableIsGroup == false{
                    let tempBuddy = self.buddiesRecent[indexPath.row]
                    if !self.uniqueIds.contains((tempBuddy.objectId!)){
                        var buddyList:[PFUser] = [tempBuddy, user]
                        if tempBuddy == user {
                            buddyList.removeLast()
                        }
                        postBuddy(tempBuddy, buddyList: buddyList)
                        self.namesToSend.append(tempBuddy.username!)
                        self.uniqueIds.append(tempBuddy.objectId!)
                    }
                } else {
                    let tempGroup:PFObject = self.groupRecent[indexPath.row]
                    if !self.uniqueIds.contains((tempGroup.objectId!)){
                        let tempMembers:[PFUser] = tempGroup["members"] as AnyObject? as! [PFUser]
                        postGroup(tempGroup, buddyList: tempMembers)
                        self.namesToSend.append((tempGroup["groupName"] as? String)!)
                        self.uniqueIds.append(tempGroup.objectId!)
                    }
                }
                
            case 1:
                if tableIsGroup == false{
                    let tempBuddy = self.buddies[indexPath.row]
                    if !self.uniqueIds.contains((tempBuddy.objectId!)){
                        var buddyList:[PFUser] = [tempBuddy, user]
                        if tempBuddy == user {
                            buddyList.removeLast()
                        }
                        postBuddy(tempBuddy, buddyList: buddyList)
                        self.namesToSend.append(tempBuddy.username!)
                        self.uniqueIds.append(tempBuddy.objectId!)
                    }
                } else {
                    let tempGroup:PFObject = self.groups[indexPath.row]
                    if !self.uniqueIds.contains((tempGroup.objectId!)){
                        let tempMembers:[PFUser] = tempGroup["members"] as AnyObject? as! [PFUser]
                        postGroup(tempGroup, buddyList: tempMembers)
                        self.namesToSend.append((tempGroup["groupName"] as? String)!)
                        self.uniqueIds.append(tempGroup.objectId!)
                    }
                }
               
            default:
                print("")
            }
            
            self.receiversLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
            self.receiversLabel.textColor = UIColor.darkGrayColor()
            let multipleLineString = self.namesToSend.joinWithSeparator(", ")
            self.receiversLabel.text = multipleLineString
            self.receiversLabel.numberOfLines = 0
            self.receiversLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
            
            self.send.alpha = 1
        } else if namesToSend.count == 5 {
            displayAlert("Too many selected!", error: "You can only send the Flashback to 5 buddies (or groups). Swipe left to manage groups. Or press 'OK' to create a group and send to that group only.")
        }



    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "addGroup") {
            
            let imageToSend = segue.destinationViewController as! AddGroupViewController;
            
            imageToSend.delegate2 = self
            imageToSend.fromBuddies = false
            imageToSend.picTitle = self.picTitle
            imageToSend.unlockDate = self.unlockDate
            imageToSend.isMemory = self.isMemory
            imageToSend.videoData = self.videoData
            imageToSend.video = self.video
            imageToSend.picVideo = self.picVideo
        } else if (segue.identifier == "addContacts") {
            
            let addContactsVC = segue.destinationViewController as! AddContactsViewController;
            addContactsVC.delegateReceivers = self
            addContactsVC.fromReceivers = true
            
        }
    }
    
    func popController(){
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController!.navigationBar.hidden = false

    }

   
    
    func displayAlertContact(title:String, error:String){
        
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "no", style: UIAlertActionStyle.Cancel, handler: { action in
        }))
        
        alert.addAction(UIAlertAction(title: "yes", style: .Default, handler: { action in
            self.performSegueWithIdentifier("addContacts", sender: self)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addContacts() {
        let title = "Add Buddies"
        let error = "Uh-oh, we couldn't find any of your buddies? Would you like to add buddies from your iPhone contacts?"
        displayAlertContact(title, error: error)
    }

    
}