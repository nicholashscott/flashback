//  Created by Nick Scott

import CoreData
import UIKit

protocol GroupDelegate {
    func populateGroupsLocal()
}

protocol dismissWindowDelegate {
    func popController()
}


class AddGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var delegate:GroupDelegate?
    var delegate2:dismissWindowDelegate?
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var labelGroupEmpty: UILabel!
    @IBOutlet var usersTable: UITableView!
    @IBOutlet var groupTitle: UILabel!
    @IBOutlet var groupNameText: UITextField!
    @IBOutlet var buddiesTitle: UILabel!
    @IBOutlet var groupTable: UITableView!
    @IBOutlet var keyboardSpace: NSLayoutConstraint!
    @IBOutlet var create: UIButton!

    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    @IBAction func newUser(sender: AnyObject) {
        performSegueWithIdentifier("newUser", sender: self)
    }

    //from receivers
    var fromBuddies:Bool!
    var picTitle:UITextField!
    var unlockDate:NSDate!
    var isMemory:Bool!
    var videoData:NSData!
    var video:Bool!
    var picVideo:PFObject!
    
    var user:PFUser = PFUser.currentUser()!
    var buddies:[PFUser] = []
    var lightGreyDict = [String:Bool]()
    var viewedGroup = [Bool]()
    var group = [PFUser]()
    var buddiesRecent = [PFUser]()
    var lightGreyRecentDict = [String:Bool]()
    
    var activityIndicator = UIActivityIndicatorView()
    var blurView:UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
    
    override func viewDidLayoutSubviews() {
        containerView.layer.cornerRadius = 5
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateBuddies()
        populateBuddiesRecent()
        
        if let fromBuddies = self.fromBuddies {
            if fromBuddies == true {
                self.create.setTitle("CREATE", forState: UIControlState.Normal)
            }
        }
        
        self.groupTable.separatorStyle = UITableViewCellSeparatorStyle.None
        self.groupNameText.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardNotificationHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func populateBuddies(){
        ParseUtilFuncs.populateBuddiesLocal{ tempBuddies in
            self.buddies = tempBuddies
            self.buddies = self.buddies.sort { ($0["username"]! as! String).localizedCaseInsensitiveCompare($1["username"] as! String) == NSComparisonResult.OrderedAscending }
            for buddy in self.buddies {
                if buddy == self.user {
                    self.buddies = self.buddies.filter() { $0 !== self.user }
                } else {
                    self.lightGreyDict[buddy["username"] as! String] = false
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.usersTable.reloadData()
            }
        }
    }
    
    func populateBuddiesRecent(){
        if GlobalShouldReload == true {
            ParseUtilFuncs.recentBuddies{ tempRecentGlobal in
                self.buddiesRecent = tempRecentGlobal
                for buddy in self.buddiesRecent {
                    let name:String = buddy.username!
                    self.lightGreyRecentDict[name] = false
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.usersTable.reloadData()
                }
            }
        } else{
            ParseUtilFuncs.populateRecentLocal{ tempRecentGlobal in
                self.buddiesRecent = tempRecentGlobal
                for buddy in self.buddiesRecent {
                    let name:String = buddy.username!
                    self.lightGreyRecentDict[name] = false
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.usersTable.reloadData()
                }
            }
        }
    }

    func waiting(){
        self.view.addSubview(self.blurView)
        blurView.alpha = 0.5
        blurView.frame = self.view.bounds
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        activityIndicator.color = UIColor.grayColor()
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()

    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        textField.returnKeyType = UIReturnKeyType.Next
        var shouldChange = false
        
        if groupNameText.text!.characters.count + string.characters.count < 60 {
            shouldChange = true
        }
        return shouldChange
    }

    @IBAction func send(sender: AnyObject) {
        waiting()
        if Reachability.isConnectedToNetwork() {
            if (group.count > 1 && groupNameText.text != "" && groupNameText.text != nil) {
                self.group.append(self.user)
                let objectPost = PFObject(className: "Group")
                objectPost["groupName"] = self.groupNameText.text
                objectPost["members"] = self.group as Array
                objectPost["user"] = self.user
                
                let acl = PFACL()
                let members:[PFUser] = self.group as AnyObject? as! [PFUser]
                for member in members {
                    acl.setReadAccess(true, forUser: member)
                }
                acl.setWriteAccess(true, forUser: self.user)

                objectPost.ACL = acl
                objectPost.saveInBackgroundWithBlock{
                    (success, error) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.blurView.removeFromSuperview()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    if success {
                        objectPost.pinInBackgroundWithBlock{
                            (success, error) -> Void in
                            self.delegate?.populateGroupsLocal()
                            if let fromBuddies = self.fromBuddies {
                                if fromBuddies == false{
                                    self.viewedGroup.append(false)
                                    var groupDict = [String: Bool]()
                                    for var i = 0; i < self.group.count; i++ {
                                        groupDict[self.group[i].username!] = self.viewedGroup[i]
                                    }
                                    let post = PFObject(className: "Activity")
                                    post["picVideo"] = self.picVideo
                                    post["dateUnlocked"] = self.unlockDate
                                    post["group"] = objectPost
                                    post["viewed"] = self.group
                                    post["user"] = PFUser.currentUser()
                                    post["title"] = self.picTitle.text
                                    post["memory"] = self.isMemory
                                    if self.video == false {
                                        post["video"] = false
                                    } else {
                                        post["video"] = true
                                    }
                                    let acl = PFACL()
                                    let members:[PFUser] = self.group as AnyObject? as! [PFUser]
                                    for member in members {
                                        acl.setReadAccess(true, forUser: member)
                                    }
                                    acl.setWriteAccess(true, forUser: self.user)
                                    post.ACL = acl
                                    post.saveEventually()
                                    self.delegate2?.popController()
                                }
                            }
                            self.dismissViewControllerAnimated(true, completion: {
                            })
                        }
                    } else if error != nil {
                        let error = "But please try again later!"
                        self.displayAlert("Sorry! Something went wrong creating the group!", error: error)
                        
                    }
                }
            } else if group.count < 2 {
                let error = "You need at least two buddies to be a group!"
                self.displayAlert("Need buddies!", error: error)
                self.activityIndicator.stopAnimating()
                self.blurView.removeFromSuperview()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                
            } else if self.groupNameText.text == "" {
                let error = "You need a name for your group!"
                self.displayAlert("Need a Name", error: error)
                self.activityIndicator.stopAnimating()
                self.blurView.removeFromSuperview()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
            }
        }else{
            let error = "Looks like you aren't connected to the internet :("
            self.displayAlert("We need a connection", error: error)
            self.activityIndicator.stopAnimating()
            self.blurView.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardNotificationHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)

    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView == usersTable {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number:Int
        if tableView == usersTable {
            switch (section){
            case 0:
                
                number = buddiesRecent.count
                return number
            case 1:
                number = buddies.count
                return number
                
            default:
                return 1
            }
            
        } else {
            number = group.count
            return number
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.whiteColor()
        
        if tableView == usersTable {
            switch (indexPath.section) {
            case 0:
                
                let buddyAdded:PFUser = buddiesRecent[indexPath.row]
                if !self.group.contains(buddyAdded){
                    let name = buddyAdded.username
                    self.lightGreyRecentDict[name!] = true
                    self.usersTable.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                    if let _ = self.lightGreyDict[name!] {
                        self.lightGreyDict[name!] = true
                        tableView.reloadData()
                    }
                    self.group.append(buddyAdded)
                    self.viewedGroup.append(false)
                }
                
            case 1:
                let buddyAdded:PFUser = self.buddies[indexPath.row]
                if !self.group.contains(buddyAdded){
                    let name = buddyAdded.username
                    self.lightGreyDict[name!] = true
                    self.usersTable.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                    if let _ = self.lightGreyRecentDict[name!] {
                        self.lightGreyRecentDict[name!] = true
                        tableView.reloadData()
                        
                    }
                    self.group.append(self.buddies[indexPath.row])
                    self.viewedGroup.append(false)
                }
                
            default:
                self.groupTable.reloadData()
            }
            
            if group.isEmpty{
                labelGroupEmpty.hidden = false
            } else {
                labelGroupEmpty.hidden = true
            }
            
            self.groupTable.reloadData()
            
        } else {
            let buddyRemoved:PFUser = self.group[indexPath.row]
            self.group.removeAtIndex(indexPath.row)
            
            if let _ = self.lightGreyRecentDict[buddyRemoved.username!] {
                lightGreyRecentDict[buddyRemoved.username!] = false
            }
            
            
            if let _ = self.lightGreyDict[buddyRemoved.username!] {
                lightGreyDict[buddyRemoved.username!] = false
            }
            
            if group.isEmpty{
                labelGroupEmpty.hidden = false
            } else {
                labelGroupEmpty.hidden = true
            }
            
            self.groupTable.reloadData()
            self.usersTable.reloadData()
        }
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if tableView == usersTable {
            
            let cell = self.usersTable.dequeueReusableCellWithIdentifier("cell1") as! PotentialBuddiesCell
            switch (indexPath.section) {
                
            case 0:
                let user:PFUser = buddiesRecent[indexPath.row]
                let name:String = user.username!
                cell.name.text = name
                
                if lightGreyRecentDict[name] == false {
                    cell.name.textColor = UIColor.blackColor()
                    
                } else {
                    cell.name.textColor = UIColor.lightGrayColor()
                    
                }
            case 1:
                let buddyTemp:PFUser = self.buddies[indexPath.row]
                let name = buddyTemp.username

                cell.name.text = name!
                
                if lightGreyDict[name!] == false {
                    cell.name.textColor = UIColor.blackColor()
                    
                } else {
                    cell.name.textColor = UIColor.lightGrayColor()
                    
                }
            default:
                cell.textLabel?.text = "Other"
                
            }
            
            return cell
            
            
        } else {
            let cell = self.groupTable.dequeueReusableCellWithIdentifier("cell2") as! GroupCell
            
            cell.name.text = group[indexPath.row].username!
            
            return cell
            
            
        }
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == usersTable{
            
            let  headerCell = usersTable.dequeueReusableCellWithIdentifier("HeaderCell") as! HeaderCellGroup
            
            switch (section) {
            case 0:
                headerCell.headingLabel?.text = "Recent buddies";
                
            case 1:
                headerCell.headingLabel?.text = "All buddies";
                
            default:
                headerCell.headingLabel?.text = "Other";
            }
            
            return headerCell
        } else{
            
            let  headerCell = groupTable.dequeueReusableCellWithIdentifier("HeaderCell") as! HeaderCellGroup
            
            switch (section) {
            case 0:
                headerCell.headingLabel?.text = "GROUP";
                
            default:
                headerCell.headingLabel?.text = "Other";
            }
            
            return headerCell
        }
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 25
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func displayAlert(title:String, error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: {});
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue()
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardSpace?.constant = endFrame!.size.height + 8
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    
                }
            )
        }
    }
    
    func keyboardNotificationHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardSpace?.constant = 50
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    
                }
            )
        }
    }

    
    
}

