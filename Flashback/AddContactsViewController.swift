//  Created by Nick Scott

import UIKit
import AddressBook

protocol ReceiversDelegate {
    func populateBuddies(local:Bool)
}


class AddContactsViewController: UIViewController {
    
    static var delegate:BuddiesDelegate? //updates buddies from local
    var delegateReceivers:ReceiversDelegate? //updates buddies with call from local
    
    var fromReceivers:Bool?
    
    @IBOutlet var viewBud: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var x: UIButton!
    var currentBuddies = [PFObject]()
    var numbers = [String]()
    var potentialBuddies = [PFObject]()
    var checkmarks = [Bool]()
    var loading = [Bool]()
    var addedBuddies = [PFObject]()
    var user:PFUser?
    var activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))

    @IBAction func done(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.view.alpha = 1.0
        if let fromReceiversTemp = fromReceivers {
            if fromReceiversTemp == true {
                delegateReceivers?.populateBuddies(true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ParseUtilFuncs.populateBuddiesLocal{ tempBuddies in
            self.currentBuddies = tempBuddies
            self.addInitialBuddies()
        }
        
        user = PFUser.currentUser()!
    }

    func displayAlertContacts (title:String, error:String){
        
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
            self.dismissViewControllerAnimated(true, completion: nil)
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            UIApplication.sharedApplication().openURL(settingsUrl!)
        }))

        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addInitialBuddies (){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.alpha = 0.75
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()

        self.potentialBuddies.removeAll()
        self.numbers.removeAll()
        self.checkmarks.removeAll()
        self.loading.removeAll()
        
        // make sure user hadn't previously denied access
        let status = ABAddressBookGetAuthorizationStatus()
        if status == .Denied || status == .Restricted {
            displayAlertContacts("Help! We need access!", error: "Looks like you previously said 'no' to us accessing your contacts, but we need access to find your buddies! Can we take you to settings so you can change it?")
        }
        var error: Unmanaged<CFError>?
        let addressBook: ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error)?.takeRetainedValue()
        
        
        if addressBook == nil {
            print(error?.takeRetainedValue())
        }
        
        
        // request permission to use it
        
        ABAddressBookRequestAccessWithCompletion(addressBook) {
            granted, error in
            

            
            if let people = ABAddressBookCopyArrayOfAllPeople(addressBook)?.takeRetainedValue() as NSArray? as? [ABRecordRef] {
                for person:ABRecordRef in people {
                    
                    let phones: ABMultiValueRef = ABRecordCopyValue(person, kABPersonPhoneProperty).takeUnretainedValue() as ABMultiValueRef
                    
                    let countOfPhones = ABMultiValueGetCount(phones)
                    for index in 0..<countOfPhones{
                        let phone:String = ABMultiValueCopyValueAtIndex(phones, index).takeUnretainedValue() as ABMultiValueRef as! String
                        let stringArray = phone.componentsSeparatedByCharactersInSet(
                            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
                        var newString:String = NSArray(array: stringArray).componentsJoinedByString("") as String
                        if Array(newString.characters)[0] == "1" {
                            newString = newString.substringFromIndex(newString.startIndex.advancedBy(1))
                        }
                        if Array(newString.characters).count == 10 {
                            self.numbers.append(newString)
                        }
                        
                    }

                }
                
                let usersQuery = PFUser.query()
                usersQuery?.whereKey("phone", containedIn: self.numbers)
                usersQuery!.findObjectsInBackgroundWithBlock {
                    (objects, error) -> Void in
                    self.view.alpha = 1
                    if error == nil {
                        for object in objects! {
                            if !self.currentBuddies.contains((object as? PFUser)!) {
                                self.potentialBuddies.append(object)
                                self.checkmarks.append(false)
                                self.loading.append(false)
                            }

                        }
                        self.potentialBuddies = self.potentialBuddies.sort { ($0["username"]! as! String).localizedCaseInsensitiveCompare($1["username"] as! String) == NSComparisonResult.OrderedAscending }
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                    } else {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = false
                        self.activityIndicator.color = UIColor.redColor()
                    }
                }

            } else {
                
            }
        }
        

    }
    
    
    func displayAlert(title:String, error:String){

        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func selectAllContacts(sender: AnyObject) {
        if !self.activityIndicator.isAnimating() {
            if !potentialBuddies.isEmpty {
                
                self.activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
                self.activityIndicator.center = self.view.center
                self.activityIndicator.hidesWhenStopped = true
                self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
                self.view.addSubview(activityIndicator)
                self.activityIndicator.startAnimating()
                UIApplication.sharedApplication().beginIgnoringInteractionEvents()

                var posts:[PFObject] = [PFObject]()
                for i in 0..<potentialBuddies.count {
                    let post = PFObject(className: "buddies")
                    post.setObject(self.user!, forKey: "user")
                    post.setObject(self.potentialBuddies[i], forKey: "friend")
                    let acl = PFACL()
                    acl.setReadAccess(true, forUser: self.user!)
                    acl.setWriteAccess(true, forUser: self.user!)
                    post.ACL = acl
                    posts.append(post)
                }
                PFObject.saveAllInBackground(posts){
                    (success, error) -> Void in
                    if error?.code == 142 || error == nil {
                        PFObject.pinAllInBackground(posts){
                            (success, error) -> Void in
                                if success {
                                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                                    AddContactsViewController.delegate?.populateBuddiesLocal()
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                    if let fromReceiversTemp = self.fromReceivers {
                                        if fromReceiversTemp == true {
                                            self.delegateReceivers?.populateBuddies(true)
                                        }
                                    }
                                }
                        }
                    } else {
                        self.activityIndicator.stopAnimating()
                        UIApplication.sharedApplication().endIgnoringInteractionEvents()
                        self.displayAlert("Sorry something went wrong!", error: "Looks like something went wrong on our end (sorry)... Please try again later!")
                        
                    }
                }
            }
        }
    }
    


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.loading[indexPath.row] == false && self.checkmarks[indexPath.row] == false{
            let activityIndicator = UIActivityIndicatorView()
            
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
                activityIndicator.frame = CGRectMake(cell.contentView.frame.width - 32, cell.contentView.frame.height/2 - 10, 20, 20)
                cell.contentView.addSubview(activityIndicator)
                activityIndicator.startAnimating()
            }

            let post = PFObject(className: "buddies")
            post.setObject(self.user!, forKey: "user")
            post.setObject(self.potentialBuddies[indexPath.row], forKey: "friend")
            let acl = PFACL()
            acl.setReadAccess(true, forUser: self.user!)
            acl.setWriteAccess(true, forUser: self.user!)
            post.ACL = acl
            post.saveInBackgroundWithBlock{
                (success, error) -> Void in
                if success{
                    post.pinInBackgroundWithBlock{
                    (success, error) -> Void in
                            if success {
                                AddContactsViewController.delegate?.populateBuddiesLocal()
                            }
                        }
                    self.checkmarks[indexPath.row] = true
                    self.tableView.reloadData()
                    
                } else if error != "taken" { // error from cloud code if it is taken
                    activityIndicator.stopAnimating()
                    activityIndicator.hidden = false
                    activityIndicator.color = UIColor.redColor()
                    self.loading[indexPath.row] = false
                }
            }
        }
        
    }

    override func viewDidLayoutSubviews() {
        self.viewBud.layer.cornerRadius = 5
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.potentialBuddies.count
   
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell")!

        
        cell.textLabel?.text = potentialBuddies[indexPath.row]["username"] as? String
        cell.textLabel?.font = UIFont(name: "AvenirNext-Regular", size: 18)
        
        if checkmarks[indexPath.row] == true {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }
        
        return cell
    }
}
