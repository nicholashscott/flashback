//  Created by Nick Scott


import UIKit
import AVFoundation
class ViewAllViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var name:String!
    var user:PFUser = PFUser.currentUser()!
    var activities = [PFObject]()
    var images = [UIImage]()
    var groupBool:Bool!
    var uniqueId:String!
    
    @IBOutlet var mainCollectionView: UICollectionView!
    
    var buddy:PFUser!
    var group:PFObject!
    
    @IBOutlet var memoriesTitle: UINavigationItem!

    var errorHeight:NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newTitle:UILabel = UILabel()
        newTitle.frame = CGRectMake(0, 0, 0, 0)
        newTitle.font = UIFont(name: "AvenirNext-DemiBold", size: 30)
        newTitle.textAlignment = .Center
        newTitle.baselineAdjustment = .AlignCenters
        newTitle.textColor = UIColor.whiteColor()
        newTitle.text = self.name
        newTitle.minimumScaleFactor = 0.5
        newTitle.adjustsFontSizeToFitWidth = true
        newTitle.sizeToFit()
        memoriesTitle.titleView = newTitle
        
        populateFiles(false)
    }
    
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            let side = (self.view.frame.width - 6)/3
            
            return CGSize(width: side, height: side)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            
            let sectionInsets = UIEdgeInsets(top: 2.0, left: 0, bottom: 2, right: 0)

            return sectionInsets
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return 2
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return images.count
    }
    
    var reloading:Bool = false
    
    
    var activityIndicator = UIActivityIndicatorView()
    
    func waiting(){
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.activityIndicator.color = UIColor.highlightColor()
        
        self.view.addSubview(self.activityIndicator)

        self.activityIndicator.hidden = false
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(
            item: self.activityIndicator,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: view,
            attribute: .CenterX,
            multiplier: 1,
            constant: 0)
        )
        constraints.append(NSLayoutConstraint(
            item: self.activityIndicator,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: view,
            attribute: .CenterY,
            multiplier: 1,
            constant: -50)
        )
        self.view.addConstraints(constraints)
        self.activityIndicator.startAnimating()
        
    }

    
    var tryAgainButton:UIButton?
    
    func tryAgain(){
        
        self.activityIndicator.stopAnimating()
        self.activityIndicator.hidden = false
        self.activityIndicator.color = UIColor.redColor()
        
        tryAgainButton = UIButton()
        tryAgainButton!.translatesAutoresizingMaskIntoConstraints = false
        tryAgainButton!.titleLabel!.font = UIFont(name: "AvenirNext-Regular", size: 17)
        tryAgainButton!.setTitle("TRY AGAIN?", forState: UIControlState.Normal)
        tryAgainButton!.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        tryAgainButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        tryAgainButton!.addTarget(self, action:"tryAgainFunc", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(self.tryAgainButton!)
        var constraintsTryAgain = [NSLayoutConstraint]()
        constraintsTryAgain.append(NSLayoutConstraint(
            item: self.tryAgainButton!,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .CenterX,
            multiplier: 1,
            constant: 0)
        )
        constraintsTryAgain.append(NSLayoutConstraint(
            item: self.tryAgainButton!,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .CenterY,
            multiplier: 1,
            constant: -20)
        )
        
        self.view.addConstraints(constraintsTryAgain)
    }
    
    func tryAgainFunc(){
        self.tryAgainButton?.removeFromSuperview()
        populateFiles(false)
    }
    
    func populateFiles(reload:Bool) {
        reloading = true
        if noMoreMedia == false {
            if reload == false{
                waiting()
            }
            
            if let constraintError = self.errorHeight {
                if constraintError.constant > 0 {
                    self.errorHeight!.constant = 0
                }
            }
            
            if Reachability.isConnectedToNetwork() {
                
                var activityQuery = PFQuery()
                
                if groupBool == false {
                    
                    let usersQuery = PFUser.query()
                    
                    let activityQuery1 = PFQuery(className: "Activity")
                    if buddy == nil {
                        usersQuery!.whereKey("objectId", equalTo: uniqueId)
                        activityQuery1.whereKey("receiver", matchesQuery: usersQuery!)

                    } else {
                        activityQuery1.whereKey("receiver", equalTo: self.buddy)

                    }
                    activityQuery1.whereKey("user", equalTo: user)
                    activityQuery1.whereKey("dateUnlocked", lessThan: NSDate())
                    activityQuery1.whereKey("memory", equalTo: true)

                    
                    
                    let activityQuery2 = PFQuery(className: "Activity")
                    activityQuery2.whereKey("receiver", equalTo: user)
                    if buddy == nil {
                        usersQuery!.whereKey("objectId", equalTo: uniqueId)
                        activityQuery2.whereKey("user", matchesQuery: usersQuery!)
                    } else{
                        activityQuery2.whereKey("user", equalTo: self.buddy)

                    }
                    activityQuery2.whereKey("dateUnlocked", lessThan: NSDate())
                    activityQuery2.whereKey("memory", equalTo: true)

                    
                    // for ones where user is sending to themselves
                    let activityQuery3 = PFQuery(className: "Activity")
                    activityQuery3.whereKeyDoesNotExist("group")
                    activityQuery3.whereKeyDoesNotExist("receiver")
                    if buddy == nil {
                        usersQuery!.whereKey("objectId", equalTo: uniqueId)
                        activityQuery3.whereKey("user", matchesQuery: usersQuery!)
                    } else{
                        activityQuery3.whereKey("user", equalTo: self.buddy)
                    }
                    activityQuery3.whereKey("dateUnlocked", lessThan: NSDate())
                    activityQuery3.whereKey("memory", equalTo: true)


                    activityQuery = PFQuery.orQueryWithSubqueries([activityQuery1, activityQuery2, activityQuery3])
                } else {
                    
                    
                    let activityQuery3 = PFQuery(className: "Activity")
                    if group == nil {
                        let groupQuery = PFQuery(className: "Group")
                        groupQuery.whereKey("objectId", equalTo: uniqueId)
                        activityQuery3.whereKey("group", matchesQuery: groupQuery)


                    } else {
                        activityQuery3.whereKey("group", equalTo: self.group)
                    }
                    activityQuery3.whereKey("dateUnlocked", lessThan: NSDate())
                    activityQuery3.whereKey("memory", equalTo: true)

                    
                    activityQuery = activityQuery3
                }
                
                let viewedQuery = PFQuery(className: "Viewed")
                viewedQuery.orderByDescending("createdAt")
                viewedQuery.whereKey("user", equalTo: user)
                viewedQuery.whereKey("viewed", equalTo: true)
                viewedQuery.whereKey("activity", matchesQuery: activityQuery)
                viewedQuery.includeKey("activity")
                viewedQuery.includeKey("activity.picVideo")
                if reload == true {
                    viewedQuery.skip = images.count
                    viewedQuery.limit = 9
                } else {
                    viewedQuery.limit = 18
                }
                var index = self.images.count
                
                viewedQuery.findObjectsInBackgroundWithBlock {
                    (objects, error) -> Void in
                    if reload == false {
                        self.activityIndicator.removeFromSuperview()
                        self.activityIndicator.stopAnimating()
                    }
                    if error == nil {
                        
                        if reload == false {
                            if objects!.count == 0 {
                                self.noMemories()
                            }
                        }
                        
                        var indexPaths = [NSIndexPath]()
                        var tasks = [BFTask]()
                        
                        for object in objects! {
                            let activity:PFObject = (object["activity"] as? PFObject)!
                            let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
                            let file:PFFile = (picVideo["thumbnail"] as? PFFile)!
                            let task:BFTask = file.getDataInBackground()
                            tasks.append(task)
                        
                            self.activities.append(activity)
                        }
                        
                        BFTask(forCompletionOfAllTasks: tasks).continueWithBlock{
                            (task: BFTask!) -> BFTask in
                            for i in 0..<tasks.count {
                                
                                let image = UIImage(data: tasks[i].result as! NSData)
                                let previewImage = ImageUtilCrop.cropToSquare(image: image!)
                                self.images.append(previewImage)
                                let indexPath = NSIndexPath(forItem: index++, inSection: 0)
                                indexPaths.append(indexPath)
                            }
                            
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                
                                self.collectionView!.performBatchUpdates({
                                    self.collectionView!.insertItemsAtIndexPaths(indexPaths)
                                    }, completion: nil)
                                
                            }
                            
                            self.reloading = false
                            
                            return task
                        }
                        
                        if objects?.count < 9 {
                            self.noMoreMedia = true
                        }
                        
                    } else {
                        if reload == false {
                            self.tryAgain()
                        } else {
                            if self.mainCollectionView.contentOffset.y >= self.mainCollectionView.contentSize.height - self.mainCollectionView.frame.size.height - 30 {
                                let adjustment = self.mainCollectionView.contentOffset.y - (self.mainCollectionView.contentSize.height - self.mainCollectionView.frame.size.height)
                                self.mainCollectionView.contentOffset.y = self.mainCollectionView.contentOffset.y - (40 - adjustment)
                            }
                            self.noConnection()

                        }
                        self.reloading = false


                    }
                }
            } else if reload == false {
                delay(0.5){
                    self.tryAgain()
                    self.reloading = false
                }
                
            } else if reload == true {
                if self.mainCollectionView.contentOffset.y >= self.mainCollectionView.contentSize.height - self.mainCollectionView.frame.size.height - 30 {
                    let adjustment = self.mainCollectionView.contentOffset.y - (self.mainCollectionView.contentSize.height - self.mainCollectionView.frame.size.height)
                    self.mainCollectionView.contentOffset.y = self.mainCollectionView.contentOffset.y - (40 - adjustment)
                }
                noConnection()
                self.reloading = false
                
            }
        }
    }
    
    func noConnection(){
        let noConnectionView:UIView = UIView()
        noConnectionView.translatesAutoresizingMaskIntoConstraints = false
        noConnectionView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.7)
        noConnectionView.clipsToBounds = true
        
        self.view.addSubview(noConnectionView)
        
        var constraintsView = [NSLayoutConstraint]()
        constraintsView.append(NSLayoutConstraint(
            item: noConnectionView,
            attribute: NSLayoutAttribute.Bottom,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self.view,
            attribute: .Bottom,
            multiplier: 1,
            constant: 0)
        )
        
        constraintsView.append(NSLayoutConstraint(
            item: noConnectionView,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self.view,
            attribute: .Leading,
            multiplier: 1,
            constant: 0)
        )
        
        constraintsView.append(NSLayoutConstraint(
            item: noConnectionView,
            attribute: NSLayoutAttribute.Trailing,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self.view,
            attribute: .Trailing,
            multiplier: 1,
            constant: 0)
        )
        
        errorHeight = NSLayoutConstraint(
            item: noConnectionView,
            attribute: .Height,
            relatedBy: .Equal,
            toItem: nil,
            attribute: NSLayoutAttribute.NotAnAttribute,
            multiplier: 1,
            constant: 30)

        constraintsView.append(errorHeight!)
        
        self.view.addConstraints(constraintsView)
        
        let noConnectionLabel:UILabel = UILabel()
        noConnectionLabel.translatesAutoresizingMaskIntoConstraints = false
        noConnectionLabel.backgroundColor = UIColor.clearColor()
        noConnectionLabel.text = "no connection :("
        noConnectionLabel.textColor = UIColor.redColor().colorWithAlphaComponent(0.7)
        noConnectionLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)

        noConnectionView.addSubview(noConnectionLabel)
        
        var constraintsLabel = [NSLayoutConstraint]()
        
        constraintsLabel.append(NSLayoutConstraint(
            item: noConnectionLabel,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: noConnectionView,
            attribute: .CenterX,
            multiplier: 1,
            constant: 0)
        )
        
        constraintsLabel.append(NSLayoutConstraint(
            item: noConnectionLabel,
            attribute: NSLayoutAttribute.Bottom,
            relatedBy: NSLayoutRelation.Equal,
            toItem: noConnectionView,
            attribute: .Bottom,
            multiplier: 1,
            constant: 0)
        )
        
        constraintsLabel.append(NSLayoutConstraint(
            item: noConnectionLabel,
            attribute: .Height,
            relatedBy: .Equal,
            toItem: nil,
            attribute: NSLayoutAttribute.NotAnAttribute,
            multiplier: 1,
            constant: 30)
        )
        
        self.view.addConstraints(constraintsLabel)
        
        delay(0.0){
            UIView.animateWithDuration(0.65, delay: 1.5, options: UIViewAnimationOptions.CurveEaseOut, animations:
                {
                    
                    self.errorHeight!.constant = 0
                    self.view.layoutIfNeeded()
                    
                }, completion: {finished in
                    noConnectionLabel.removeFromSuperview()
                    noConnectionView.removeFromSuperview()
            })
        }
    }
    
    
    func noMemories(){
        let noMemories:UILabel = UILabel()
        noMemories.translatesAutoresizingMaskIntoConstraints = false
        noMemories.backgroundColor = UIColor.clearColor()
        noMemories.text = "no memories ... GO MAKE SOME!"
        noMemories.textColor = UIColor.darkGrayColor()
        noMemories.font = UIFont(name: "AvenirNext-Regular", size: 18)
        
        self.view.addSubview(noMemories)
        
        var constraintsView = [NSLayoutConstraint]()
        constraintsView.append(NSLayoutConstraint(
            item: noMemories,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .CenterX,
            multiplier: 1,
            constant: 0)
        )
        
        constraintsView.append(NSLayoutConstraint(
            item: noMemories,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .CenterY,
            multiplier: 1,
            constant: -50)
        )
        
        
        self.view.addConstraints(constraintsView)
        
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {


        let cell:CollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CollectionViewCell
        cell.cellImage.image = images[indexPath.item]
        
        return cell
        
    }
    
    func writeVidTemp (data: NSData) -> NSURL {
        
        let outputPath:String?
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        outputPath = "\(documentsPath)/vidThumbNail.mov"
        let outputFileUrl = NSURL(fileURLWithPath: outputPath!)
        
        let error2 = NSErrorPointer()
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(outputPath!) {
            do {
                try fileManager.removeItemAtPath(outputPath!)
            } catch let error as NSError {
                error2.memory = error
            }
        }
        
        _ = data.writeToFile(outputPath!, atomically: false)
        return outputFileUrl
        
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let picComments = self.storyboard!.instantiateViewControllerWithIdentifier("PicComments") as! PicCommentsViewController
        picComments.activity = self.activities[indexPath.item]
        picComments.name = name
        picComments.fromViewAll = true
        self.navigationController!.pushViewController(picComments, animated: true)
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if images.count > 15 && reloading == false {
            if scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height {

                populateFiles(true)
            }
        }
    }
    
    var noMoreMedia:Bool = false
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if images.count < 15 || noMoreMedia == true {
            return CGSize(width: self.view.frame.width, height: 0.1)
        } else {
            return CGSize(width: self.view.frame.width, height: 40)

        }
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let footerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "footer", forIndexPath: indexPath) as! FooterCollectionView
        footerView.tag = 99
        
        footerView.activityIndicator.startAnimating()
        
        return footerView
    }
    


    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   
}
