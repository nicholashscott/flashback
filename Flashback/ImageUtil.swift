

import UIKit

class ImageUtil: NSObject {
    
    static func cropToSquare(image originalImage: UIImage) -> UIImage {

        let contextImage: UIImage = UIImage(CGImage: originalImage.CGImage!)
        
        // Get the size of the contextImage
        let contextSize: CGSize = contextImage.size
        
        let posX: CGFloat
        let posY: CGFloat
        let width: CGFloat
        let height: CGFloat
        
        // these are flipped
            posX = 0.15 * contextSize.height * 1.3
            posY = 0

            width = 1.3 * contextSize.height
            height = contextSize.height
        
        
        let rect: CGRect = CGRectMake(posX, posY, width, height)
        
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: originalImage.scale, orientation: originalImage.imageOrientation)
        
        return image
    }
    
}
