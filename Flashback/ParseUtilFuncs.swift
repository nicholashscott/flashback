//  Created by Nick Scott


import CoreData
class ParseUtilFuncs {
    
   
    
    class func populateBuddiesLocal(completionHandler: (tempBuddies: [PFUser]) -> ()){
        
        let query = PFQuery(className: "buddies")
        query.includeKey("friend")
        query.fromLocalDatastore()
        query.findObjectsInBackground().continueWithBlock {
            (task: BFTask!) -> AnyObject in
            if let error = task.error {
                print("Error: \(error)")
                return task
            }
            var tempBuddies = [PFUser]()
            let results = task.result as! NSArray

            for result in results {
                tempBuddies.append(result.objectForKey("friend") as! PFUser)
            }
            
            tempBuddies = tempBuddies.sort { ($0["username"]! as! String).localizedCaseInsensitiveCompare($1["username"] as! String) == NSComparisonResult.OrderedAscending }

            completionHandler(tempBuddies: tempBuddies)
            return task
        }
    }
    
    class func populateBuddies(completionHandler: (tempBuddiesGlobal: [PFUser]?, error:Bool) -> ()){
        
        let user:PFObject = PFUser.currentUser()!
        
        let buddiesQuery = PFQuery(className: "buddies")
        buddiesQuery.whereKey("user", equalTo: user )
        buddiesQuery.includeKey("friend")
        buddiesQuery.findObjectsInBackgroundWithBlock {
            (objects, error) -> Void in
            if error == nil {
                let query = PFQuery(className: "buddies")
                query.fromLocalDatastore()
                query.findObjectsInBackground().continueWithBlock {
                    (task: BFTask!) -> AnyObject in
                    if let error = task.error {
                        print("Error: \(error)")
                        return task
                    }
                    let results:Array = task.result as! NSArray as AnyObject as! [PFObject]
                    PFObject.unpinAllInBackground(results, block: {
                        (success, error) -> Void in
                        if success {
                            PFObject.pinAllInBackground(objects, block: {
                                (success, error) -> Void in
                                if success {
                                    self.populateBuddiesLocal{ tempBuddies in
                                        let tempBuddiesGlobal:[PFUser] = tempBuddies
                                        completionHandler(tempBuddiesGlobal: tempBuddiesGlobal, error: false)
                                    }
                                }
                            })
                            
                        }
                        
                    })
                    
                    return task
                }
                
            } else {
                completionHandler(tempBuddiesGlobal: nil, error: true)
            }
        }
    }

    
    class func populateGroupsLocal(completionHandler: (tempGroupsLocal: [PFObject]) -> ()){
        let query = PFQuery(className: "Group")
        query.includeKey("members")
        query.fromLocalDatastore()
        query.findObjectsInBackground().continueWithBlock {
            (task: BFTask!) -> AnyObject in
            if let error = task.error {
                print("Error: \(error)")
                return task
            }
            
            var results:[PFObject] = task.result as! NSArray as AnyObject as! [PFObject]
            
            if results.count > 1 {
            results = results.sort({ $0.createdAt!.compare($1.createdAt!) == NSComparisonResult.OrderedDescending })
            }

            
            completionHandler(tempGroupsLocal: results)

            
            return task
        }
    }

    

    
    class func populateGroups(completionHandler: (tempGroupsGlobal: [PFObject]?, error:Bool) -> ()){
        
        let user:PFObject = PFUser.currentUser()!
        
        let groupQuery = PFQuery(className: "Group")
        groupQuery.includeKey("members")
        groupQuery.whereKey("members", containsAllObjectsInArray: [user])
        groupQuery.findObjectsInBackgroundWithBlock {
            (objects, error) -> Void in
            if error == nil {
                let query = PFQuery(className: "Group")
                query.fromLocalDatastore()
                query.findObjectsInBackground().continueWithBlock {
                    (task: BFTask!) -> AnyObject in
                    if let error = task.error {
                        print("Error: \(error)")
                        return task
                    }
                    
                    let results:[PFObject] = task.result as! NSArray as AnyObject as! [PFObject]
        
                    PFObject.unpinAllInBackground(results, block: {
                        (success, error) -> Void in
                        if success {
                            
                            PFObject.pinAllInBackground(objects, block: {
                                (success, error) -> Void in
                                if success {
                                    self.populateGroupsLocal{ (tempGroups) in
                                        let tempGroupsGlobal:[PFObject] = tempGroups
    
                                        completionHandler(tempGroupsGlobal: tempGroupsGlobal, error: false)
                                    }
                                }
                            })
                            
                        }
                        
                    })
                    
                    return task
                }
            }else {
                
                self.populateGroupsLocal{ (tempGroups) in
                    
                    completionHandler(tempGroupsGlobal: nil, error: true)
                    
                }
                
            }
        }
    }
    
    class func recentBuddies(completionHandler: (tempRecentGlobal:[PFUser]) -> ()){
        
        let user:PFObject = PFUser.currentUser()!
        
        let userSenderQuery = PFQuery(className: "Activity")
        userSenderQuery.whereKey("user", equalTo: user)
        userSenderQuery.whereKeyExists("receiver")
        userSenderQuery.selectKeys(["receiver"])
        userSenderQuery.orderByDescending("createdAt")
        userSenderQuery.findObjectsInBackgroundWithBlock {
            (objects, error) -> Void in
            if error == nil {
                
                PFObject.unpinAllObjectsInBackgroundWithName("recentBuddies", block: {
                    (success, error) -> Void in
                    var counter = 0
                    var toSave = [PFObject]()
                    var tempNames = [String]()
                    for object in objects! {
                        let user:PFUser = object["receiver"] as! PFUser
                        let name:String = user.username!
                        if counter <= 4 {
                            if !tempNames.contains(name){
                                tempNames.append(name)
                                let post = PFObject(className: "buddiesRecent")
                                post["receiver"] = object["receiver"] as! PFUser
                                post["createdAt"] = object.createdAt!
                                
                                toSave.append(post as PFObject)
                                counter++
                            }
                        } else {
                            userSenderQuery.cancel()
                        }
                    }
                    PFObject.pinAllInBackground(toSave, withName: "recentBuddies", block: {
                        (success, error) -> Void in
                        if success {

                            self.populateRecentLocal{ tempRecentBuddies in
                                
                                let tempRecentGlobal:[PFUser] = tempRecentBuddies
                                completionHandler(tempRecentGlobal: tempRecentGlobal)
                            }
                        }
                        
                    })
                })
            }
        }
    }
    
    class func deleteExtraRecentLocal(){
        let query = PFQuery(className: "buddiesRecent")
        query.fromLocalDatastore()
        query.fromPinWithName("recentBuddies")
        query.findObjectsInBackground().continueWithBlock {
            (task: BFTask!) -> AnyObject in
            if let error = task.error {
                print("Error: \(error)")
                return task
            }
            
            PFObject.unpinAllObjectsInBackgroundWithName("recentBuddies", block: {
                (success, error) -> Void in
                    var buddiesRecent:[PFUser] = [PFUser]()
                    var results = task.result as! NSArray as AnyObject as! [PFObject]
                    
                    results = results.sort({ ($0["createdAt"]! as! NSDate).compare($1["createdAt"]! as! NSDate) == NSComparisonResult.OrderedDescending })
                    
                    var filtered:[PFObject] = [PFObject]()

                
                    var five = 0
                    for i in 0..<results.count{
                        if five <= 4 {
                            if !buddiesRecent.contains(results[i]["receiver"] as! PFUser) {
                                buddiesRecent.append(results[i]["receiver"] as! PFUser)
                                filtered.append(results[i])
                                five++
                            }else {
                            }
                        }
                    }
                
                    PFObject.pinAllInBackground(filtered, withName: "recentBuddies")
                
                })
            
            
            return task
        }
    }
    
    class func deleteExtraRecentGroups(){
        
        let query = PFQuery(className: "groupsRecent")
        query.fromLocalDatastore()
        query.fromPinWithName("recentGroups")
        query.findObjectsInBackground().continueWithBlock {
            (task: BFTask!) -> AnyObject in
            if let error = task.error {
                print("Error: \(error)")
                return task
            }
            
            PFObject.unpinAllObjectsInBackgroundWithName("recentGroups", block: {
                (success, error) -> Void in

                    var groupsRecent:[PFObject] = [PFObject]()
                    var results = task.result as! NSArray as AnyObject as! [PFObject]
                    
                    var filtered:[PFObject] = [PFObject]()
                
                    results = results.sort({ ($0["createdAt"]! as! NSDate).compare($1["createdAt"]! as! NSDate) == NSComparisonResult.OrderedDescending })
                    var five = 0
                    for i in 0..<results.count{
                        if five <= 4 {
                            if !groupsRecent.contains(results[i]["group"] as! PFObject) {
                                groupsRecent.append(results[i]["group"] as! PFObject)
                                filtered.append(results[i])
                                five++
                            }
                        }
                    }
                
                PFObject.pinAllInBackground(filtered, withName: "recentGroups")

                })
            
            
            return task
        }
        
    }
    
    
    class func populateRecentLocal(completionHandler: (tempBuddies: [PFUser]) -> ()){
        
        
        let query = PFQuery(className: "buddiesRecent")
        query.fromLocalDatastore()
        query.fromPinWithName("recentBuddies")
        query.findObjectsInBackground().continueWithBlock {
            (task: BFTask!) -> AnyObject in
            if let _ = task.error {
                return task
            }
            var buddiesRecent:[PFUser] = [PFUser]()
            let results = task.result as! NSArray as AnyObject as! [PFObject]
            
            for result in results {
                buddiesRecent.append(result["receiver"] as! PFUser)
            }
            
            completionHandler(tempBuddies: buddiesRecent)
            return task
        }
    }
    

    
    class func populateRecentGroupsLocal(completionHandler: (tempGroupsLocal:[PFObject]) -> ()){
        
        let query = PFQuery(className: "groupsRecent")
        query.includeKey("group")
        query.fromLocalDatastore()
        query.fromPinWithName("recentGroups")
        query.findObjectsInBackground().continueWithBlock {
            (task: BFTask!) -> AnyObject in
            if let _ = task.error {
                return task
            }
            
            
            var tempGroupsLocal = [PFObject]()
            
            
            let results = task.result as! NSArray
            for result in results {
                tempGroupsLocal.append(result["group"] as! PFObject)
            }
            
            completionHandler(tempGroupsLocal: tempGroupsLocal)
            
            
            
            return task
        }
    }
    
    
    
    
    class func recentGroups(completionHandler: (tempGroups:[PFObject]) -> ()){
        
        let user:PFObject = PFUser.currentUser()!
        
        let userSenderQuery = PFQuery(className: "Activity")
        userSenderQuery.whereKey("user", equalTo: user)
        userSenderQuery.whereKeyExists("group")
        userSenderQuery.includeKey("group")
        userSenderQuery.orderByDescending("createdAt")
        userSenderQuery.selectKeys(["group"])
        
        userSenderQuery.findObjectsInBackgroundWithBlock {
            (objects, error) -> Void in
            if error == nil {
                PFObject.unpinAllObjectsInBackgroundWithName("recentGroups", block: {
                    (success, error) -> Void in
                    var counter = 0
                    var toSave = [PFObject]()
                    var recentGroups = [PFObject]()
                    
                    for object in objects! {
                        if counter <= 4 {
                            if !recentGroups.contains(object["group"] as! PFObject){
                                
                                recentGroups.append(object["group"] as! PFObject)
                                
                                let post = PFObject(className: "groupsRecent")
                                post["group"] = object["group"]
                                post["createdAt"] = object.createdAt!
                                
                                toSave.append(post as PFObject)
                                counter++
                            }
                        } else {
                            userSenderQuery.cancel()
                        }
                    }
                    PFObject.pinAllInBackground(toSave, withName: "recentGroups", block: {
                        (success, error) -> Void in
                        if success {
                            self.populateRecentGroupsLocal{ (tempGroupsLocal) in

                                completionHandler(tempGroups: tempGroupsLocal)
                            }
                        }
                        
                    })
                })
            }
        }
    }
    

}





