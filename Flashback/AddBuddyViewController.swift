//  Created by Nick Scott

protocol BuddiesDelegate {
    func populateBuddiesLocal()
}
protocol ContactsDelegate {
    func addFromContacts()
}

import UIKit

class AddBuddyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var delegate2:BuddiesDelegate? //updates buddies from local
    var delegate3:ContactsDelegate? //brings up add from contacts window
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewBud: UIView!
    @IBOutlet var bottomKeyboardConstraint: NSLayoutConstraint!
    
    @IBAction func addFromContacts(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {});
        delegate3?.addFromContacts()
    }
    
    @IBAction func done(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: {});
    }
    
    var currentBuddies = [String]()
    var fromBuddies:Bool!
    var user:PFUser = PFUser.currentUser()!
    
    var potentialBuddies = [PFUser]()
    var checkmarks = [Bool]()
    var loading = [Bool]()
    var filteredBuddies: [PFUser] = []
    
    var counter:Int = 0
    
    override func viewDidLayoutSubviews() {
        viewBud.layer.cornerRadius = 5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardNotificationHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        viewBud.layer.cornerRadius = 5
        
        self.searchBar.returnKeyType = UIReturnKeyType.Done
        self.searchBar.autocapitalizationType = UITextAutocapitalizationType.None
        
        ParseUtilFuncs.populateBuddiesLocal{ tempBuddies in
            for buddy in tempBuddies {
                self.currentBuddies.append(buddy.username!)
            }
        }
    }
    
    func filterContentForSearchText() {
        self.potentialBuddies.removeAll()
        self.checkmarks.removeAll()
        self.loading.removeAll()

        if searchBar.text != "" {
            let query = PFUser.query()
            query?.whereKey("userLower", hasPrefix: searchBar.text!.lowercaseString)
            query?.limit = 10
            query!.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                if error == nil {
                    self.potentialBuddies.removeAll(keepCapacity: true)
                    self.counter++
                    for object in objects! {
                        let potentialBuddy:PFUser = object as! PFUser
                        let stringPotentialBuddy:String = potentialBuddy.username!
                        if !self.currentBuddies.contains(stringPotentialBuddy){
                            self.potentialBuddies.append(potentialBuddy)
                            self.checkmarks.append(false)
                            self.loading.append(false)
                        }
                    }
                    self.filteredBuddies = self.potentialBuddies
                    self.tableView.reloadData()
                }
            }
        }else{
            self.filteredBuddies.removeAll()
            self.tableView.reloadData()
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterContentForSearchText()
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
        
        if self.loading[indexPath.row] == false && self.checkmarks[indexPath.row] == false{
            let tempCounter = counter
            self.loading[indexPath.row] = true
            let activityIndicator = UIActivityIndicatorView()
            
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
                activityIndicator.frame = CGRectMake(cell.contentView.frame.width - 32, cell.contentView.frame.height/2 - 10, 20, 20)
                cell.contentView.addSubview(activityIndicator)
                activityIndicator.startAnimating()
            }
            
            let post = PFObject(className: "buddies")
            post.setObject(self.user, forKey: "user")
            post.setObject(self.filteredBuddies[indexPath.row], forKey: "friend")
            let acl = PFACL()
            acl.setReadAccess(true, forUser: self.user)
            acl.setWriteAccess(true, forUser: self.user)
            post.ACL = acl
            post.saveInBackgroundWithBlock{
                (success, error) -> Void in
                if success{
                    if !self.currentBuddies.contains((self.filteredBuddies[indexPath.row].username!)) {
                        post.pinInBackgroundWithBlock{
                            (success, error) -> Void in
                            if success {
                                self.delegate2?.populateBuddiesLocal()
                            }
                        }
                    }
                    let friend = (post["friend"] as? PFUser)!
                    self.currentBuddies.append(friend.username!)
                    if tempCounter == self.counter {
                        self.checkmarks[indexPath.row] = true
                    }
                    self.tableView.reloadData()

                } else if error != "taken" { // error from cloud code if already exists
                    activityIndicator.stopAnimating()
                    activityIndicator.hidden = false
                    activityIndicator.color = UIColor.redColor()
                    if tempCounter == self.counter {
                        self.loading[indexPath.row] = false
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell")!
        let listItem = filteredBuddies[indexPath.row].username!
        
        cell.textLabel?.text = listItem
        cell.textLabel?.font = UIFont(name: "AvenirNext-Regular", size: 17)
        
        if checkmarks[indexPath.row] == true {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }
        return cell
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredBuddies.count
    }
    
    override func viewWillDisappear(animated: Bool) {
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue()
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.bottomKeyboardConstraint?.constant = endFrame!.size.height + 10
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    
                }
            )
        }
    }
    
    func keyboardNotificationHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.bottomKeyboardConstraint?.constant = 50
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    
                }
            )
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}