//  Created by Nick Scott


import UIKit
import AVFoundation

func == (lhs: PicCommentsViewController.comment, rhs: PicCommentsViewController.comment) -> Bool {
    
    return lhs.commenter == rhs.commenter && lhs.createdAt == rhs.createdAt && lhs.message == rhs.message
}

class PicCommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    
    @IBOutlet var picVideoTitle: UINavigationItem!
    
    
    @IBOutlet var groupInfoLabel: UILabel!
    @IBOutlet var groupLabelHeight: NSLayoutConstraint!
    
    
    @IBOutlet var infoButton: UIBarButtonItem!
    
    var infoOpen = false
    
    
    @IBOutlet var tryAgainButton: UIButton!
    
    @IBAction func tryAgain(sender: AnyObject) {
        loadPicVideo()
        self.comments.removeAll()
        populateComments(.initial){}
        
    }
    
    @IBAction func infoGroup(sender: AnyObject) {
        
        if infoOpen == false {
            if let activity = activity {
                
                if activity["group"] != nil {
                    
                    let group:PFObject = (activity["group"] as? PFObject)!
                    
                    let groupName:String = (group["groupName"] as? String)! + ": "
                    
                    let groupMembersPF:[PFUser] = group["members"] as AnyObject? as! [PFUser]
                    var groupMembers = [String]()
                    
                    for member in groupMembersPF {
                        groupMembers.append(member.username!)
                    }
                    
                    let lastMember:String = groupMembers.last!
                    
                    groupMembers.removeLast()
                    
                    var joinMembers = groupMembers.joinWithSeparator(", ")
                    
                    joinMembers = joinMembers + " and " + lastMember
                    
                    let groupLabelText = groupName + joinMembers
                    
                    let nsGroupLabelText = NSMutableAttributedString(string: groupLabelText)
                    
                    nsGroupLabelText.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(15, weight: UIFontWeightBold), range: NSRange(location: 0,length: (groupName.characters.count) ))
                    nsGroupLabelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSRange(location:0,length: (groupName.characters.count)))
                    groupInfoLabel.attributedText = nsGroupLabelText
                    
                    groupInfoLabel.sizeToFit()
                    
                    UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.groupLabelHeight.constant = self.groupInfoLabel.frame.height + 8
                            self.view.layoutIfNeeded()
                            
                        }, completion: {finished in
                    })
                    
                    
                    infoOpen = true
                }
            }
        } else {
            UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations:
                { () -> Void in
                    
                    self.groupLabelHeight.constant = 0
                    self.view.layoutIfNeeded()
                    
                }, completion: {finished in
            })
            infoOpen = false
        }
    }
    
    @IBOutlet var noConnectionHeight: NSLayoutConstraint!
    @IBOutlet var noConnection: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var selectedImage: UIImageView!
    @IBOutlet var muteItem: UIButton!
    @IBOutlet var play: UIButton!
    @IBOutlet var drag: UIButton!
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet var verticalSpaceViewImage: NSLayoutConstraint! // for drag function
    
    var keyboardOpen:Bool = false
    
    var refreshError = false
    
    var activity:PFObject?
    var picVideo:PFObject?
    var fromViewAll:Bool?
    
    
    var picVideoId:String?
    var activityId:String?
    var activityCreatedAt:NSDate? 
    var activityIsVideo:Bool?
    var fromPush:Bool?
    var activityUsername:String?
    var activityMessage:String?
    
    var name:String?

    
    var player : AVPlayer? = nil
    var playerLayer : AVPlayerLayer? = nil
    var asset : AVAsset? = nil
    var playerItem: AVPlayerItem? = nil
    
    let audioOffImage = UIImage(named: "audioOff")
    let audioOnImage = UIImage(named: "audioOn")
    
    class comment:Equatable {
        var commenter:String?
        var message:String?
        var createdAt:NSDate?
        var status:aIStatus?

        init (commenter: String, message: String, createdAt: NSDate, status: aIStatus) {
            self.commenter = commenter
            self.message = message
            self.createdAt = createdAt
            self.status = status
        }
    }
    
    
    enum aIStatus {
        case loaded
        case loading
        case error
    }
    
    var comments = [comment]()
    
    var loadMoreSection:Bool = false
    
    var runningCounterPast:Int = 0
    var runningCounterFuture:Int = 0
    
    func sortByCreatedAt(lhs:comment, rhs:comment) -> Bool {
        let sort = lhs.createdAt!.compare(rhs.createdAt!)
        if sort == .OrderedAscending {
            return true
        } else {
            return false
        }
    }
    
    
    enum PopulateType {
        case initial
        case afterPost
        case loadMore
    }
    var initialLoadDate:NSDate?
    
    var commentUniqueIds = [String]()

    
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var keyboardConstraintImage: NSLayoutConstraint!
    var keyBoardHeight:CGFloat?
    
    
    func getActivityWhenPush(){
        
        let getActivity = PFQuery(className: "Activity")
        getActivity.includeKey("group")
        getActivity.getObjectInBackgroundWithId(activityId!){
            (object, error) -> Void in
            
            self.activity = object
            if self.activity!["group"] === nil {
                self.infoButton.title = ""
            }
        }
    }
    
    

    func displayAlert(title:String, error:String){
        
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
       
    override func viewWillDisappear(animated: Bool) {
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self)

    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    
    @IBAction func mute(sender: AnyObject) {
        if muted == true {
            muted = false
            muteItem.setImage(audioOnImage, forState: .Normal)
            if let player = player {
                player.muted = false
            }
        } else {
            muted = true
            muteItem.setImage(audioOffImage, forState: .Normal)
            if let player = player {
                player.muted = true
            }
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("muteNotification", object: self)

    }
    
    @IBAction func play(sender: AnyObject) {
        if muted==true{
            player?.muted = true
        } else {
            player?.muted = false
        }
        player?.play()
        player?.seekToTime(kCMTimeZero)
    }
    
    @IBAction func postIt(sender: AnyObject) {
        if textView.text != "" && textView.text != "post to Flashback..."  {

            postItGeneral(textView.text, again:false, tempComment: nil)
            if Reachability.isConnectedToNetwork() {
                self.textView.text = ""
            }
        }
    }
    
    
    func again(sender:UIButton){
        
        let buttonRow = sender.tag
        let tempComment = comments[buttonRow]
        tempComment.status = .loading
        let indexPath:NSIndexPath = NSIndexPath(forRow: buttonRow, inSection: 2)
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
 
        
        let text = tempComment.message
        self.postItGeneral(text!, again:true, tempComment:tempComment)
        
    }
    
    func postItGeneral(text:String, again:Bool, tempComment:comment?){
        if Reachability.isConnectedToNetwork() {
            if let activity = activity {
                noConnectionHeight.constant = 0
                self.refreshError = false
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                
                var newComment:comment?

                if again == false {
                    newComment = comment(commenter: PFUser.currentUser()!.username!, message: text, createdAt: NSDate(), status: .loading)
                    
                    self.comments.append(newComment!)
                    dispatch_async(dispatch_get_main_queue()) {

                        self.tableView.beginUpdates()
                        let row = self.comments.count - 1
                        let indexPath:NSIndexPath = NSIndexPath(forRow: row, inSection: 2)
                        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                        self.tableView.endUpdates()
                        self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                
                    }
                } else {
                    newComment = tempComment
                }
                
                let post = PFObject(className: "comments")
                post["madeBy"] = PFUser.currentUser()!.username!
                post["comment"] = text
                post["activity"] = activity
                
                let acl = PFACL()
                if let group:PFObject = activity["group"] as? PFObject {
                    let members:[PFUser] = group["members"] as AnyObject? as! [PFUser]
                    for member in members {
                        acl.setReadAccess(true, forUser: member)
                    }
                } else {
                    if let receiver = activity["receiver"] as? PFUser {
                        acl.setReadAccess(true, forUser: receiver)
                    }
                    acl.setReadAccess(true, forUser: (activity["user"] as? PFUser)!)
                    acl.setReadAccess(true, forUser: PFUser.currentUser()!)
                }
                
                acl.setWriteAccess(true, forUser: PFUser.currentUser()!)
                post.ACL = acl
                post.saveInBackgroundWithBlock{(success, error) -> Void in

                    if success == true {
                        self.populateComments(.afterPost){
                            dispatch_async(dispatch_get_main_queue()) {
                                self.tableView.beginUpdates()
                                newComment!.status = .loaded
                                let row:Int = self.comments.indexOf(newComment!)!
                                newComment!.createdAt = (post.createdAt)!

                                let indexPath:NSIndexPath = NSIndexPath(forRow: row, inSection: 2)
                                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                                self.tableView.endUpdates()
                            }
                        }
                        
                    } else {
                        newComment!.status = .error
                        dispatch_async(dispatch_get_main_queue()) {
                            self.tableView.beginUpdates()
                            let row:Int = self.comments.indexOf(newComment!)!
                            let indexPath:NSIndexPath = NSIndexPath(forRow: row, inSection: 2)
                            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                            self.tableView.endUpdates()
                        }
                        
                    }
                
                }
        
        }
        } else {
            if noConnectionHeight.constant != 30 {
                noConnectionHeight.constant = 30
                
                self.delay(3){
                    UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.noConnectionHeight.constant = 0
                            self.view.layoutIfNeeded()
                            
                        }, completion: {finished in
                            
                    })
                }
            }
        }
    
        
    }

    func populateComments(checked:PopulateType, completionHandler: () -> ()) {
        
        if Reachability.isConnectedToNetwork() {
            
            self.noConnectionHeight.constant = 0

            var initial:Bool?
            var afterPost:Bool?
            var loadMore:Bool?

            switch(checked) {
            case .initial:
                initial = true
            case .afterPost:
                afterPost = true
            case .loadMore:
                loadMore = true
            }
            
            let getComments = PFQuery(className: "comments")
            if fromPush == true {
                let getActivity = PFQuery(className: "Activity")
                getActivity.whereKey("objectId", equalTo: self.activityId!)
                getComments.whereKey("activity", matchesQuery: getActivity)

            } else {
                getComments.whereKey("activity", equalTo: activity!)
            }
            if loadMore == true || initial == true {
                getComments.orderByDescending("createdAt")
            } else {
                getComments.orderByDescending("createdAt")
            }
            if loadMore == true {
                getComments.skip = runningCounterPast
                if let initialDate = initialLoadDate {
                    getComments.whereKey("createdAt", lessThanOrEqualTo: initialDate)
                }
            } else if afterPost == true {
                getComments.skip = runningCounterFuture
                if let initialDate = initialLoadDate {
                    getComments.whereKey("createdAt", greaterThan: initialDate)
                }
            }
            let limit:Int = 10
            getComments.limit =  limit
            getComments.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                if error == nil {
                    var index = 0

                    if afterPost == true{
                        index = self.comments.count - 1
                    }
                    let reloadLastRow = self.comments.count - 1
                    var indexPaths = [NSIndexPath]()
                
                    for object in objects! {
                        
                        if loadMore == true || initial == true {
                            if objects!.count == limit {
                                self.loadMoreSection = true
                            } else {
                                self.loadMoreSection = false
                            }
                        }
                        
                        if self.commentUniqueIds.isEmpty {
                            self.initialLoadDate = object.createdAt!
                        }
                        if !self.commentUniqueIds.contains(((object.objectId)!)) {
                            
                            self.commentUniqueIds.append((object.objectId)!)
                            let name:String = (object["madeBy"] as? String)!
                            let newComment = comment(commenter: name, message: (object["comment"] as? String)!, createdAt: object.createdAt!, status: .loaded)
                            if initial == true {
                                self.comments.append(newComment)
                                self.runningCounterPast++
                            } else if (afterPost == true && name != PFUser.currentUser()!.username!) {
                                self.comments.append(newComment)
                                self.runningCounterFuture++
                                let indexPath = NSIndexPath(forItem: index++, inSection: 2)
                                indexPaths.append(indexPath)
                                
                            } else if loadMore == true  {
                                self.comments.append(newComment)
                                self.runningCounterPast++
                                let indexPath = NSIndexPath(forItem: index++, inSection: 2)
                                indexPaths.append(indexPath)
                                
                            }
                        }
                    }
                    
                    completionHandler()

                    if initial == true {
                        self.comments.sortInPlace(self.sortByCreatedAt)

                        self.tableView.reloadData()
                    } else {
                        self.comments.sortInPlace(self.sortByCreatedAt)
                        self.tableView.beginUpdates()
                        let lastIndexPath = NSIndexPath(forItem: max(0, reloadLastRow), inSection: 2)
                        self.tableView.reloadRowsAtIndexPaths([lastIndexPath], withRowAnimation: UITableViewRowAnimation.None)
                        
                        self.tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.None)
                        self.tableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
                        self.tableView.endUpdates()
                    }
                    
                    
                }else{
                    if self.noConnectionHeight.constant != 30 {
                        self.noConnectionHeight.constant = 30
                        self.delay(3){
                            UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                { () -> Void in
                                    self.noConnectionHeight.constant = 0
                                    self.view.layoutIfNeeded()
                                }, completion: {finished in
                            })
                        }
                    }
                }
            }
        
        } else {
            if noConnectionHeight.constant != 30 {
                noConnectionHeight.constant = 30
                
                self.delay(3){
                    UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.noConnectionHeight.constant = 0
                            self.view.layoutIfNeeded()
                            
                        }, completion: {finished in
                            
                    })
                }
            }
        }
    }
    
    func loadMoreFunc() {
        populateComments(.loadMore){}
    }

    func writeVidTemp (data: NSData){


        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let outputPath = "\(documentsPath)/playVid.mov"
        let outputFileUrl = NSURL(fileURLWithPath: outputPath)
        
        let error2 = NSErrorPointer()
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(outputPath) {
            do {
                try fileManager.removeItemAtPath(outputPath)
            } catch let error as NSError {
                error2.memory = error
            }
        }
        
        let test = data.writeToFile(outputPath, atomically: false)
        if test {
            playVideo(outputFileUrl)
            self.play.hidden = false
            self.muteItem.hidden = false
            activityIndicator.stopAnimating()
            activityIndicator.hidden = true
        }
    }
    
    func playVideo(vidURL: NSURL) {
        let videoURL = vidURL
        asset = AVAsset(URL: videoURL)
        playerItem = AVPlayerItem(asset: asset!)
        player = AVPlayer(playerItem: self.playerItem!)
        
        if let player = player {
            playerLayer = AVPlayerLayer(player: self.player)
            playerLayer!.frame = CGRectMake(0, 0, selectedImage.frame.width, selectedImage.frame.height)
            self.selectedImage.layer.addSublayer(playerLayer!)
            player.actionAtItemEnd = .None
        }
    }
    
    func lowerAlpha(sender:UIButton) {
        sender.alpha = 0.6
    }
    
    func raiseAlpha(sender:UIButton) {
        sender.alpha = 0.3
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        var shouldChange = false

        if(text == "\n") {
            textView.resignFirstResponder()
            shouldChange = false
        } else if textView.text.characters.count + text.characters.count < 300 {
            shouldChange = true
        }
        
        return shouldChange
    }
    
    func loadPicVideo(){
        
        self.tryAgainButton.hidden = true
        activityIndicator.startAnimating()
        activityIndicator.color = UIColor.highlightColor()
        activityIndicator.hidden = false
        
        if Reachability.isConnectedToNetwork(){
            
            var isVideo:Bool!
            var uniqueId:String!
            
            if self.fromPush == true{
                uniqueId = self.picVideoId!
                isVideo = self.activityIsVideo!
            }else{
                isVideo = (self.activity!["video"] as! Bool)
                let picVideo:PFObject = (self.activity!["picVideo"] as? PFObject)!
                uniqueId = picVideo.objectId!
            }
            let viewedQuery = PFQuery(className: "PicVideo")
            viewedQuery.getObjectInBackgroundWithId(uniqueId){
                (object, error) -> Void in
                if error == nil {
                    
                    let file:PFFile = (object!["mediaFile"] as? PFFile)!
                    
                    file.getDataInBackgroundWithBlock{
                        (data, error) -> Void in
                        if error == nil{
                            if isVideo == true {
                                self.writeVidTemp(data!)
                            }else {
                                self.playerLayer?.removeFromSuperlayer()
                                let image = UIImage(data: data!)
                                self.selectedImage.image = image
                                self.activityIndicator.stopAnimating()
                                self.activityIndicator.hidden = true
                            }
                        }
                    }
                } else {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.color = UIColor.redColor()
                    self.tryAgainButton.hidden = false
                }
            }
        } else {
            delay(0.5){
                self.activityIndicator.stopAnimating()
                self.activityIndicator.color = UIColor.redColor()
                self.tryAgainButton.hidden = false
            }
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.infoButton.title = ""
        
        
        let newTitle:UILabel = UILabel()
        newTitle.frame = CGRectMake(0, 0, 0, 0)
        newTitle.font = UIFont(name: "AvenirNext-DemiBold", size: 30)
        newTitle.textAlignment = .Center
        newTitle.baselineAdjustment = .AlignCenters
        newTitle.textColor = UIColor.whiteColor()
        newTitle.text = name
        newTitle.minimumScaleFactor = 0.5
        newTitle.adjustsFontSizeToFitWidth = true
        newTitle.sizeToFit()
    
        picVideoTitle.titleView = newTitle

        textView.delegate = self
        
        self.containerView.backgroundColor = UIColor(white: 1, alpha: 0.8)
        self.tableView.backgroundColor = UIColor(white: 1, alpha: 0.0)
        
        self.play.hidden = true
        self.muteItem.hidden = true

        play.layer.borderColor = UIColor.highlightColor().CGColor
        play.layer.borderWidth = 1
        play.layer.backgroundColor = UIColor.lightGrayColor().CGColor
        play.alpha = 0.7
        play.addTarget(self, action: "lowerAlpha:", forControlEvents: UIControlEvents.TouchDown)
        play.addTarget(self, action: "raiseAlpha:", forControlEvents: UIControlEvents.TouchCancel)
        
        if fromViewAll == false {
            let isVideo:Bool = (self.activity!["video"] as! Bool)
            let mediaFile:PFFile = (self.picVideo!["mediaFile"] as! PFFile)
            mediaFile.getDataInBackgroundWithBlock{
                (data, error) -> Void in
                if error == nil{
                    if isVideo == true {
                        self.writeVidTemp(data!)
                    }else {
                        self.playerLayer?.removeFromSuperlayer()
                        let image = UIImage(data: data!)
                        self.selectedImage.image = image
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                    }
                }
            }
        }else {
            loadPicVideo()
        }
        
        if fromPush == true {
            getActivityWhenPush()
        } else {
            if self.activity!["group"] === nil {
                self.infoButton.title = ""
            }else{
                self.infoButton.title = "( i )"

            }
        }

        let gesture = UIPanGestureRecognizer(target: self, action: Selector("wasDragged:"))
        self.drag.addGestureRecognizer(gesture)
        self.drag.userInteractionEnabled = true
        
        self.view.addSubview(self.containerView)
        
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "muteNotification:", name:"muteNotification", object: nil)
        center.addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardNotificationHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        populateComments(.initial) {}
        
    }
    
    
    func muteNotification(notification:NSNotification){
        dispatch_async(dispatch_get_main_queue()) {
            if muted == false {
                self.muteItem.setImage(self.audioOnImage, forState: UIControlState.Normal)
                if let player = self.player {
                    player.muted = false
                }
            } else {
                self.muteItem.setImage(self.audioOffImage, forState: UIControlState.Normal)
                if let player = self.player {
                    player.muted = true
                }
            }
        }
    }
    
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue()
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardConstraintImage.constant = self.selectedImage.frame.height
            self.keyboardHeightLayoutConstraint?.constant = endFrame!.size.height
            self.keyBoardHeight = endFrame?.height // for drag function
            view.addSubview(commentView)
            let indexPath2 = NSIndexPath(forRow: self.comments.count - 1, inSection: 2)
            if self.comments.count > 0 {
                self.tableView.scrollToRowAtIndexPath(indexPath2, atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
            }
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    if self.comments.count > 0 {
                        let indexPath2 = NSIndexPath(forRow: self.comments.count - 1, inSection: 2)
                        self.tableView.scrollToRowAtIndexPath(indexPath2, atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
                    }
                    self.keyboardOpen = true // for drag function
                }
            )
        }
    }

    
    func     keyboardNotificationHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardConstraintImage.constant = 35
            self.keyboardHeightLayoutConstraint?.constant = 0
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: {finished in
                    self.keyboardOpen = false // for drag function
                }
            )
        }
    }
    
 
    

    func wasDragged(gesture: UIPanGestureRecognizer) {
        
        var yFromCenter: CGFloat = 0
        let translation = gesture.translationInView(self.view)
        let drag = gesture.view!
        let scale = min(200 / abs(yFromCenter), 1)
        yFromCenter += translation.y

        let yLimitTest = containerView.frame.minY + translation.y
        let yLimitTestOpen = yLimitTest
        
        if keyboardOpen == false {
            if (yLimitTest <= (selectedImage.frame.maxY)) && yLimitTest >= selectedImage.frame.minY  {
                
                drag.center = CGPoint(x: drag.center.x, y: drag.center.y + translation.y)
                gesture.setTranslation(CGPointZero, inView: self.view)
                drag.transform = CGAffineTransformScale(self.view.transform, scale, scale)
                verticalSpaceViewImage.constant = verticalSpaceViewImage.constant - translation.y
                
            }
        }else {
            let commentY = self.commentView.frame.minY - self.drag.frame.height + 5
            if (yLimitTestOpen <= commentY) && yLimitTest >= selectedImage.frame.minY  {
                
                drag.center = CGPoint(x: drag.center.x, y: drag.center.y + translation.y)
                gesture.setTranslation(CGPointZero, inView: self.view)
                drag.transform = CGAffineTransformScale(self.view.transform, scale, scale)
                verticalSpaceViewImage.constant = verticalSpaceViewImage.constant - translation.y
            }
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        textView.text = nil
        textView.textColor = UIColor.blackColor()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section){
            
        case 0:
            return 1
        
        case 1:
            if loadMoreSection == false {
                return 0
            } else {
                return 1
            }
        case 2:
            return self.comments.count
        default:
            return 1
            
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch(indexPath.section){
            
        case 0:
            return UITableViewAutomaticDimension
        case 1:
            
            return UITableViewAutomaticDimension
        case 2:
            return UITableViewAutomaticDimension
        default:
            return UITableViewAutomaticDimension
        }

    }
    
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch(indexPath.section){
        case 0:
            return UITableViewAutomaticDimension
        case 1:
            return UITableViewAutomaticDimension
        case 2:
            return UITableViewAutomaticDimension
        default:
            return UITableViewAutomaticDimension
        }
    }


    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        switch (indexPath.section) {
        case 0:
            let cell2:PicCell = tableView.dequeueReusableCellWithIdentifier("picCell") as! PicCell
            
            cell2.backgroundColor = UIColor(white: 1, alpha: 0.0)
            cell2.commentCell.numberOfLines = 0
            cell2.commentCell.lineBreakMode = NSLineBreakMode.ByWordWrapping
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            var username:String!
            var message:String!
            var creationDate:NSDate!

            if fromPush == true {
                creationDate = activityCreatedAt!
            }else{
                creationDate = self.activity!.createdAt!
            }
            
            if self.fromPush == true {
                username = activityUsername
                message = activityMessage
            }else{
                let user:PFUser = self.activity!["user"] as! PFUser
                username = "taken by: " + user.username!
                message = self.activity!["title"] as! String
            }
            cell2.activityIndicator.hidden = true
            cell2.again.hidden = true
  
            let userCalendar = NSCalendar.currentCalendar()
            
            let myWeekday = NSCalendar.currentCalendar().components(NSCalendarUnit.Weekday, fromDate: creationDate).weekday
            
            var myWeekdayConverted:String?
            
            switch(myWeekday) {
            case 1:
                myWeekdayConverted = "Sun"
            case 2:
                myWeekdayConverted = "Mon"
            case 3:
                myWeekdayConverted = "Tue"
            case 4:
                myWeekdayConverted = "Wed"
            case 5:
                myWeekdayConverted = "Thu"
            case 6:
                myWeekdayConverted = "Fri"
            case 7:
                myWeekdayConverted = "Sun"
            default:
                myWeekdayConverted = "error"
            }
            
            let dateComponentsToday:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: NSDate())
            
            let dayToday = userCalendar.dateFromComponents(dateComponentsToday)!
            
            let dayYesterday = userCalendar.dateByAddingUnit(
                .Day,
                value: -1,
                toDate: dayToday,
                options: [])!
            
            let dateComponentsPicker:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: creationDate)
            let dayCreatedAt = userCalendar.dateFromComponents(dateComponentsPicker)!
            
            
            let dateString:String?
            if dayToday == dayCreatedAt {
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                dateString = "today " + formatter.stringFromDate(creationDate)
                cell2.dateLabel!.text = dateString!
            } else if dayYesterday == dayCreatedAt {
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                dateString = "yesterday " + formatter.stringFromDate(creationDate)
                cell2.dateLabel!.text = dateString!
            } else {
                dateString = formatter.stringFromDate(creationDate)
                cell2.dateLabel!.text = myWeekdayConverted! + " " + dateString!
            }
            
            cell2.username!.text = username
            cell2.commentCell!.font = UIFont.systemFontOfSize(13)
            cell2.commentCell!.text = message
            if cell2.commentCell.text == "" {
                
                cell2.commentCell.text = "...no caption..."
                cell2.commentCell.font = UIFont.systemFontOfSize(13, weight: UIFontWeightLight)
                cell2.commentCell.textColor = UIColor.lightGrayColor()
                
            }
            

            return cell2
        case 1:
            let reloadCell:ReloadCell = tableView.dequeueReusableCellWithIdentifier("reloadCell") as! ReloadCell
            reloadCell.loadMore.addTarget(self, action: "loadMoreFunc", forControlEvents: UIControlEvents.TouchUpInside)
            
            return reloadCell
        
        case 2:
            let cell2:PicCell = tableView.dequeueReusableCellWithIdentifier("picCell") as! PicCell
            
            cell2.backgroundColor = UIColor(white: 1, alpha: 0.0)
            cell2.commentCell.numberOfLines = 0
            cell2.commentCell.lineBreakMode = NSLineBreakMode.ByWordWrapping
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            var username:String!
            var message:String!
            var creationDate:NSDate!
            
            creationDate = comments[indexPath.row].createdAt!
            username = comments[indexPath.row].commenter!
            message = comments[indexPath.row].message!
            
            
            if comments[indexPath.row].status! == .loading {
                cell2.dateLabel.hidden = true
                cell2.again.hidden = true
                
                cell2.activityIndicator.hidden = false
                cell2.activityIndicator.startAnimating()
                
            } else if comments[indexPath.row].status! == .loaded {
                cell2.dateLabel.hidden = false
                cell2.again.hidden = true
                
                cell2.activityIndicator.hidden = true
                
                cell2.activityIndicator.stopAnimating()
            } else if comments[indexPath.row].status! == .error {
                cell2.dateLabel.hidden = true
                cell2.activityIndicator.hidden = false
                dispatch_async(dispatch_get_main_queue()) {
                    
                    cell2.activityIndicator.color = UIColor.redColor()
                }
                cell2.activityIndicator.stopAnimating()
                cell2.again.hidden = false
                cell2.again.tag = indexPath.row
                cell2.again.addTarget(self, action: "again:", forControlEvents: UIControlEvents.TouchUpInside)
            }
            
            let userCalendar = NSCalendar.currentCalendar()
            
            let myWeekday = NSCalendar.currentCalendar().components(NSCalendarUnit.Weekday, fromDate: creationDate).weekday
            
            var myWeekdayConverted:String?
            
            switch(myWeekday) {
            case 1:
                myWeekdayConverted = "Sun"
            case 2:
                myWeekdayConverted = "Mon"
            case 3:
                myWeekdayConverted = "Tue"
            case 4:
                myWeekdayConverted = "Wed"
            case 5:
                myWeekdayConverted = "Thu"
            case 6:
                myWeekdayConverted = "Fri"
            case 7:
                myWeekdayConverted = "Sun"
            default:
                myWeekdayConverted = "error"
            }
            
            let dateComponentsToday:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: NSDate())
            
            let dayToday = userCalendar.dateFromComponents(dateComponentsToday)!
            
            let dayYesterday = userCalendar.dateByAddingUnit(
                .Day,
                value: -1,
                toDate: dayToday,
                options: [])!
            
            let dateComponentsPicker:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: creationDate)
            let dayCreatedAt = userCalendar.dateFromComponents(dateComponentsPicker)!
            
            
            let dateString:String?
            if dayToday == dayCreatedAt {
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                dateString = "today " + formatter.stringFromDate(creationDate)
                cell2.dateLabel!.text = dateString!
            } else if dayYesterday == dayCreatedAt {
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                dateString = "yesterday " + formatter.stringFromDate(creationDate)
                cell2.dateLabel!.text = dateString!
            } else {
                dateString = formatter.stringFromDate(creationDate)
                cell2.dateLabel!.text = myWeekdayConverted! + " " + dateString!
            }
            
            cell2.username!.text = username
            
            cell2.commentCell!.font = UIFont.systemFontOfSize(13)

            cell2.commentCell!.text = message

            
            return cell2
        default:
            return UITableViewCell()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
