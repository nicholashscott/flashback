//  Created by Nick Scott


import UIKit
import CoreData

class ThreadsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UpdateThreads, HaveExtraDelegate {
    
    static var haveExtra:Bool = false
    @IBOutlet var connectionError: UILabel!
    @IBOutlet var connectionErrorHeight: NSLayoutConstraint!
    
    @IBOutlet var connectionErrorLabelHeight: NSLayoutConstraint! // to silence warning
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loadMoreActivity: UIActivityIndicatorView!
    
    @IBOutlet var showingLabel: UILabel!
    
    @IBOutlet var showOlderButton: UIButton!

    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var managedContext:NSManagedObjectContext?
    // last query of threads
    let defaults = NSUserDefaults.standardUserDefaults()
    
    static var delegate:ContainerView! // for scrolling
    var refresher = UIRefreshControl()
    
    struct infoStruct {
        var numberOfIncoming:Int?
        var numberOfFlashbacks:Int?
        var dateUnlocked = NSDate()
        var group:Bool?
        var groupMembers:NSArray?
        var uniqueId:String?
    }
    
    //ordered dict of flashbacks with uniqueId as key
    var threads = OrderedDictionary<String, infoStruct>()
    
    @IBAction func loadOld(sender: AnyObject) {
        showOlderButton.hidden = true
        loadMoreActivity.hidden = false
        
        loadMoreActivity.startAnimating()
        populateThreads(ThreadsChoice.loadOld)
    }

    
    @IBAction func left(sender: AnyObject) {
        ThreadsViewController.delegate!.disableScroll(ChildViews.B)
    }
    
    
    
    func extraFunc(sender:AnyObject?){

        ThreadsViewController.haveExtra = false
        haveExtraFunc()
        
        let AVc = storyboard!.instantiateViewControllerWithIdentifier("Extra") 
        
        self.navigationController?.pushViewController(AVc, animated: false)
        
    }
    
    func haveExtraFunc(){
        if ThreadsViewController.haveExtra == true {
            let extraImage = UIImage(named: "extra")
            extraButton.alpha = 1
            extraButton.setImage(extraImage, forState: UIControlState.Normal)
        } else {
            let extraImage = UIImage(named: "extraGrey")
            extraButton.alpha = 0.8
            extraButton.setImage(extraImage, forState: UIControlState.Normal)
        }
    }
    
    var extraButton = UIButton()
    var extra:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        AppDelegate.delegate = self
        connectionErrorLabelHeight.constant = 20
        
        extraButton.frame = CGRectMake(self.view.bounds.width - 45, 20, 40, 40)
        
        let extraImage = UIImage(named: "extraGrey")
        extraButton.alpha = 0.8
        extraButton.setImage(extraImage, forState: UIControlState.Normal)
        extraButton.addTarget(self, action: "extraFunc:", forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationController?.view.addSubview(extraButton)
        
        
        self.loadMoreActivity.hidden = true

        self.managedContext = self.appDelegate.managedObjectContext!
        
        configureTableView()
        
        
        if GlobalSignInUp == true {
            self.populateThreads(ThreadsChoice.initialPopulate)
        }else if GlobalShouldReload == false {
            self.fetchThreads(false)
            moreFlashbacksSignedIn()
        } else {
            self.populateThreads(ThreadsChoice.getNew)
            moreFlashbacksSignedIn()
        }
    
        refresher.addTarget(self, action: "getNewThreads", forControlEvents: UIControlEvents.ValueChanged)
        refresher.tintColor = UIColor.highlightColor()
        tableView.addSubview(refresher)
        
    
        if let globalPush = GlobalOpenedByPushNotification {
            if globalPush == "comment" {
                let picComment = self.storyboard!.instantiateViewControllerWithIdentifier("PicComments") as! PicCommentsViewController
                
                picComment.activityIsVideo = globalPushComment.isVideo
                picComment.picVideoId = globalPushComment.picVideoId
                picComment.activityId = globalPushComment.activityId
                picComment.fromPush = true
                picComment.activityCreatedAt = globalPushComment.createdAtNSDate
                picComment.activityUsername = globalPushComment.name
                picComment.activityMessage = globalPushComment.message
                
                picComment.name = globalPushComment.picVideoName

                self.navigationController!.pushViewController(picComment, animated: true)
            }
        }
        
    }
    
    
    func moreFlashbacksSignedIn(){
        if let tempFirst:NSDate = self.defaults.objectForKey("firstFlashback") as? NSDate {
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            let moreFlashbacks:Bool = self.defaults.objectForKey("moreFlashbacks") as! Bool
            var specialMessage:String = ""
            if moreFlashbacks == false {
                self.showOlderButton.hidden = true
                specialMessage = " - the moment you first flashedbacked..."
                
            } else {
                self.showOlderButton.hidden = false
                specialMessage = ""
            }
            let showingText = "fb's since " + formatter.stringFromDate(tempFirst) + specialMessage
            self.showingLabel.text = showingText
        }
    }
    
    enum ThreadsChoice {
        case initialPopulate
        case getNew
        case loadOld
    }
    
    func getNewThreads(){
        populateThreads(ThreadsChoice.getNew)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.hidden = false
        self.extraButton.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.extraButton.hidden = true

    }
    
    func prepPopulateThreads() -> [String] {
        var uniqueIds = [String]()
        let fetchRequest = NSFetchRequest(entityName:"Threads")
        var error:NSError?
        let fetchedResults = (try! managedContext!.executeFetchRequest(fetchRequest)) as? [Threads]
        if let results = fetchedResults {
            for result in results{
                uniqueIds.append(result.uniqueId)
                result.incoming = 0
            }
            do {
                try self.managedContext!.save()
            } catch let error1 as NSError {
                error = error1
                print("Could not save \(error), \(error?.userInfo)")
            }
            
        } else {
            print("Could not fetch \(error), \(error!.userInfo)")
        }
        return uniqueIds
    }
    
    func populateThreads(threadsChoice: ThreadsChoice) {
        self.fetchThreads(false)
        if Reachability.isConnectedToNetwork() {
            if self.connectionErrorHeight.constant == 20 {
                UIView.animateWithDuration(0.65, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations:
                    { () -> Void in
                        
                        self.connectionErrorHeight.constant = 0
                        self.view.layoutIfNeeded()
                        
                    }, completion: {finished in
                        
                })
            }
            if let user:PFUser = PFUser.currentUser() {
                var firstTime:Bool?
                var loadOld:Bool?
                var getNew:Bool?
                switch threadsChoice{
                case .initialPopulate:
                    firstTime = true
                case .getNew:
                    getNew = true
                case .loadOld:
                    loadOld = true
                }
                
                let formatter = NSDateFormatter()
                formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                
                var userCalendar = NSCalendar.currentCalendar()
                var releaseTime = userCalendar.dateByAddingUnit(
                    .Hour,
                    value: 1,
                    toDate: NSDate(),
                    options: [])!
                
                var infoForBuddyAsReceiver = PFQuery(className: "Activity")
                infoForBuddyAsReceiver.whereKey("user", equalTo: user)

                var infoForBuddyAsSender = PFQuery(className: "Activity")
                infoForBuddyAsSender.whereKey("receiver", equalTo: user)
                
                var groupsQuery = PFQuery(className: "Group")
                groupsQuery.whereKey("members", containsAllObjectsInArray: [user])
                
                var infoForUserInGroup = PFQuery(className: "Activity")
                infoForUserInGroup.whereKey("group", matchesQuery: groupsQuery)
                
                var query2 = PFQuery.orQueryWithSubqueries([infoForBuddyAsSender, infoForBuddyAsReceiver, infoForUserInGroup])
                
                var viewedQuery = PFQuery(className: "Viewed")
                viewedQuery.whereKey("user", equalTo: user)
                viewedQuery.whereKey("activity", matchesQuery: query2)
                viewedQuery.includeKey("activity")
                viewedQuery.includeKey("activity.group")
                viewedQuery.includeKey("activity.receiver")
                viewedQuery.includeKey("activity.user")
                viewedQuery.whereKey("dateUnlocked", lessThan: releaseTime)

                var limitOnQuery:Int = 100
                viewedQuery.limit = limitOnQuery
                viewedQuery.orderByAscending("dateUnlocked")
                
                
                if getNew == true  {
                    viewedQuery.orderByAscending("dateUnlocked,createdAt")
                    if let queryDate:NSDate = (self.defaults.objectForKey("LastQueryThread") as? NSDate) {
                        viewedQuery.whereKey("dateUnlocked", greaterThanOrEqualTo: queryDate)
                    }
                    
                    if let arrayAtLast:[String] = self.defaults.objectForKey("arrayEqualLastQuery") as? Array {
                        viewedQuery.skip = arrayAtLast.count
                    }
                    
                } else if firstTime == true {
                    viewedQuery.orderByDescending("dateUnlocked,createdAt")
                } else if loadOld == true {
                    viewedQuery.orderByDescending("dateUnlocked,createdAt")
                    let queryDate:NSDate = (self.defaults.objectForKey("firstFlashback") as? NSDate)!
                    viewedQuery.whereKey("dateUnlocked", lessThanOrEqualTo: queryDate)
                    
                    if let arrayAtFirst:[String] = self.defaults.objectForKey("arrayEqualFirstFlashback") as? Array {
                        viewedQuery.skip = arrayAtFirst.count
                    }
                    
                }

                var uniqueIds = prepPopulateThreads()
                var totalIncoming:Int = 0

                viewedQuery.findObjectsInBackgroundWithBlock {
                    (objects, error) -> Void in
                    if error == nil {
                        var specialMessage:String = ""
                        
                        for object in objects! {
                            
                            let activity:PFObject = (object["activity"] as? PFObject)!
                          
                            var buddy:PFUser?
                            var group:PFObject?
                            var dateUnlocked:NSDate! = activity["dateUnlocked"] as! NSDate
                            
                            
                            var uniqueId:String? //of buddy or group
                            var counterName:String? //name of buddy or group
                            var groupMembersPF:[PFUser]?
                            var members:[String]?
                            
                            var error: NSError?

                            
                            var viewed:Bool = object["viewed"] as! Bool
                            var viewedObject:PFObject = object 
                            var viewedId:String = viewedObject.objectId!

                            
                            
                            //not incoming - if defaults not assigned - assign them 
                            if uniqueIds.count <= totalIncoming {
                                let notIncoming = dateUnlocked.compare(NSDate())
                                if notIncoming == .OrderedAscending {
                                    if let lastQuery:NSDate = self.defaults.objectForKey("LastQueryThread") as? NSDate {
                                    
                                    } else {
                                        self.defaults.setObject(dateUnlocked, forKey: "firstFlashback")
                                        self.defaults.setObject([], forKey: "arrayEqualFirstFlashback")
                                        self.defaults.setObject(dateUnlocked, forKey: "LastQueryThread") // most recent query
                                        self.defaults.setObject([], forKey: "arrayEqualLastQuery")
                                    }
                                }else{
                                    totalIncoming++
                                }
                            }
                            
                            //function for if 1 to 1 or group
                            func addThread(isGroup: Bool, counterParty:PFObject){
                                if isGroup == false {
                                    buddy = counterParty as? PFUser
                                    uniqueId = buddy!.objectId
                                    counterName = buddy!.username
                                } else {
                                    group = activity["group"] as? PFObject
                                    uniqueId = group!.objectId
                                    counterName = (group!["groupName"] as? String)!
                                    groupMembersPF = (group!["members"] as AnyObject? as! [PFUser])
                                    members = [String]()
                                    for member in groupMembersPF! {
                                        members!.append(member.username!)
                                    }
                                }
                                
                                //if doesn't exist then create object
                                if !uniqueIds.contains((uniqueId!)) {
                                    uniqueIds.append(uniqueId!)
                                    let entity1 =  NSEntityDescription.entityForName("Threads",
                                        inManagedObjectContext:
                                        self.managedContext!)
                                    let threadsObject1 = NSManagedObject(entity: entity1!,
                                        insertIntoManagedObjectContext:self.managedContext!) as! Threads
                                    
                                    threadsObject1.uniqueId = uniqueId!
                                    threadsObject1.name = counterName!
                                    threadsObject1.dateUnlocked = dateUnlocked
                                    threadsObject1.flashbacks = 0
                                    threadsObject1.incoming = 0
                                    if isGroup == false{
                                        threadsObject1.group = false
                                    } else {
                                        threadsObject1.group = true
                                        threadsObject1.groupMembers = members!
                                        
                                    }
                                    
                                    do {
                                        try self.managedContext!.save()
                                    } catch let error1 as NSError {
                                        error = error1
                                        print("Could not save \(error), \(error?.userInfo)")
                                    }
                                }
                                
                                //find object (either just created or not)
                                let predicate = NSPredicate(format: "uniqueId == %@", uniqueId!)
                                let fetchRequest = NSFetchRequest(entityName: "Threads")
                                fetchRequest.predicate = predicate
                                let fetchedEntities = (try! self.managedContext!.executeFetchRequest(fetchRequest)) as! [Threads]
                                
                                //for sorting purposes
                                let threadUnlocked:NSDate = fetchedEntities.first!.dateUnlocked
                                let unlocked = dateUnlocked.compare(threadUnlocked)
                                if unlocked == .OrderedDescending {
                                    fetchedEntities.first?.dateUnlocked = dateUnlocked
                                }
                                
                                if viewed == false {
                                    let unlocked = dateUnlocked.compare(NSDate())
                                    if unlocked == .OrderedAscending {
                                            let tempNumber:Int = Int(fetchedEntities.first!.flashbacks)
                                            fetchedEntities.first!.flashbacks = tempNumber + 1
                                    } else {
                                        let unlocked2 = dateUnlocked.compare(releaseTime)
                                        if unlocked2 == .OrderedAscending {
                                            let tempNumber:Int = Int(fetchedEntities.first!.incoming)
                                            fetchedEntities.first!.incoming = tempNumber + 1
                                        }
                                    }
                                }
                                do {
                                    try self.managedContext!.save()
                                } catch let error1 as NSError {
                                    error = error1
                                    print("Could not save \(error), \(error?.userInfo)")
                                }
                            }
                            
                            //run addThread function
                            if activity["group"] === nil {
                                if activity["user"] as! PFUser == user {
                                    if let tempBuddy = activity["receiver"] as? PFUser {
                                        addThread(false, counterParty: tempBuddy)
                                    } else {
                                        addThread(false, counterParty: user)
                                    }
                                } else {
                                    let tempBuddy = activity["user"] as? PFUser
                                    addThread(false, counterParty: tempBuddy!)
                                }
                            }else if activity["group"] != nil{
                                let tempGroup = activity["group"] as? PFObject
                                addThread(true, counterParty: tempGroup!)
                            }
                            
                            let notIncoming = dateUnlocked.compare(NSDate())
                            if notIncoming == .OrderedAscending {
                                if let lastQuery:NSDate = self.defaults.objectForKey("LastQueryThread") as? NSDate {
                                    let unlocked = dateUnlocked.compare(lastQuery)
                                    
                                    if unlocked == .OrderedDescending {
                                        self.defaults.setObject(dateUnlocked, forKey: "LastQueryThread")
                                        self.defaults.setObject([viewedId], forKey: "arrayEqualLastQuery")
                                        
                                    }else if lastQuery == dateUnlocked {
                                        var arrayAtLastQuery:[String] = self.defaults.objectForKey("arrayEqualLastQuery") as! Array
                                        arrayAtLastQuery.append(viewedId)
                                        self.defaults.setObject(arrayAtLastQuery, forKey: "arrayEqualLastQuery")
                                    }
                                }
                                if let firstFlashback:NSDate = self.defaults.objectForKey("firstFlashback") as? NSDate {
                                    let unlockedFirst = dateUnlocked.compare(firstFlashback)
                                    if unlockedFirst == .OrderedAscending {
                                        self.defaults.setObject(dateUnlocked, forKey: "firstFlashback")
                                        self.defaults.setObject([viewedId], forKey: "arrayEqualFirstFlashback")
                                    }else if firstFlashback == dateUnlocked {
                                        
                                        var arrayAtFirstQuery:[String] = self.defaults.objectForKey("arrayEqualFirstFlashback") as! Array
                                        if !arrayAtFirstQuery.contains(viewedId) {
                                            arrayAtFirstQuery.append(viewedId)
                                            self.defaults.setObject(arrayAtFirstQuery, forKey: "arrayEqualFirstFlashback") // number to skip next query
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                        if loadOld == true || firstTime == true {
                            if objects!.count < limitOnQuery {
                                self.defaults.setObject(false, forKey: "moreFlashbacks")
                                self.showOlderButton.hidden = true
                                specialMessage = " - the moment you first flashedbacked..."
                            } else {
                                self.defaults.setObject(true, forKey: "moreFlashbacks")
                                self.showOlderButton.hidden = false
                            }
                            
                            if let tempFirst:NSDate = self.defaults.objectForKey("firstFlashback") as? NSDate {
                                let showingText = "fb's since " + formatter.stringFromDate(tempFirst) + specialMessage
                                self.showingLabel.text = showingText
                            }
                        }
                        
                        if loadOld == true {
                            if self.loadMoreActivity.isAnimating(){
                                self.loadMoreActivity.stopAnimating()
                            }
                            self.loadMoreActivity.hidden = true
                        }
                        
                        if getNew == true {
                            self.moreFlashbacksSignedIn()
                        }
                        
                        self.fetchThreads(true)
                        

                    }
                    else {
                        
                        
                        if self.refresher.refreshing {
                            self.refresher.endRefreshing()
                        }
                        self.connectionError.text = "no internet connection"
                        self.connectionErrorHeight.constant = 20
                        self.delay(3){
                            UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                { () -> Void in
                                    
                                    self.connectionErrorHeight.constant = 0
                                    self.view.layoutIfNeeded()
                                    
                                }, completion: {finished in
                                    
                            })
                        }
                    }
                }
            }
        
        } else {
            self.connectionErrorHeight.constant = 20
            self.refresher.endRefreshing()
            self.delay(3){
                if !self.refresher.refreshing {
                    UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.connectionErrorHeight.constant = 0
                            self.view.layoutIfNeeded()
                        
                        }, completion: {finished in

                    })
                    
                    
                
                }
            }
        }
    
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func fetchThreads(populate:Bool){
        
        let fetchRequest = NSFetchRequest(entityName:"Threads")
        let sortDescriptor = NSSortDescriptor(key: "dateUnlocked", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResults = (try! managedContext!.executeFetchRequest(fetchRequest)) as? [Threads]
        self.threads = OrderedDictionary()
        if let results = fetchedResults {
            for result in results{
                self.threads[result.name] = infoStruct(numberOfIncoming: Int(result.incoming), numberOfFlashbacks: Int(result.flashbacks), dateUnlocked: result.dateUnlocked, group: result.group, groupMembers: result.groupMembers, uniqueId: result.uniqueId)
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()

            }
            if self.refresher.refreshing {
                self.refresher.endRefreshing()
            }
        } else {
            
        }
    }
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return threads.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:ThreadsCell = tableView.dequeueReusableCellWithIdentifier("cell") as! ThreadsCell
        
        cell.counterFbs.textColor = UIColor.darkGrayColor()
        cell.counterIncoming.textColor = UIColor.lightGrayColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None

        cell.groupBuddies.numberOfLines = 0
        cell.groupBuddies.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
        //buddy or group name
        cell.buddy!.text = self.threads[indexPath.row].0
        
        //names of group members
        if self.threads[
            indexPath.row].1.group == false {
            cell.groupBuddies.text = ""
        } else {
            let memberNames:[String] = threads[indexPath.row].1.groupMembers as AnyObject? as! [String]
            let multipleLineString = memberNames.joinWithSeparator(", ")
            cell.groupBuddies.text = multipleLineString
        }
        
        cell.counterWidth.constant = 0
        
        //flashbacks
        if self.threads[indexPath.row].1.numberOfFlashbacks > 0 {
            cell.counterFbs!.text = "flashbacks: " + String(stringInterpolationSegment: self.threads[indexPath.row].1.numberOfFlashbacks!)
            cell.counterFbs.hidden = false
            cell.counterWidth.constant = 113
            
        } else {
            cell.counterFbs.hidden = true
        }
        
        //incoming
        if self.threads[indexPath.row].1.numberOfIncoming > 0 {
            cell.counterIncoming!.text = "incoming: " + String(stringInterpolationSegment: self.threads[indexPath.row].1.numberOfIncoming!)
            cell.counterIncoming.hidden = false
            cell.counterWidth.constant = 113

        } else {
            cell.counterIncoming.hidden = true
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier("toThread", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toThread") {
            let indexPath:Int = self.tableView.indexPathForSelectedRow!.row
            let userToSend = segue.destinationViewController as! FeedViewController;
            userToSend.delegateThreads = self
            userToSend.name = threads[indexPath].0
            userToSend.uniqueId = threads[indexPath].1.uniqueId
            if self.threads[indexPath].1.group == false {
                userToSend.groupBool = false
            } else {
                userToSend.groupBool = true
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}
  