//  Created by Nick Scott

import UIKit
import AVFoundation



protocol UpdateThreads {
    
    func fetchThreads(populate:Bool)
    
}


import CoreData


class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet var viewedActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var navHeader: UINavigationItem!
    @IBOutlet var noMoreRight: UILabel!
    @IBOutlet var noMoreLeft: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var mainPic: UIImageView!
    @IBOutlet var informationLabel: UILabel!
    @IBOutlet var play: UIButton!
    @IBOutlet var muteItem: UIButton!
    @IBOutlet var loading: UILabel!
    @IBOutlet var tryAgain: UIButton!
    
    @IBOutlet var picDescriptionView: UIView!
    @IBOutlet var picDescription: UILabel!
    
    @IBOutlet var connectionErrorView: UIView!
    @IBOutlet var connectionErrorHeight: NSLayoutConstraint!
    

    var user:PFUser = PFUser.currentUser()!
    var name:String!
    var groupBool:Bool!
    var uniqueId:String!
    

    enum Status {
        case tapToLoad
        case loading
        case blank
        case tryAgain
        case pressToDream
    }
    
    class Flashback {
        
        var status:Status?
        var time:NSTimer?
        var dreamTime:Double?
        var loaded:Bool?
    }
    
    var FlashbackList = OrderedDictionary<PFObject, Flashback>()
    var CurrentList = OrderedDictionary<PFObject, Flashback>()

    var imageCounter:Int?
    var refresher = UIRefreshControl()
    var tableEmpty:Bool = true
    var frameOfPic:CGRect?
    
    var player : AVPlayer? = nil
    var playerLayer : AVPlayerLayer? = nil
    var asset : AVAsset? = nil
    var playerItem: AVPlayerItem? = nil
    
    var playerDream : AVPlayer? = nil
    var playerLayerDream : AVPlayerLayer? = nil
    var assetDream : AVAsset? = nil
    var playerItemDream: AVPlayerItem? = nil
    var lengthOfVidDream:CMTime?
    
    //for dream
    var currentDream:PFObject?
    var mainView:UIView?
    var imageView:UIImageView?
    var counterLabel:UILabel?
    var dreamNameLabel:UILabel?
    var dreamPicTitle:UILabel?
    
    var userCalendar = NSCalendar.currentCalendar()
    var releaseTime:NSDate?
    
    var delegateThreads:UpdateThreads?
    
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var managedContext:NSManagedObjectContext?
    
    var refreshError = false
    
    let defaults = NSUserDefaults.standardUserDefaults()

    
    let audioOffImage = UIImage(named: "audioOff")
    let audioOnImage = UIImage(named: "audioOn")
    
    @IBAction func tryAgainButton(sender: AnyObject) {
        self.tryAgain.hidden = true
        self.informationLabel.hidden = true
        populateFlashbacksAndViewedWithRefresh(false)
        
    }
    
    @IBAction func play(sender: AnyObject) {
        
        if muted == true{
            player?.muted = true
        } else {
            player?.muted = false
        }
        
        player?.play()
        player?.seekToTime(kCMTimeZero)
    }

    @IBAction func seeAll(sender: AnyObject) {
        
        self.performSegueWithIdentifier("seeAll", sender: self)
    }
    
    @IBAction func mute(sender: AnyObject) {
        if muted == true {
            muted = false
            
            muteItem.setImage(audioOnImage, forState: UIControlState.Normal)
            
            if let player = player {
                player.muted = false
            }
        } else {
            muted = true
            muteItem.setImage(audioOffImage, forState: UIControlState.Normal)

            if let player = player {
                player.muted = true
            }
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("muteNotification", object: self)
    }
    
    
    func muteNotification(notification:NSNotification){
        
        if notification.object as? UIViewController != self {
        
            dispatch_async(dispatch_get_main_queue()) {

                if muted == false {
                    self.muteItem.setImage(self.audioOnImage, forState: UIControlState.Normal)
                    
                    if let player = self.player {
                        player.muted = false
                    }
                } else {
                    self.muteItem.setImage(self.audioOffImage, forState: UIControlState.Normal)
                    if let player = self.player {
                        player.muted = true
                    }
                }
                
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
    
        
        center.addObserver(self, selector: "muteNotification:", name:"muteNotification", object: nil)


        self.tableEmpty = true

        viewedActivityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3)

        self.managedContext = self.appDelegate.managedObjectContext!
        self.navigationController!.interactivePopGestureRecognizer!.delegate = self

        
        setupPopUp() //for dream
        configureTableView()


        self.releaseTime = userCalendar.dateByAddingUnit(
            .Hour,
            value: 1,
            toDate: NSDate(),
            options: [])!
        
        self.imageCounter = 0

        
        play.layer.cornerRadius = 0.5 * play.frame.width
        play.layer.borderColor = UIColor.highlightColor().CGColor
        play.layer.borderWidth = 2
        play.layer.backgroundColor = UIColor.lightGrayColor().CGColor
        play.alpha = 0.6
        play.addTarget(self, action: "lowerAlpha:", forControlEvents: UIControlEvents.TouchDown)
        play.addTarget(self, action: "raiseAlpha:", forControlEvents: UIControlEvents.TouchCancel)
        
        
        muteItem.layer.cornerRadius = 0.5 * muteItem.frame.width
        muteItem.layer.backgroundColor = UIColor.lightGrayColor().CGColor
        muteItem.alpha = 0.6
        
        self.frameOfPic = self.mainPic.frame
 
        let newTitle:UILabel = UILabel()
        newTitle.frame = CGRectMake(0, 0, 0, 0)
        newTitle.font = UIFont(name: "AvenirNext-DemiBold", size: 30)
        newTitle.textAlignment = .Center
        newTitle.baselineAdjustment = .AlignCenters
        newTitle.textColor = UIColor.whiteColor()
        newTitle.text = self.name
        newTitle.minimumScaleFactor = 0.5
        newTitle.adjustsFontSizeToFitWidth = true
        newTitle.sizeToFit()
        
        self.navHeader.titleView = newTitle
        
        self.mainPic.userInteractionEnabled = true
        self.mainPic.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.mainPic.layer.borderWidth = 2.0
        self.view.addSubview(self.mainPic)
        self.view.addSubview(self.tableView)
        
        let gesture = UIPanGestureRecognizer(target: self, action: Selector("wasDragged:"))
        self.mainPic.addGestureRecognizer(gesture)
        populateFlashbacksAndViewedWithRefresh(false)
        
        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        refresher.transform = CGAffineTransformMakeScale(0.75, 0.75)

        refresher.tintColor = UIColor.highlightColor()
        self.tableView.addSubview(refresher)

    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController!.navigationBar.hidden = false
        self.connectionErrorView.hidden = true
        if muted == true {
            muteItem.setImage(audioOffImage, forState: UIControlState.Normal)
        } else {
            muteItem.setImage(audioOnImage, forState: UIControlState.Normal)
        }
        
    }
    
    func writeVidTemp (data: NSData, dream:Bool) -> NSURL? {
        
        let outputPath:String?
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        if dream == true{
            outputPath = "\(documentsPath)/playVidDream.mov"
        }else {
            outputPath = "\(documentsPath)/playVid.mov"
        }
        
        let outputFileUrl = NSURL(fileURLWithPath: outputPath!)
        
        let error2 = NSErrorPointer()
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(outputPath!) {
            do {
                try fileManager.removeItemAtPath(outputPath!)
            } catch let error as NSError {
                error2.memory = error
            }
        }
        
        let test = data.writeToFile(outputPath!, atomically: false)
        if dream == false {
            if test {
                playVideo(outputFileUrl, dream: false)
            }
            return nil
        } else {
            return outputFileUrl
        }
    }
    
    func playVideo(vidURL: NSURL, dream: Bool) {
        
        let videoURL = vidURL
        
        if dream == false {
            asset = AVAsset(URL: videoURL)
            playerItem = AVPlayerItem(asset: asset!)
            player = AVPlayer(playerItem: self.playerItem!)
            
            if let player = player {
                playerLayer = AVPlayerLayer(player: self.player)
                playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                playerLayer!.frame = CGRectMake(0, 0, mainPic.frame.width, mainPic.frame.height)
                self.mainPic.layer.addSublayer(playerLayer!)
                self.play.hidden = false
                self.muteItem.hidden = false
                self.mainPic.addSubview(self.play)
                self.mainPic.addSubview(self.muteItem)
                
                player.actionAtItemEnd = .None
            }
        } else {
            assetDream = AVAsset(URL: videoURL)
            playerItemDream = AVPlayerItem(asset: assetDream!)
            playerDream = AVPlayer(playerItem: self.playerItemDream!)
            
            if let player = playerDream {
                playerLayerDream = AVPlayerLayer(player: self.playerDream)
                playerLayerDream!.frame = CGRectMake(0, 0, self.imageView!.frame.width, self.imageView!.frame.height)
                self.imageView!.image = nil
                self.imageView!.layer.addSublayer(playerLayerDream!)
                player.actionAtItemEnd = .None
            }
        }
    }
    
    
    func lowerAlpha(sender:UIButton) {
        sender.alpha = 0.6
    }
    
    func raiseAlpha(sender:UIButton) {
        sender.alpha = 0.3
    }
    
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func loadPhoto(row: Int, completionHandler: (error:Bool, row:Int) -> ()){
        
        let viewedObject:PFObject = self.FlashbackList[row].0
        let mediaQuery = PFQuery(className: "Viewed")
        mediaQuery.includeKey("activity.picVideo")
        mediaQuery.getObjectInBackgroundWithId((viewedObject.objectId)!){
            (object, error) -> Void in
            
            let updatedRow:Int = self.FlashbackList.returnIndex(viewedObject)!
            
            
            if error == nil {
                let activity:PFObject = object!["activity"] as! PFObject
                let picVideo:PFObject = activity["picVideo"] as! PFObject
                
                viewedObject["viewed"] = true
                viewedObject.saveEventually()
                self.subtractFlashback((activity["dateUnlocked"] as? NSDate)!, viewedId: viewedObject.objectId!)
                self.CurrentList.insert(Flashback(), forKey: object!, atIndex: 0)
                
                let mediaFile:PFFile = picVideo["mediaFile"] as! PFFile
                mediaFile.getDataInBackgroundWithBlock{
                    (mediaData, error) -> Void in
                    
                    if error == nil {
                        self.informationLabel.hidden = true
                        self.imageCounter = 0
                        if activity["video"] as? Bool == true {
                            self.writeVidTemp(mediaData!, dream: false)
                            self.play.hidden = false
                            self.muteItem.hidden = false
                            self.mainPic.addSubview(self.play)
                            self.mainPic.addSubview(self.muteItem)
                        } else {
                            self.playerLayer?.removeFromSuperlayer()
                            self.player?.pause()
                            let image = UIImage(data: mediaData!)
                            let previewImage = ImageUtilCrop.cropToSquare(image: image!)
                            self.mainPic.image = previewImage
                            self.play.hidden = true
                            self.muteItem.hidden = true
                        }
                        self.mainPic.addSubview(self.picDescriptionView)
                        let takenByUser:PFUser = (activity["user"] as? PFUser)!
                        let takenBy:String = takenByUser.username!
                        let formatter = NSDateFormatter()
                        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                        formatter.timeStyle = NSDateFormatterStyle.NoStyle
                        let dateTaken = formatter.stringFromDate(activity.createdAt!)
                        let description:String = takenBy + " -- " + dateTaken
                        self.picDescription.text = description
                        
                        
                        self.FlashbackList.removeEntryAtIndex(updatedRow)
                        if self.FlashbackList.count == 0 {
                            self.tableEmpty = true
                            
                        } else {
                            self.tableEmpty = false
                            
                        }
                        
                        let tap = UITapGestureRecognizer(target: self, action: Selector("tappedPic"))
                        self.mainPic.addGestureRecognizer(tap)
                        self.mainPic.userInteractionEnabled = true
                        
                        completionHandler(error:false, row: updatedRow)
                    }else {
                        completionHandler(error:true, row: updatedRow)
                        
                    }
                }
            }else{
                print(error)
                completionHandler(error:true, row: updatedRow)
            }
            
        }
    }
    
    func populateFlashbacksAndViewedWithRefresh(refresh:Bool){
        
        if refresh == false {
            self.viewedActivityIndicator.hidden = false
            self.loading.hidden = false
            
            self.viewedActivityIndicator.startAnimating()
            self.tableView.hidden = true
        }
        
        if Reachability.isConnectedToNetwork(){


            var activityQuery = PFQuery()
            if groupBool == false {
                
                let usersQuery = PFUser.query()
                usersQuery!.whereKey("objectId", equalTo: uniqueId)
                

                let activityQuery1 = PFQuery(className: "Activity")
                activityQuery1.whereKey("receiver", matchesQuery: usersQuery!)
                activityQuery1.whereKey("user", equalTo: user)
                activityQuery1.whereKey("memory", equalTo: true)

                
                let activityQuery2 = PFQuery(className: "Activity")
                activityQuery2.whereKey("receiver", equalTo: user)
                activityQuery2.whereKey("user", matchesQuery: usersQuery!)
                activityQuery2.whereKey("memory", equalTo: true)

                if user.objectId == self.uniqueId {
                    // for ones where user is sending to themselves
                    let activityQuery3 = PFQuery(className: "Activity")
                    activityQuery3.whereKey("user", equalTo: user)
                    activityQuery3.whereKeyDoesNotExist("group")
                    activityQuery3.whereKeyDoesNotExist("receiver")
                    activityQuery3.whereKey("memory", equalTo: true)

                    activityQuery = PFQuery.orQueryWithSubqueries([activityQuery1, activityQuery2, activityQuery3])
                } else {
                    activityQuery = PFQuery.orQueryWithSubqueries([activityQuery1, activityQuery2])
                }
            
            } else {
                
                let groupQuery = PFQuery(className: "Group")
                groupQuery.whereKey("objectId", equalTo: uniqueId)

                let activityQuery3 = PFQuery(className: "Activity")
                activityQuery3.whereKey("group", matchesQuery: groupQuery)
                activityQuery3.whereKey("memory", equalTo: true)

                
                activityQuery = activityQuery3
                
            }
            var oneIsFinished:Bool = false
            
            if refresh == false {
                let viewedQuery = PFQuery(className: "Viewed")
                viewedQuery.whereKey("user", equalTo: user)
                viewedQuery.whereKey("viewed", equalTo: true)
                viewedQuery.whereKey("activity", matchesQuery: activityQuery)
                viewedQuery.includeKey("activity")
                viewedQuery.includeKey("activity.user")
                viewedQuery.includeKey("activity.picVideo")
                viewedQuery.orderByDescending("dateUnlocked")
                viewedQuery.whereKey("dateUnlocked", lessThan: self.releaseTime!)

                viewedQuery.limit = 10

                viewedQuery.findObjectsInBackgroundWithBlock {
                    (objects, error) -> Void in
                    
                    
                    if error == nil {
                        for object in objects! {
                            let viewedObject:PFObject = (object)
                            self.CurrentList[viewedObject] = Flashback()
                        }
                        if self.CurrentList.count > 0 {
                            
                            self.informationLabel.hidden = true
                            let viewedObject = self.CurrentList[self.imageCounter!].0
                            let activity:PFObject = (viewedObject["activity"] as? PFObject)!
                            let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
                            
                            let data:PFFile = (picVideo["mediaFile"] as? PFFile)!
                            data.getDataInBackgroundWithBlock{
                                (data, error) -> Void in
                                if error == nil {
                                    if activity["video"] as? Bool == true {
                                        self.writeVidTemp(data!, dream:false)
                                        self.play.hidden = false
                                        self.muteItem.hidden = false
                                        self.mainPic.addSubview(self.play)
                                        self.mainPic.addSubview(self.muteItem)
                                        let tap = UITapGestureRecognizer(target: self, action: Selector("tappedPic"))
                                        self.mainPic.addGestureRecognizer(tap)
                                        self.mainPic.userInteractionEnabled = true
                                    }else {
                                        let image = UIImage(data: data!)
                                        let previewImage = ImageUtilCrop.cropToSquare(image: image!)
                                        self.mainPic.image = previewImage
                                        let tap = UITapGestureRecognizer(target: self, action: Selector("tappedPic"))
                                        self.mainPic.addGestureRecognizer(tap)
                                        self.mainPic.userInteractionEnabled = true
                                        self.play.hidden = true
                                        self.muteItem.hidden = true
                                        
                                    }

                                    self.mainPic.addSubview(self.picDescriptionView)
                                    let takenByUser:PFUser = (activity["user"] as? PFUser)!
                                    let takenBy:String = takenByUser.username!
                                    let formatter = NSDateFormatter()
                                    formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                                    formatter.timeStyle = NSDateFormatterStyle.NoStyle
                                    let dateTaken = formatter.stringFromDate(activity.createdAt!)
                                    let description:String = takenBy + " -- " + dateTaken
                                    self.picDescription.text = description
                                    
                                }
                            }
                            
                        }else {
                            self.informationLabel.hidden = false

                        }
                    } else {
                        if oneIsFinished == true {
                            self.informationLabel.hidden = false
                            self.informationLabel.text = "Sorry! You don't seem to have an internet connection...Blame your cell phone carrier or your wifi provider (anyone but us)!"
                            self.tryAgain.hidden = false
                            self.view.bringSubviewToFront(self.tryAgain)
                            self.loading.hidden = true

                        }

                    }
                    if oneIsFinished == true {
                        self.viewedActivityIndicator.hidden = true
                        self.loading.hidden = true
                    }
                    oneIsFinished = true

                }
            }
            
            
            var activityQueryFeed = PFQuery()
            if groupBool == false {
                
                let usersQuery = PFUser.query()
                usersQuery!.whereKey("objectId", equalTo: uniqueId)
                
                
                let activityQuery1 = PFQuery(className: "Activity")
                activityQuery1.whereKey("receiver", matchesQuery: usersQuery!)
                activityQuery1.whereKey("user", equalTo: user)
                
                
                let activityQuery2 = PFQuery(className: "Activity")
                activityQuery2.whereKey("receiver", equalTo: user)
                activityQuery2.whereKey("user", matchesQuery: usersQuery!)
                
                if user.objectId == self.uniqueId {
                    // for ones where user is sending to themselves
                    let activityQuery3 = PFQuery(className: "Activity")
                    activityQuery3.whereKey("user", equalTo: user)
                    activityQuery3.whereKeyDoesNotExist("group")
                    activityQuery3.whereKeyDoesNotExist("receiver")
                    
                    activityQueryFeed = PFQuery.orQueryWithSubqueries([activityQuery1, activityQuery2, activityQuery3])
                } else {
                    activityQueryFeed = PFQuery.orQueryWithSubqueries([activityQuery1, activityQuery2])
                }
                
            } else {
                
                let groupQuery = PFQuery(className: "Group")
                groupQuery.whereKey("objectId", equalTo: uniqueId)
                
                let activityQuery3 = PFQuery(className: "Activity")
                activityQuery3.whereKey("group", matchesQuery: groupQuery)
                
                
                activityQueryFeed = activityQuery3
                
            }
            
            let flashbackQuery = PFQuery(className: "Viewed")
            flashbackQuery.whereKey("user", equalTo: user)
            flashbackQuery.whereKey("viewed", equalTo: false)
            flashbackQuery.whereKey("activity", matchesQuery: activityQueryFeed)
            flashbackQuery.includeKey("activity")
            flashbackQuery.includeKey("activity.user")
            flashbackQuery.orderByDescending("dateUnlocked")
            flashbackQuery.whereKey("dateUnlocked", lessThan: self.releaseTime!)

            flashbackQuery.limit = 100
            
            flashbackQuery.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                

                if error == nil {
                    self.tableView.hidden = false
                    self.loading.hidden = true
                    for object in objects! {
                        let viewedObject:PFObject = (object)
                        let tempFlashback = Flashback()
                        let activity:PFObject = (object["activity"] as? PFObject)!
                        if activity["memory"] as? Bool == false {
                            tempFlashback.status = Status.tapToLoad
                        } else {
                            tempFlashback.status = Status.blank
                        }
                        self.FlashbackList[viewedObject] = tempFlashback
                    }
                    
                    if refresh == true {
                        self.refresher.endRefreshing()
                        self.refreshError = false
                    }
                    
                } else {
                    if oneIsFinished == true {
                        if self.mainPic.image == nil {
                            self.informationLabel.hidden = false
                            self.informationLabel.text = "Oops something went wrong!"
                            self.tryAgain.hidden = false
                            self.view.bringSubviewToFront(self.tryAgain)
                            self.loading.hidden = true
                        }
                    }
                    if refresh == true {
                        self.refresher.endRefreshing()
                        self.refreshError = true
                        self.connectionErrorHeight.constant = 20
                        self.connectionErrorView.hidden = false
                        self.delay(3){
                            UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                { () -> Void in
                                    
                                    self.connectionErrorHeight.constant = 0
                                    self.view.layoutIfNeeded()
                                    
                                }, completion: {finished in
                                    
                            })
                        }


                    }
                }
                
                
                if self.FlashbackList.count == 0 {
                    self.tableEmpty = true
                }else {
                    self.tableEmpty = false
                }
                
                if oneIsFinished == true && self.viewedActivityIndicator.isAnimating() {
                    self.viewedActivityIndicator.hidden = true
                    self.loading.hidden = true

                    self.viewedActivityIndicator.stopAnimating()
                }
                oneIsFinished = true
                
                self.tableView.reloadData()

            }
        } else {
            if refresh == false {
                delay(0.5){
                    if self.mainPic.image == nil {
                        self.informationLabel.hidden = false
                        self.informationLabel.text = "Sorry! You don't seem to have an internet connection...Blame your cell phone carrier or your wifi provider (anyone but us)!"
                        self.tryAgain.hidden = false
                        self.view.bringSubviewToFront(self.tryAgain)
                        self.viewedActivityIndicator.hidden = true
                        self.loading.hidden = true
                        self.tableView.hidden = true

                    }
                }
            } else if refresh == true {
                self.refresher.endRefreshing()
                self.refreshError = true
                self.connectionErrorHeight.constant = 20
                self.connectionErrorView.hidden = false
                self.delay(3){
                    UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                        { () -> Void in
                            
                            self.connectionErrorHeight.constant = 0
                            self.view.layoutIfNeeded()
                            
                        }, completion: {finished in
                            
                    })
                }
            }
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }

    
    func tappedPic()
    {
        performSegueWithIdentifier("pictureWithComments", sender: self)
    }
    
    
    
    
    func refresh() {
        
        populateFlashbacksAndViewedWithRefresh(true)
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as? FeedCell
        if cell?.indicator.isAnimating() == false {

            
            if self.FlashbackList.count != 0 {
                let viewedObject = self.FlashbackList[indexPath.row].0
                let activity:PFObject = (viewedObject["activity"] as? PFObject)!
                let flashback = self.FlashbackList[indexPath.row].1
                
                let dateUnlocked:NSDate =  (activity["dateUnlocked"] as? NSDate)!
                
                let unlocked = dateUnlocked.compare(NSDate())
                if unlocked == .OrderedAscending {

                    if activity["memory"] as? Bool == true {
                        flashback.status = Status.loading
                        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                        
                        loadPhoto(indexPath.row){
                            (error, row) in
                            let indexPathUpdated:NSIndexPath = NSIndexPath(forRow: row, inSection: 0)


                            if error == false {
                                flashback.status = Status.blank
                                if self.FlashbackList.count > 0 {
                                    self.tableView.deleteRowsAtIndexPaths([indexPathUpdated], withRowAnimation: UITableViewRowAnimation.None)
                                } else {
                                    self.tableView.reloadData()
                                }
                            } else {
                                flashback.status = Status.tryAgain
                                self.tableView.reloadRowsAtIndexPaths([indexPathUpdated], withRowAnimation: UITableViewRowAnimation.None)
                            }
                        }
                    } else {
                        flashback.status = Status.loading
                        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                        
                        prepDream(indexPath.row){
                            (error, row) in
                            
                            let indexPathUpdated:NSIndexPath = NSIndexPath(forRow: row, inSection: 0)

                            
                            if error == false {

                                flashback.status = Status.pressToDream
                                
                                if self.FlashbackList.count > 0 {
                                    self.tableView.reloadRowsAtIndexPaths([indexPathUpdated], withRowAnimation: UITableViewRowAnimation.None)

                                } else {
                                    self.tableView.reloadData()
                                }
                            } else {
                                flashback.status = Status.tryAgain
                                self.tableView.reloadRowsAtIndexPaths([indexPathUpdated], withRowAnimation: UITableViewRowAnimation.None)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func prepDream(row: Int, completionHandler: (error:Bool, row:Int) -> ()){
        
        let viewedObject:PFObject = self.FlashbackList[row].0
        let mediaQuery = PFQuery(className: "Viewed")
        mediaQuery.includeKey("activity.picVideo")
        mediaQuery.getObjectInBackgroundWithId((viewedObject.objectId)!){
            (object, error) -> Void in
            
            let updatedRow = self.FlashbackList.returnIndex(viewedObject)!

            
            if error == nil {
                let flashback = self.FlashbackList[updatedRow].1
                self.FlashbackList.removeEntryAtIndex(updatedRow)
                flashback.loaded = true
                self.FlashbackList.insert(flashback, forKey: object!, atIndex: updatedRow)
                completionHandler(error:false, row: updatedRow)
                
            } else {
                completionHandler(error:true, row: updatedRow)
                
            }
        }
    }
    
    func subtractFlashback(dateUnlocked:NSDate, viewedId:String){
        
        func fetchAndSubtract() {
            
            let predicate = NSPredicate(format: "uniqueId == %@", self.uniqueId!)
            
            let fetchRequest = NSFetchRequest(entityName: "Threads")
            fetchRequest.predicate = predicate
            
            let fetchedEntities = (try! self.managedContext!.executeFetchRequest(fetchRequest)) as! [Threads]
            
            let threadNumberOfFlashback:Int = Int(fetchedEntities.first!.flashbacks)
            
            fetchedEntities.first?.flashbacks = threadNumberOfFlashback - 1
            
        }
        
        if let queryDate:NSDate = self.defaults.objectForKey("LastQueryThread") as? NSDate {
            if let firstFlashback:NSDate = self.defaults.objectForKey("firstFlashback") as? NSDate{
                
                let isBefore = queryDate.compare(dateUnlocked)
                let isAfter = firstFlashback.compare(dateUnlocked)
                if (isBefore == NSComparisonResult.OrderedDescending) && (isAfter == .OrderedAscending) {
                    
                    fetchAndSubtract()
            
                }else if isBefore == .OrderedSame {
                    

                    let arrayAtLastQuery:[String] = self.defaults.objectForKey("arrayEqualLastQuery") as! Array
                    if arrayAtLastQuery.contains(viewedId) {
                        
                        fetchAndSubtract()
                    }
                    
                } else if isAfter == .OrderedSame {

                    let arrayAtFirstQuery:[String] = self.defaults.objectForKey("arrayEqualFirstFlashback") as! Array
                    
                    if arrayAtFirstQuery.contains(viewedId) {
                        
                        fetchAndSubtract()

                    }
                    
                }
            }
        }
    }
    
    
    func updateViewed(){
        
        if let delegateThreads = self.delegateThreads {
            delegateThreads.fetchThreads(false)
        }

    }

    func held (gestureRecognizer:UILongPressGestureRecognizer){
        
        if gestureRecognizer.state == UIGestureRecognizerState.Began {
            
            
            let point = gestureRecognizer.locationInView(self.tableView)
            let indexPath = self.tableView.indexPathForRowAtPoint(point)
            let row = indexPath?.row
            
            let viewedObject:PFObject = self.FlashbackList[row!].0
            let activity:PFObject = (viewedObject["activity"] as? PFObject)!
            let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
            
            let picTitle:String = (activity["title"] as? String)!
            
            let mediaData:PFFile = (picVideo["mediaFile"] as? PFFile)!
            mediaData.getDataInBackgroundWithBlock{
                (data, error) -> Void in
                
                
                let updatedRow = self.FlashbackList.returnIndex(viewedObject)!

                if error == nil {
                    
                    viewedObject["viewed"] = true
                    viewedObject.saveEventually()
                    self.subtractFlashback((activity["dateUnlocked"] as? NSDate)!, viewedId: viewedObject.objectId!)

                    
                    let tempClass:Flashback = self.FlashbackList[updatedRow].1
                    
                    
                    let user:PFUser = (activity["user"] as? PFUser)!
                    let name:String = user.username!
                    self.dreamNameLabel!.text = name
                    
                    self.dreamPicTitle!.text = picTitle
                    
                    if (activity["video"] as? Bool)! == false{
                        if tempClass.dreamTime == nil {
                            tempClass.dreamTime = 5
                        }
                        
                        let image = UIImage(data: data!)
                        self.imageView!.image = image
                        if tempClass.dreamTime > 0 {
                            self.playerLayerDream?.removeFromSuperlayer()
                            self.playerDream?.pause()
                            self.counterLabel!.text = String(Int(tempClass.dreamTime!))
                            self.navigationController!.view.addSubview(self.mainView!)
                            self.navigationController!.view.bringSubviewToFront(self.mainView!)
                            
                        }
                        if tempClass.time == nil {
                            tempClass.time = NSTimer.scheduledTimerWithTimeInterval(0.5, target:self, selector: Selector("startTimer:"), userInfo: viewedObject, repeats: true)
                        }
                        
                    } else {
                        let vidURL: NSURL = self.writeVidTemp(data!, dream: true)!
                        let video: AVAsset = (AVAsset(URL: vidURL))
                        let videoLength = video.duration;
                        if tempClass.dreamTime == nil {
                            let floatLength:Double = Double(CMTimeGetSeconds(videoLength))
                            tempClass.dreamTime = floatLength
                        }
                        self.playVideo(vidURL, dream: true)
                        if tempClass.dreamTime > 0 {
                            self.counterLabel!.text = String(Int(tempClass.dreamTime!))
                            self.navigationController!.view.addSubview(self.mainView!)
                            self.navigationController!.view.bringSubviewToFront(self.mainView!)
                            self.playerDream!.play()
                        }
                        if tempClass.time == nil {
                            tempClass.time = NSTimer.scheduledTimerWithTimeInterval(0.5, target:self, selector: Selector("startTimer:"), userInfo: viewedObject, repeats: true)
                        }
                    }
                    self.currentDream = viewedObject
                    
                }
            }
        }
        
        if gestureRecognizer.state == UIGestureRecognizerState.Ended {
            self.mainView?.removeFromSuperview()
        }
    }
    
    
    func startTimer(timer:NSTimer){
        
        let key:PFObject = timer.userInfo as! PFObject
        let tempObject:Flashback = FlashbackList[key]!
        
        if currentDream == key {
            counterLabel!.text = String(Int(tempObject.dreamTime!))
        }
        
        if tempObject.dreamTime! <= 0 {
            if currentDream == key {
                self.mainView?.removeFromSuperview()
            }
            tempObject.time?.invalidate()

            FlashbackList.removeEntryForKey(key)
            let offset = self.tableView.contentOffset.y
            self.tableView.reloadData()
            
            self.tableView.setContentOffset(CGPoint(x:0, y:offset), animated: false)
            
            if self.FlashbackList.count == 0 {
                self.tableEmpty = true
            } else {
                self.tableEmpty = false
            }
            
        } else {
            tempObject.dreamTime = tempObject.dreamTime! - 0.5
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.FlashbackList.count > 0 {
            return self.FlashbackList.count
        } else {
            return 1
        }
    }
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FeedCell = tableView.dequeueReusableCellWithIdentifier("cell") as! FeedCell

        cell.title.numberOfLines = 0
        cell.title.lineBreakMode = NSLineBreakMode.ByWordWrapping
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        if self.tableEmpty == true {
            if indexPath.row == 0 {
                cell.title.text = "...no incoming flashbacks ... send more! ..."
                cell.title.font = UIFont(name: "AvenirNext-Italic", size: 17)
                cell.title.textAlignment = .Center
                cell.title.textColor = UIColor.lightGrayColor()
                cell.name.text = ""
                cell.counter.text = ""
                cell.sentTimeLabel.text = ""
                cell.tapToLoad.text = ""
                cell.indicator.removeFromSuperview()
            }
            
        } else {
            
            if indexPath.row == self.FlashbackList.count {
                tableView.separatorStyle = .None
            }
            
            let flashback = self.FlashbackList[indexPath.row].1
            let status = flashback.status!
            switch status {
            case .tapToLoad:
                cell.tapToLoad.text = "tap to load" //changed below if incoming
                if cell.indicator.isAnimating() == false {
                    cell.indicator.stopAnimating()
                }
                cell.indicator.alpha = 0
            case .loading:
                cell.tapToLoad.text = "loading..."
                cell.indicator.startAnimating()
                cell.indicator.alpha = 1
            case .blank:
                cell.tapToLoad.text = ""
                if cell.indicator.isAnimating() == false {
                    cell.indicator.stopAnimating()
                }
                cell.indicator.alpha = 0
            case .tryAgain:
                cell.tapToLoad.text = "try again"
                if cell.indicator.isAnimating() == false {
                    cell.indicator.stopAnimating()
                }
                cell.indicator.alpha = 1
                cell.indicator.color = UIColor.redColor()
            case .pressToDream:
                cell.tapToLoad.text = "press to dream"
                if cell.indicator.isAnimating() == false {
                    cell.indicator.stopAnimating()
                }
                cell.indicator.alpha = 0
            }
            
            let viewedObject:PFObject = self.FlashbackList[indexPath.row].0
            let activity:PFObject = (viewedObject["activity"] as? PFObject)!
            cell.title.font = UIFont.systemFontOfSize(13)
            cell.title.textColor = UIColor.darkGrayColor()
            cell.title.textAlignment = .Left
            cell.title.text = activity["title"] as? String
            if cell.title.text == "" {
                
                cell.title.text = "...no caption..."
                cell.title.font = UIFont.systemFontOfSize(13, weight: UIFontWeightLight)
                cell.title.textColor = UIColor.lightGrayColor()

            }
            let user:PFUser = (activity["user"] as? PFUser)!
            cell.name.text = user.username!

            
            if activity["memory"] as? Bool == true {
                cell.counter.text = "memory"
                cell.counter.textColor = UIColor.darkGrayColor()
                cell.counter.font = UIFont(name: cell.counter.font.fontName, size: 17)

            }else {
                cell.counter.text = "DREAM"
                cell.counter.textColor = UIColor.highlightColor()
                cell.counter.font = UIFont(name: cell.counter.font.fontName, size: 17)

            }
            
            let formatter = NSDateFormatter()
            
            let createdAt = activity.createdAt
            
            let userCalendar = NSCalendar.currentCalendar()
            
            
            let dateComponentsToday:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: NSDate())
            
            let dayToday = userCalendar.dateFromComponents(dateComponentsToday)!
            
            let oneWeekAgo = userCalendar.dateByAddingUnit(
                .Day,
                value: -7,
                toDate: dayToday,
                options: [])!
            
            let dayCreatedComp:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: createdAt!)
            
            let dayCreatedAt = userCalendar.dateFromComponents(dayCreatedComp)!
            
            let withinOneWeek = oneWeekAgo.compare(dayCreatedAt)
            if withinOneWeek == .OrderedAscending{
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                
                
                let myWeekday = NSCalendar.currentCalendar().components(NSCalendarUnit.Weekday, fromDate: createdAt!).weekday
                
                var myWeekdayConverted:String?
                
                switch(myWeekday) {
                case 1:
                    myWeekdayConverted = "Sun"
                case 2:
                    myWeekdayConverted = "Mon"
                case 3:
                    myWeekdayConverted = "Tue"
                case 4:
                    myWeekdayConverted = "Wed"
                case 5:
                    myWeekdayConverted = "Thu"
                case 6:
                    myWeekdayConverted = "Fri"
                case 7:
                    myWeekdayConverted = "Sat"
                default:
                    myWeekdayConverted = "error"
                }

                let dayYesterday = userCalendar.dateByAddingUnit(
                    .Day,
                    value: -1,
                    toDate: dayToday,
                    options: [])!
                
                if dayCreatedAt == dayToday {
                    cell.sentTimeLabel.text = "today " + formatter.stringFromDate(createdAt!)
                } else if dayCreatedAt == dayYesterday {
                    cell.sentTimeLabel.text = "yesterday " + formatter.stringFromDate(createdAt!)
                } else {
                    cell.sentTimeLabel.text = myWeekdayConverted! + " " + formatter.stringFromDate(createdAt!)
                }

                
            } else {
            
                formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                formatter.timeStyle = NSDateFormatterStyle.ShortStyle

                let dateString = formatter.stringFromDate(createdAt!)
                cell.sentTimeLabel.text = dateString
            }
            
            let dateUnlocked:NSDate =  (activity["dateUnlocked"] as? NSDate)!
            let unlockedFlashback = dateUnlocked.compare(NSDate())
            if unlockedFlashback == .OrderedDescending {
                let unlockedIncoming = dateUnlocked.compare(self.releaseTime!)
                if unlockedIncoming == .OrderedAscending{
                    
                    cell.counter.textColor = UIColor.lightGrayColor()
                    cell.counter.font = UIFont(name: cell.counter.font.fontName, size: 14)

                    let timerAmount:Double = dateUnlocked.timeIntervalSinceDate(NSDate())
                    if timerAmount > 90{
                        cell.counter.text = String(Int(round(timerAmount/60))) + " minutes..."
                    } else if timerAmount < 90 && timerAmount > 0 {
                        cell.counter.text = "1 minute..."
                    }
                    cell.tapToLoad.text = "incoming..."

                }
            }
        
            if (activity["memory"] as? Bool)! == false {
                
                let dateUnlocked:NSDate =  (activity["dateUnlocked"] as? NSDate)!
                let unlocked = dateUnlocked.compare(NSDate())
                if unlocked == .OrderedAscending {

                    if self.FlashbackList[indexPath.row].1.loaded == true {
                        let longPress = UILongPressGestureRecognizer(target: self, action: Selector("held:"))
                        cell.addGestureRecognizer(longPress)
                    }
                }
            }
        }
        
        return cell
    }
    
    func setupPopUp(){
        
        mainView = UIView()
        mainView!.frame = CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height)
        mainView!.backgroundColor = UIColor.clearColor()
        
        
        let blurEffect:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let effectView:UIVisualEffectView = UIVisualEffectView (effect: blurEffect)
        effectView.frame = mainView!.frame
        mainView!.addSubview(effectView)
        
        
        imageView = UIImageView()
        imageView!.backgroundColor = UIColor.lightGrayColor()
        imageView!.frame = CGRectMake(0, 74, self.view.frame.width, 1.3 * self.view.frame.width)
        mainView!.addSubview(imageView!)
        
        
        let dreamTitle = UILabel()
        dreamTitle.text = "D R E A M"
        dreamTitle.font = UIFont(name: "AvenirNext-DemiBold", size: 30)
        dreamTitle.textColor = UIColor.whiteColor()
        mainView!.addSubview(dreamTitle)


        dreamTitle.translatesAutoresizingMaskIntoConstraints = false
        let xCenterConstraint = NSLayoutConstraint(item: dreamTitle, attribute: .CenterX, relatedBy: .Equal, toItem: mainView, attribute: .CenterX, multiplier: 1, constant: 0)
        
        let topConstraint = NSLayoutConstraint(item: dreamTitle, attribute: .Top, relatedBy: .Equal, toItem: mainView, attribute: .Top, multiplier: 1, constant: 20)
        
        mainView!.addConstraints([xCenterConstraint, topConstraint])
        
        counterLabel = UILabel()
        counterLabel!.text = "10"
        counterLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 30)
        counterLabel!.textColor = UIColor.whiteColor()
        mainView!.addSubview(counterLabel!)
        
        counterLabel!.translatesAutoresizingMaskIntoConstraints = false
        let trailingConstraint2 = NSLayoutConstraint(item: counterLabel!, attribute: .Trailing, relatedBy: .Equal, toItem: mainView, attribute: .Trailing, multiplier: 1, constant: 8)

        let width2 = NSLayoutConstraint(item: counterLabel!, attribute: .Width, relatedBy: .Equal, toItem: nil,
            attribute: NSLayoutAttribute.NotAnAttribute,
            multiplier: 1,
            constant: 50)

        let topConstraint2 = NSLayoutConstraint(item: counterLabel!, attribute: .Top, relatedBy: .Equal, toItem: mainView, attribute: .Top, multiplier: 1, constant: 20)
        
        mainView!.addConstraints([trailingConstraint2, topConstraint2, width2])
        
        dreamNameLabel = UILabel()
        dreamNameLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        dreamNameLabel!.textColor = UIColor.darkGrayColor()
        mainView!.addSubview(dreamNameLabel!)

        
        dreamNameLabel!.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraint3 = NSLayoutConstraint(item: dreamNameLabel!, attribute: .Leading, relatedBy: .Equal, toItem: mainView, attribute: .Leading, multiplier: 1, constant: 8)
        
        let width3 = NSLayoutConstraint(item: dreamNameLabel!, attribute: .Width, relatedBy: .Equal, toItem: nil,
            attribute: NSLayoutAttribute.NotAnAttribute,
            multiplier: 1,
            constant: 100)
        
        let height3 = NSLayoutConstraint(item: dreamNameLabel!, attribute: .Height, relatedBy: .Equal, toItem: nil,
            attribute: NSLayoutAttribute.NotAnAttribute,
            multiplier: 1,
            constant: 20)
        
        let topConstraint3 = NSLayoutConstraint(item: dreamNameLabel!, attribute: .Top, relatedBy: .Equal, toItem: imageView, attribute: .Bottom, multiplier: 1, constant: 5)
        
        mainView!.addConstraints([leadingConstraint3, topConstraint3, width3, height3])
        
        dreamPicTitle = UILabel()
        dreamPicTitle!.font = UIFont(name: "AvenirNext-Regular", size: 13)
        dreamPicTitle!.minimumScaleFactor = 0.5
        dreamPicTitle!.textColor = UIColor.darkGrayColor()
        dreamPicTitle!.numberOfLines = 0
        dreamPicTitle!.lineBreakMode = NSLineBreakMode.ByWordWrapping
        dreamPicTitle!.layoutIfNeeded()

        mainView!.addSubview(dreamPicTitle!)
        
        
        dreamPicTitle!.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraint4 = NSLayoutConstraint(item: dreamPicTitle!, attribute: .Leading, relatedBy: .Equal, toItem: mainView, attribute: .Leading, multiplier: 1, constant: 8)
        
        let trailingConstraint4 = NSLayoutConstraint(item: dreamPicTitle!, attribute: .Trailing, relatedBy: .Equal, toItem: mainView, attribute: .Trailing, multiplier: 1, constant: 8)

        let topConstraint4 = NSLayoutConstraint(item: dreamPicTitle!, attribute: .Top, relatedBy: .Equal, toItem: dreamNameLabel, attribute: .Bottom, multiplier: 1, constant: 5)
        
        let bottomConstraint4 = NSLayoutConstraint(item: dreamPicTitle!, attribute: .Bottom, relatedBy: NSLayoutRelation.GreaterThanOrEqual, toItem: dreamNameLabel, attribute: .Bottom, multiplier: 1, constant: 5)
    
        mainView!.addConstraints([leadingConstraint4, topConstraint4, bottomConstraint4, trailingConstraint4])
    }

    
    func wasDragged(gesture: UIPanGestureRecognizer) {
        if CurrentList.count > 0 {
            var xFromCenter: CGFloat = 0
            let translation = gesture.translationInView(self.view)
            let mainPic = gesture.view!
            let scale = min(200 / abs(xFromCenter), 1)
            xFromCenter += translation.x

            mainPic.center = CGPoint(x: mainPic.center.x + translation.x, y: mainPic.center.y)
            gesture.setTranslation(CGPointZero, inView: self.view)
            mainPic.transform = CGAffineTransformScale(self.view.transform, scale, scale)
            
            if self.imageCounter == 0 {

                if translation.x > 0 {

                    let alpha:CGFloat = self.noMoreLeft.alpha + translation.x/200.0

                    self.noMoreLeft.alpha = max(0, min(1, alpha))
                
                } else if self.noMoreLeft.alpha > 0 {

                    let alpha:CGFloat = self.noMoreLeft.alpha + translation.x/200.0
                    
                    self.noMoreLeft.alpha = max(0, min(1, alpha))
                }
            }
            
            
            if self.imageCounter == self.CurrentList.count - 1 {
                
                if translation.x < 0 {
                    
                    let alpha:CGFloat = self.noMoreRight.alpha - translation.x/200.0
                    
                    self.noMoreRight.alpha = max(0, min(1, alpha))
                    
                } else if self.noMoreRight.alpha > 0 {
                    
                    let alpha:CGFloat = self.noMoreRight.alpha - translation.x/200.0
                    
                    self.noMoreRight.alpha = max(0, min(1, alpha))
                }
            }

            if gesture.state == UIGestureRecognizerState.Ended {

                self.noMoreLeft.alpha = 0
                self.noMoreRight.alpha = 0

                if mainPic.center.x < 0 {
                    
                    if ((self.imageCounter) < (self.CurrentList.count-1)) {
                        self.imageCounter! += 1
                        
                        let viewedObject:PFObject = self.CurrentList[self.imageCounter!].0
                        let activity:PFObject = (viewedObject["activity"] as? PFObject)!
                        let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
                        
                        let data:PFFile = (picVideo["mediaFile"] as? PFFile)!
                        self.mainPic.alpha = 0.0
                        self.mainPic.frame = self.frameOfPic!

                        data.getDataInBackgroundWithBlock{
                            (data, error) -> Void in
                            if error == nil {
                                if activity["video"] as? Bool == true {
                                    self.writeVidTemp(data!, dream:false)
                                }else {
                                    if self.player?.rate > 0 {
                                        self.player?.pause()
                                    }
                                    self.playerLayer?.removeFromSuperlayer()
                                    let image = UIImage(data: data!)
                                    let previewImage = ImageUtilCrop.cropToSquare(image: image!)
                                    self.mainPic.image = previewImage
                                    self.play.hidden = true
                                    self.muteItem.hidden = true
                                }
                                
                                self.mainPic.addSubview(self.picDescriptionView)
                                let takenByUser:PFUser = (activity["user"] as? PFUser)!
                                let takenBy:String = takenByUser.username!
                                let formatter = NSDateFormatter()
                                formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                                formatter.timeStyle = NSDateFormatterStyle.NoStyle
                                let dateTaken = formatter.stringFromDate(activity.createdAt!)
                                let description:String = takenBy + " -- " + dateTaken
                                self.picDescription.text = description
                                
                                if self.imageCounter == 1 {
                                    let tap = UITapGestureRecognizer(target: self, action: Selector("tappedPic"))
                                    self.mainPic.addGestureRecognizer(tap)
                                    self.mainPic.userInteractionEnabled = true
                                }
                                self.mainPic.transform = CGAffineTransformIdentity
                                
                                self.view.addSubview(self.mainPic)
                                UIView.animateWithDuration(0.3, delay:0.1, options: [], animations: {
                                    self.mainPic.alpha = 1.0}, completion: {
                                        (value: Bool) in
                                })
                            }
                        }
                    } else {
                            self.mainPic.transform = CGAffineTransformIdentity
                            self.mainPic.frame = self.frameOfPic!
                            self.view.addSubview(self.mainPic)
                    }
                } else if mainPic.center.x > self.view.bounds.width {
                    if self.imageCounter! > 0 {
                        self.imageCounter! -= 1
                        
                        let viewedObject:PFObject = self.CurrentList[self.imageCounter!].0
                        let activity:PFObject = (viewedObject["activity"] as? PFObject)!
                        let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
                        let data:PFFile = (picVideo["mediaFile"] as? PFFile)!
                        self.mainPic.alpha = 0.0
                        self.mainPic.frame = self.frameOfPic!

                        data.getDataInBackgroundWithBlock{
                            (data, error) -> Void in
                            if error == nil {
                                if activity["video"] as? Bool == true {
                                    self.writeVidTemp(data!, dream:false)
                                }else {
                                    if self.player?.rate > 0 {
                                        self.player?.pause()
                                    }
                                    self.playerLayer?.removeFromSuperlayer()
                                    let image = UIImage(data: data!)
                                    let previewImage = ImageUtilCrop.cropToSquare(image: image!)
                                    self.mainPic.image = previewImage
                                    self.play.hidden = true
                                    self.muteItem.hidden = true
                                }
                                
                                self.mainPic.addSubview(self.picDescriptionView)
                                let takenByUser:PFUser = (activity["user"] as? PFUser)!
                                let takenBy:String = takenByUser.username!
                                let formatter = NSDateFormatter()
                                formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                                formatter.timeStyle = NSDateFormatterStyle.NoStyle
                                let dateTaken = formatter.stringFromDate(activity.createdAt!)
                                let description:String = takenBy + " -- " + dateTaken
                                self.picDescription.text = description
                                
                                if self.imageCounter == 1 {
                                    let tap = UITapGestureRecognizer(target: self, action: Selector("tappedPic"))
                                    self.mainPic.addGestureRecognizer(tap)
                                    self.mainPic.userInteractionEnabled = true
                                }
                                self.mainPic.transform = CGAffineTransformIdentity
                                
                                self.view.addSubview(self.mainPic)
                                UIView.animateWithDuration(0.3, delay:0.1, options: [], animations: {
                                    self.mainPic.alpha = 1.0}, completion: {
                                        (value: Bool) in
                                
                                })
                            }
                        }
                    }else {
                        self.mainPic.transform = CGAffineTransformIdentity
                        self.mainPic.frame = self.frameOfPic!
                        self.view.addSubview(self.mainPic)
                    }
                }
                
                self.mainPic.transform = CGAffineTransformIdentity
                self.mainPic.frame = self.frameOfPic!
                self.view.addSubview(self.mainPic)

                xFromCenter = 0
            }
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "pictureWithComments") {
            let viewedObject:PFObject = self.CurrentList[self.imageCounter!].0
            let activity:PFObject = (viewedObject["activity"] as? PFObject)!
            let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
            let whatToSend = segue.destinationViewController as! PicCommentsViewController;
            whatToSend.activity = activity
            whatToSend.picVideo = picVideo
            whatToSend.name = name

        } else if (segue.identifier == "seeAll") {
            
            let whatToSend = segue.destinationViewController as! ViewAllViewController;
            whatToSend.name = self.name
            whatToSend.uniqueId = self.uniqueId

            if self.groupBool == false {
                whatToSend.groupBool = false
            } else {
                whatToSend.groupBool = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
        updateViewed()
    }
}
