//  Created by Nick Scott


import Foundation
import CoreData


class Threads: NSManagedObject {

    @NSManaged var name: String //
    @NSManaged var flashbacks: NSNumber //
    @NSManaged var incoming: NSNumber //
    @NSManaged var uniqueId: String //
    @NSManaged var dateUnlocked: NSDate //
    @NSManaged var group: Bool //
    @NSManaged var groupMembers: NSArray //

}
