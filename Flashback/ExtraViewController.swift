//  Created by Nick Scott


import UIKit

class ExtraViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tryAgainButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    
    @IBOutlet var errorViewHeight: NSLayoutConstraint!
    @IBOutlet var containerView: UIView!

    @IBOutlet var errorLabel: UILabel!
    
    var refresher = UIRefreshControl()
    
    @IBAction func xFunc(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(false)
        
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var toggleTable: UISegmentedControl!
    
    var connectionsTable = false
    
    // table and toggle
    @IBAction func commentsOrConnections(sender: AnyObject) {
        switch toggleTable.selectedSegmentIndex
        {
        case 0:
            if connectionsTable == true {
                connectionsTable = false
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.tableView.reloadData()
                }
            }
        case 1:
            if connectionsTable == false {
                connectionsTable = true
                
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.tableView.reloadData()
                }
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.tableView.reloadData()
                }
            }
            
        default:
            break;
        }
    }

    @IBAction func tryAgain(sender: AnyObject) {
        self.comments.removeAll(keepCapacity: true)
        
        populateUpdatesWithRefresh(false)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.hidden = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        self.tableView.rowHeight = UITableViewAutomaticDimension

        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        refresher.tintColor = UIColor.highlightColor()
        tableView.addSubview(refresher)
        self.populateUpdatesWithRefresh(false)
        self.populateConnectionsWithRefresh(false)
        
    }
    
    func refresh(){
        if connectionsTable == false {
            populateUpdatesWithRefresh(true)
        } else {
            populateConnectionsWithRefresh(true)
        }
    }
    
    struct CommentPlus{
        var createdAt:NSDate?
        var username:String?
        var comment:String?
        var thumbnail:UIImage?
        var picVideoId:String?
        var usersPhoto:String?
        
        var activityId:String?
        var activityCreatedAt:NSDate?
        var activityIsVideo:Bool?
        var activityUsername:String?
        var activityMessage:String?
        
        var picCommentTitle:String?
    }
    
    var comments = [CommentPlus]()
    var lastCommentDate:NSDate?
    
    struct ConnectionPlus{
        let createdAt:NSDate?
        let userWhoAdded:String?
        let groupName:String?
        let groupMembers:Array<String>?
    }
    
    var connections = [ConnectionPlus]()
    var lastConnection:NSDate?
    
    func populateUpdatesWithRefresh(refresh:Bool){
        
        if refresh == false {
            
            self.tryAgainButton.hidden = true
            activityIndicator.startAnimating()
            activityIndicator.color = UIColor.highlightColor()
            activityIndicator.hidden = false
        }
        
        
        if Reachability.isConnectedToNetwork() {

            let activityQuery1 = PFQuery(className: "Activity")
            activityQuery1.whereKey("user", equalTo: PFUser.currentUser()!)
            
            
            let activityQuery2 = PFQuery(className: "Activity")
            activityQuery2.whereKey("receiver", equalTo: PFUser.currentUser()!)
            
            let activityQuery = PFQuery.orQueryWithSubqueries([activityQuery1, activityQuery2])
            
            
            let getComments = PFQuery(className: "comments")
            getComments.whereKey("activity", matchesQuery: activityQuery)
            getComments.whereKey("madeBy", notEqualTo: PFUser.currentUser()!.username!)
            getComments.orderByDescending("createdAt")
            if refresh == true {
                if let lastComment = self.lastCommentDate {
                    getComments.whereKey("createdAt", greaterThan: lastComment)
                }
            }
            
            getComments.includeKey("activity.picVideo")
            getComments.limit = 50
            getComments.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                
                if error == nil {
                    
                    self.errorViewHeight.constant = 0
                    
                    let countQuery = objects!.count
                    
                    var countingThumbs:Int = 0
                    var countingObjects:Int = 0
                    if objects!.count == 0 {
                        if self.refresher.refreshing {
                            self.refresher.endRefreshing()
                        }
                    }
                    
                    
                    for object in objects! {
                        
                        if countingObjects == 0{
                            self.lastCommentDate = object.createdAt
                        } else {
                            let unlocked = self.lastCommentDate!.compare(object.createdAt!)
                            if unlocked == .OrderedAscending {
                                self.lastCommentDate = object.createdAt!
                            }
                        }
                        countingObjects++
                        
                        
                        let createdAt = object.createdAt
                        let username:String = (object["madeBy"] as? String)!
                        let comment:String = (object["comment"] as? String)!
                        let activity:PFObject = (object["activity"] as? PFObject)!
                        let activityId:String = activity.objectId!
                        let activityCreatedAt:NSDate = activity.createdAt!
                        let activityIsVideo:Bool = (activity["video"] as? Bool)!
                        let activityUser = (activity["user"] as? PFUser)!
                        let activityUsername:String = activityUser.username!
                        let activityMessage:String = (activity["title"] as? String)!
                        let picVideo:PFObject = (activity["picVideo"] as? PFObject)!
                        let picVideoId:String = picVideo.objectId!
                        let usersPhotoPFUser:PFUser = (activity["user"] as? PFUser)!
                        var usersPhoto:String?
                        
                        var picCommentTitle:String!
                        if activity["group"] as? PFObject === nil {
                            if (activity["receiver"] as? PFUser)! == PFUser.currentUser() {
                                let user:PFUser = (activity["user"] as? PFUser)!
                                picCommentTitle = user.username
                            } else {
                                let receiver:PFUser = (activity["receiver"] as? PFUser)!
                                picCommentTitle = receiver.username
                            }
                        } else {
                            let group:PFObject = (activity["group"] as? PFObject)!
                            picCommentTitle = group["groupName"] as? String
                        }
                        
                        
                        if usersPhotoPFUser == PFUser.currentUser() {
                            usersPhoto = "your"
                        }else {
                            usersPhoto = usersPhotoPFUser.username!
                        }
                        let data:PFFile = (picVideo["thumbnail"] as? PFFile)!
                        data.getDataInBackgroundWithBlock{
                            (data, error) -> Void in
                            if error == nil {
                                
                                countingThumbs++
                                let thumbnail = UIImage(data: data!)
                                let comment:CommentPlus = CommentPlus(createdAt: createdAt, username: username, comment: comment, thumbnail: thumbnail, picVideoId: picVideoId, usersPhoto: usersPhoto, activityId: activityId, activityCreatedAt: activityCreatedAt, activityIsVideo: activityIsVideo, activityUsername: activityUsername, activityMessage: activityMessage, picCommentTitle: picCommentTitle)
                                
                                self.comments.append(comment)
                                if countingThumbs == countQuery {
                                    
                                    self.comments.sortInPlace(self.sortByCreatedAt)
                    
                                    dispatch_async(dispatch_get_main_queue()) {
                                        self.tableView.reloadData()
                                    }
                                    
                                    if self.refresher.refreshing {
                                        self.refresher.endRefreshing()
                                        
                                    }
                                }
                            }
                        }
                    }
                    
                    if refresh == false {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
                    }
                    
                } else {
                    if refresh == true {
                        self.errorLabel.text = "oops! something went wrong"
                        self.errorViewHeight.constant = 20
                        self.refresher.endRefreshing()
                        self.delay(3){
                            if !self.refresher.refreshing {
                                UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                    { () -> Void in
                                        
                                        self.errorViewHeight.constant = 0
                                        self.view.layoutIfNeeded()
                                        
                                    }, completion: {finished in
                                })
                            }
                        }
                        
                    } else {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.color = UIColor.redColor()
                        self.tryAgainButton.hidden = false
                    }
                }
            }
        } else {
            if refresh == true {
                self.errorLabel.text = "no internet connection"
                self.errorViewHeight.constant = 20
                self.refresher.endRefreshing()
                self.delay(3){
                    if !self.refresher.refreshing {
                        UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                            { () -> Void in
                                
                                self.errorViewHeight.constant = 0
                                self.view.layoutIfNeeded()
                                
                            }, completion: {finished in
                        })
                    }
                }
                
            } else {
                delay(0.5){
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.color = UIColor.redColor()
                    self.tryAgainButton.hidden = false
                }
            }
            
            
        }
    }
    
    
    
    func populateConnectionsWithRefresh(refresh:Bool){
        
        if refresh == false {
            
            self.tryAgainButton.hidden = true
            activityIndicator.startAnimating()
            activityIndicator.color = UIColor.highlightColor()
            activityIndicator.hidden = false
        }
        
        
        if Reachability.isConnectedToNetwork() {
            
            let groupQuery = PFQuery(className: "Group")
            groupQuery.whereKey("members", containsAllObjectsInArray: [PFUser.currentUser()!])
            //groupQuery.whereKey("user", notEqualTo: PFUser.currentUser()!)
            if refresh == true {
                groupQuery.orderByAscending("createdAt")
            } else {
                groupQuery.orderByDescending("createdAt")
            }
            
            if refresh == true {

                if let lastConnection = self.lastConnection {
                    groupQuery.whereKey("createdAt", greaterThan: lastConnection)

                }
            }
            groupQuery.limit = 20
            groupQuery.findObjectsInBackgroundWithBlock {
                (objects, error) -> Void in
                
                
                
                if error == nil {
                    
                    self.errorViewHeight.constant = 0
                    

                    var countingObjects:Int = 0
                    if objects!.count == 0 {
                        if self.refresher.refreshing {
                            self.refresher.endRefreshing()
                            
                        }
                    }
                    
                    
                    for object in objects! {
                        
                        if countingObjects == 0{
                            self.lastConnection = object.createdAt
                        } else {
                            let unlocked = self.lastConnection!.compare(object.createdAt!)
                            if unlocked == .OrderedAscending {
                                self.lastConnection = object.createdAt!
                            }
                        }
                        countingObjects++
                        
                        
                        let createdAt = object.createdAt
                        if let userWhoAddedUser:PFUser = (object["user"] as? PFUser) {
                            let userWhoAdded:String = userWhoAddedUser.username!
                            let groupName:String = (object["groupName"] as? String)!
                            let groupMembers = (object["members"] as AnyObject? as! [PFUser])
                            var groupMembersNames = [String]()
                            for member in groupMembers {
                                if member != PFUser.currentUser() {
                                    groupMembersNames.append(member.username!)
                                }
                            }
                            
                            let connectionPlus:ConnectionPlus = ConnectionPlus(createdAt: createdAt, userWhoAdded: userWhoAdded, groupName: groupName, groupMembers: groupMembersNames)
                            
                            if refresh == false {
                                self.connections.append(connectionPlus)
                            } else {
                                self.connections.insert(connectionPlus, atIndex:0)
                            }
                        }
                    }
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                    }
                    
                    if self.refresher.refreshing {
                        self.refresher.endRefreshing()
                        
                    }
                    
                    
                    if refresh == false {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
                    }
                    
                    
                    
                } else {
                    if refresh == true {
                        self.errorLabel.text = "oops! something went wrong"
                        self.errorViewHeight.constant = 20
                        self.refresher.endRefreshing()
                        self.delay(3){
                            if !self.refresher.refreshing {
                                UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                                    { () -> Void in
                                        
                                        self.errorViewHeight.constant = 0
                                        self.view.layoutIfNeeded()
                                        
                                    }, completion: {finished in
                                })
                            }
                        }
                        
                    } else {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.color = UIColor.redColor()
                        self.tryAgainButton.hidden = false
                    }
                    
                }
            }
        } else {
            
            if refresh == true {
                self.errorLabel.text = "no internet connection"
                self.errorViewHeight.constant = 20
                self.refresher.endRefreshing()
                self.delay(3){
                    if !self.refresher.refreshing {
                        UIView.animateWithDuration(0.65, delay: 1.3, options: UIViewAnimationOptions.CurveEaseOut, animations:
                            { () -> Void in
                                
                                self.errorViewHeight.constant = 0
                                self.view.layoutIfNeeded()
                                
                            }, completion: {finished in
                        })
                    }
                }
                
                
            } else {
                delay(0.5){
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.color = UIColor.redColor()
                    self.tryAgainButton.hidden = false
                }
            }
            
            
        }
    }
    
    
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func sortByCreatedAt(lhs:CommentPlus, rhs:CommentPlus) -> Bool {
        let sort = lhs.createdAt!.compare(rhs.createdAt!)
        if sort == .OrderedDescending {
            return true
        } else {
            return false
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if connectionsTable == false {
            return comments.count
        } else {
            return connections.count
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if connectionsTable == false {
            let cell:extraCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! extraCell
            
            
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            formatter.timeStyle = NSDateFormatterStyle.NoStyle
            var creationDate:NSDate!
            
            
            creationDate = self.comments[indexPath.row].createdAt!
            
            let userCalendar = NSCalendar.currentCalendar()
            
            let myWeekday = NSCalendar.currentCalendar().components(NSCalendarUnit.Weekday, fromDate: creationDate).weekday
            
            var myWeekdayConverted:String?
            
            switch(myWeekday) {
            case 1:
                myWeekdayConverted = "Sun"
            case 2:
                myWeekdayConverted = "Mon"
            case 3:
                myWeekdayConverted = "Tue"
            case 4:
                myWeekdayConverted = "Wed"
            case 5:
                myWeekdayConverted = "Thu"
            case 6:
                myWeekdayConverted = "Fri"
            case 7:
                myWeekdayConverted = "Sun"
            default:
                myWeekdayConverted = "error"
            }
            
            let dateComponentsToday:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: NSDate())
            
            let dayToday = userCalendar.dateFromComponents(dateComponentsToday)!
            
            let dayYesterday = userCalendar.dateByAddingUnit(
                .Day,
                value: -1,
                toDate: dayToday,
                options: [])!
            
            let dateComponentsPicker:NSDateComponents = userCalendar.components([.Day, .Month, .Year], fromDate: creationDate)
            let dayCreatedAt = userCalendar.dateFromComponents(dateComponentsPicker)!
            
            
            let dateString:String?
            if dayToday == dayCreatedAt {
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                dateString = "today " + formatter.stringFromDate(creationDate) + ": "
            } else if dayYesterday == dayCreatedAt {
                formatter.dateStyle = NSDateFormatterStyle.NoStyle
                dateString = "yesterday " + formatter.stringFromDate(creationDate) + ": "
            } else {
                dateString = "on " + myWeekdayConverted! + " " + formatter.stringFromDate(creationDate) + ": "
            }
            
            let comment:String = self.comments[indexPath.row].comment!
            let name:String = self.comments[indexPath.row].username!
            let usersPhoto:String = self.comments[indexPath.row].usersPhoto!
            let commentOn:String = " commented on "
            var onPhoto:String?
            var usersPhotoLength:Int?
            if usersPhoto == "your" {
                usersPhotoLength = usersPhoto.characters.count
                onPhoto = " photo "
            } else {
                onPhoto = "'s photo "
                usersPhotoLength = usersPhoto.characters.count + 2
            }
            
            
            let wholeString:String = name + commentOn + usersPhoto + onPhoto! + dateString! + comment
            
            
            let commentPlusUser = NSMutableAttributedString(string: wholeString)
            commentPlusUser.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(12, weight: UIFontWeightBold), range: NSRange(location: 0,length: (name.characters.count) ))
            commentPlusUser.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSRange(location:0,length: (name.characters.count)))
            
            commentPlusUser.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(12, weight: UIFontWeightBold), range: NSRange(location: (name.characters.count + commentOn.characters.count),length: usersPhotoLength! ))
            commentPlusUser.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSRange(location: (name.characters.count + commentOn.characters.count),length: usersPhotoLength! ))
            
            commentPlusUser.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSRange(location: (wholeString.characters.count - comment.characters.count - (dateString!).characters.count),length: (dateString!).characters.count ))
            
            cell.comment.numberOfLines = 0
            cell.comment.lineBreakMode = NSLineBreakMode.ByWordWrapping
            cell.layoutIfNeeded()
            cell.comment.attributedText = commentPlusUser
            cell.thumbnail.image = self.comments[indexPath.row].thumbnail
            
            return cell
        } else {
            
            let cell:extraCellConnection = self.tableView.dequeueReusableCellWithIdentifier("cell2") as! extraCellConnection
            
            var userWhoAdded:String?
            var middle:String?
            
            if self.connections[indexPath.row].userWhoAdded! == PFUser.currentUser()?.username {
                userWhoAdded = self.connections[indexPath.row].userWhoAdded!
                middle = " has added you to a group named "
            } else {
                userWhoAdded = "You"
                middle = " created the group "
            }
            
            let width = " with "
            
            
            var groupMembers:[String] = self.connections[indexPath.row].groupMembers!
            let lastMember:String = groupMembers.last!
            
            groupMembers.removeLast()
            
            var joinMembers = groupMembers.joinWithSeparator(", ")
            
            joinMembers = joinMembers + " and " + lastMember
            
            let groupName = self.connections[indexPath.row].groupName!

            
            let wholeString:String = userWhoAdded! + middle! + groupName + width + joinMembers
            
            let connectionPlusUser = NSMutableAttributedString(string: wholeString)
            
            connectionPlusUser.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(12, weight: UIFontWeightBold), range: NSRange(location: 0,length: ((userWhoAdded!).characters.count) ))
            connectionPlusUser.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSRange(location:0,length: ((userWhoAdded!).characters.count)))
            
            
            connectionPlusUser.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(12, weight: UIFontWeightBold), range: NSRange(location: ((userWhoAdded!).characters.count + (middle!).characters.count),length: groupName.characters.count ))
        
            
            cell.connectionLabel.numberOfLines = 0
            cell.connectionLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
            cell.layoutIfNeeded()
            cell.connectionLabel.attributedText = connectionPlusUser
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        return UITableViewAutomaticDimension
        
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 60
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if connectionsTable == false {
            let picComments = self.storyboard!.instantiateViewControllerWithIdentifier("PicComments") as! PicCommentsViewController
            picComments.picVideoId = comments[indexPath.row].picVideoId
            picComments.activityId = comments[indexPath.row].activityId
            picComments.activityCreatedAt = comments[indexPath.row].activityCreatedAt
            picComments.activityIsVideo = comments[indexPath.row].activityIsVideo
            picComments.fromPush = true
            picComments.activityUsername = comments[indexPath.row].activityUsername
            picComments.activityMessage = comments[indexPath.row].activityMessage
            picComments.name = comments[indexPath.row].picCommentTitle
            
            self.navigationController?.pushViewController(picComments, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        containerView.layer.cornerRadius = 5
    }
}
