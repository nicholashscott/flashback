//  Created by Nick Scott

import UIKit

enum ChildViews {
    case A
    case B
    case C
}


protocol ContainerView {
    func disableScroll(_: ChildViews) //misleading name
}


class MainScrollView: UIViewController, ContainerView, UIScrollViewDelegate {
    
    
    var fromLoginSignUp:Bool?

    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var scrollViewTop: NSLayoutConstraint!

    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad();
        
        let AVc = storyboard!.instantiateViewControllerWithIdentifier("buddies") as! UINavigationController
        let BVc = storyboard!.instantiateViewControllerWithIdentifier("capture") as! UINavigationController
        let CVc = storyboard!.instantiateViewControllerWithIdentifier("threads") as! UINavigationController


        self.addChildViewController(CVc);
        self.scrollView!.addSubview(CVc.view);
        CVc.didMoveToParentViewController(self);
        
        self.addChildViewController(BVc);
        self.scrollView!.addSubview(BVc.view);
        BVc.didMoveToParentViewController(self);
        
        self.addChildViewController(AVc);
        self.scrollView!.addSubview(AVc.view);
        AVc.didMoveToParentViewController(self);
        

        var adminFrame :CGRect = AVc.view.frame;
        adminFrame.origin.x = adminFrame.width;
        BVc.view.frame = adminFrame;
        
        var BFrame :CGRect = BVc.view.frame;
        BFrame.origin.x = 2*BFrame.width;
        CVc.view.frame = BFrame;
        
        
        //solve problem of starting it up while phone call is going on because of status bar
        AVc.view.frame = CGRectMake(AVc.view.frame.minX, 0, AVc.view.frame.width, UIScreen.mainScreen().bounds.height)
        BVc.view.frame = CGRectMake(BVc.view.frame.minX, 0, BVc.view.frame.width, UIScreen.mainScreen().bounds.height)
        CVc.view.frame = CGRectMake(CVc.view.frame.minX, 0, CVc.view.frame.width, UIScreen.mainScreen().bounds.height)
        
        if MainScrollView.firstTime == true {
            let scrollWidth: CGFloat  = 3 * self.view.frame.width
            self.scrollView!.contentSize.width = scrollWidth
        }
        
        ThreadsViewController.delegate = self
        BuddiesViewController.delegate = self
        CameraViewController.delegate = self
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "adjustContainer:", name: UIApplicationWillChangeStatusBarFrameNotification, object: nil)

    }
    
    func adjustContainer(notification: NSNotification) {
        if UIApplication.sharedApplication().statusBarFrame.height > 20 {
            scrollViewTop.constant = scrollViewTop.constant - 20
        } else {
            scrollViewTop.constant = scrollViewTop.constant + 20
        }
    }
    
    
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    func disableScroll(viewToShow: ChildViews) {

            var viewNum = -1
            switch viewToShow {
            case .A:
                viewNum = 0
            case .B:
                viewNum = 1
            case .C:
                viewNum = 2
            }
        
        let xPos: CGFloat = self.view.frame.width * CGFloat(viewNum)
        self.scrollView.setContentOffset(CGPointMake(xPos,0), animated: true)
    }
    
    static var firstTime:Bool = true
    
    override func viewWillLayoutSubviews() {
        if MainScrollView.firstTime == true {
            
            
            
            if let globalPush = GlobalOpenedByPushNotification {
                if globalPush == "comment" || globalPush == "flashback" {
                    scrollView!.setContentOffset(CGPointMake((self.view.frame.width * 2), 0), animated: false)
                } else if globalPush == "group" {
                    scrollView!.setContentOffset(CGPointMake(0, 0), animated: false)
                } 
            } else {
                scrollView!.setContentOffset(CGPointMake(self.view.frame.width, 0), animated: false)

            }
            
        }
        MainScrollView.firstTime = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
