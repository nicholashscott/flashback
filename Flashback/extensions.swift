//  Created by Nick Scott


import UIKit

var GlobalShouldReload:Bool?
var GlobalSignInUp:Bool?

var GlobalOpenedByPushNotification:String? // this should be enum


class GlobalPushComment{
    var picVideoId:String!
    var activityId:String!
    var isVideo:Bool!
    var createdAtNSDate:NSDate!
    var name:String!
    var message:String!
    var picVideoName:String!
}

var globalPushComment = GlobalPushComment()

var muted:Bool?

extension UIColor {
    convenience init(hex: Int) {
        self.init(hex: hex, alpha: 1)
    }
    
    convenience init(hex: Int, alpha: Double) {
        self.init(
            red: CGFloat((hex >> 16) & 0xff) / 255,
            green: CGFloat((hex >> 8) & 0xff) / 255,
            blue: CGFloat(hex & 0xff) / 255,
            alpha: CGFloat(alpha))
    }
    
    class func highlightColor() -> UIColor{
        return UIColor(hex: 0x11F9CD)
    }
}

class PaddedCell: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 5);
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    private func newBounds(bounds: CGRect) -> CGRect {
        
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= (padding.top * 2) - padding.bottom
        newBounds.size.width -= (padding.left * 2) - padding.right
        return newBounds
    }
    
}