//  Created by Nick Scott


import UIKit

class FeedCell: UITableViewCell {

    @IBOutlet var name: UILabel!

    @IBOutlet var sentTimeLabel: UILabel!
    @IBOutlet var counter: UILabel!
    
    @IBOutlet var title: UILabel!
        
    @IBOutlet var indicator: UIActivityIndicatorView!

    @IBOutlet var tapToLoad: UILabel!

}
